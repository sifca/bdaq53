#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import urllib2
import re
import logging
import requests
import math
import pkg_resources
import tarfile
import os.path
from sys import platform

from tqdm import tqdm
import pexpect

repository = r'https://gitlab.cern.ch/silab/bdaq53/'

firmware_dev_url = repository + r'wikis/Hardware/Firmware-(development-versions)'
firmware_url = repository + r'tags'

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def get_web_page(url):
    ''' Read web page.
    '''
    response = urllib2.urlopen(url)  # Add md for markdown
    return response.read()


def download_firmware(name, url):
    ''' Download firmware tar.gz file from url

        The release firmwares are stored with the tag information (firmware_url)
        and the development firmware is attached to a wiki page (firmware_dev_url)
    '''
    response = get_web_page(url)
    links = re.findall(r'(%s)(\w+)(/%s)' % (r'uploads/', name), response)
    links = [repository + ''.join(link) for link in links]

    if not links:
        all_firmwares = re.findall(r'%s(.*?)%s' % (r'/uploads/\w+/', '.tar.gz'), response)
        logging.error('No firmware with name %s at %s\nPossible firmwares: %s', name, url, ', '.join(all_firmwares))
        return
    if len(links) > 1:
        raise RuntimeError('Found multiple firmwares with name %s at %s', name, url)
    download_link = links[0]
    logging.info('Downloading %s', download_link)
    # Streaming, so we can iterate over the response.
    r = requests.get(download_link, stream=True)

    # Total size in bytes.
    total_size = int(r.headers.get('content-length', 0))
    block_size = 1024
    wrote = 0
    with open(name, 'wb') as f:
        for data in tqdm(r.iter_content(block_size), total=math.ceil(total_size // block_size), unit='KB', unit_scale=True):
            wrote = wrote + len(data)
            f.write(data)
    if total_size != 0 and wrote != total_size:
        raise RuntimeError('Download failed!')
    return name


def unpack_bit_file(name):
    ''' Extracts the bit file from the tar.gz file

        name: string
            Name of bit file (.tar.gz suffix) and
            Name of compressed bitfile (name.bit)
    '''
    tar = tarfile.open(name, "r:gz")
    name_no_suffix = name[:name.find('.')]
    for m in tar.getmembers():
        if name_no_suffix + '.bit' in m.name:
            m.name = os.path.basename(m.name)  # unpack without folder
            tar.extract(m, path=".")
            logging.debug('Unpacked %s', m.name)
    tar.close()
    return name_no_suffix + '.bit'


def flash_firmware(name):
    ''' Flash firmware using vivado in tcl mode
    '''

    def get_return_string(timeout=1):
        ''' Helper function to get full return string.

            More complexity needed here since Xilinx does multi line returns
        '''
        flushed = ''
        try:
            while not vivado.expect(r'.+', timeout=timeout):
                flushed += vivado.match.group(0)
        except pexpect.exceptions.TIMEOUT:
            pass
        return flushed

    logger.info('Flash firmware %s', name)

    try:
        vivado = pexpect.spawn('vivado_lab -mode tcl',
                               timeout=10)  # try lab version
        vivado.expect('Vivado', timeout=3)
    except pexpect.exceptions.ExceptionPexpect:
        try:
            vivado = pexpect.spawn(
                'vivado -mode tcl', timeout=10)  # Try paid version
            vivado.expect('Vivado', timeout=3)
        except pexpect.exceptions.ExceptionPexpect:
            logger.error('Cannot execute vivado / vivado_lab commend')
            return
    vivado.expect('vivado_lab%')  # Booted up when showing prompt

    vivado.sendline('open_hw')
    vivado.expect('vivado_lab%')  # Command finished when showing prompt

    vivado.sendline('connect_hw_server')
    vivado.expect('localhost')  # Printed when successfull
    get_return_string()

    vivado.sendline('current_hw_target')
    ret = get_return_string()
    if 'WARNING' in ret:
        logger.error('Cannot find programmer hardware, Xilinx warning:\n%s', ret)
        vivado.sendline('exit')
        vivado.expect('Exiting')
        return

    vivado.sendline('open_hw_target')
    vivado.expect('Opening hw_target')  # Printed when programmer found

    vivado.sendline('current_hw_device [lindex [get_hw_devices] 0]')
    vivado.expect('vivado_lab%')  # Printed when finished

    vivado.sendline('set devPart [get_property PART [current_hw_device]]')
    vivado.expect('vivado_lab%')  # Printed when finished

    vivado.sendline('set_property PROGRAM.FILE {%s} [current_hw_device]' % name)
    vivado.expect('vivado_lab%')  # Printed when finished

    vivado.sendline('program_hw_devices [current_hw_device]')
    vivado.expect('End of startup status: HIGH')  # firmware upload successfull

    vivado.sendline('exit')
    vivado.expect('Exiting')

    logger.info('SUCCESS!')


def find_vivado_bin(path):
    ''' Search in std. installation paths for vivado(_lab) binary
    '''

    if platform == "linux" or platform == "linux2":
        linux_install_path = '/opt/' if not path else path
        folder = 'Xilinx'
        return search_files(directory=os.path.join(linux_install_path, folder), name='bin')
    else:
        raise NotImplementedError('Only Linux supported thus far')


def search_files(directory, name):
    for dirpath, dirnames, _ in os.walk(directory):
        if name in dirnames and 'xic' not in dirpath and 'xinstall' not in dirpath:
            return os.path.join(dirpath, name)


def main(name, path=None):
    ''' All steps to upload matching firmware to FPGA

        If name has .bit suffix try to flash from local file
        If not available find suitable firmware online, download, extract, and  flash
    '''

    vivado_bin = find_vivado_bin(path)
    if vivado_bin:
        logger.debug('Found vivado binary at %s', vivado_bin)
        os.environ["PATH"] += os.pathsep + vivado_bin
    else:
        logger.error('Cannot find vivado installation in %s', path)
        return

    if os.path.isfile(name):
        logger.info('Found existing local bit file')
        bit_file = name
    else:
        if not name.endswith('.tar.gz'):
            name += '.tar.gz'
        stable_firmware = True  # std. setting: use stable (tag) firmware
        version = pkg_resources.get_distribution("bdaq53").version
        try:
            import git
            try:
                repo = git.Repo(search_parent_directories=True)
                active_branch = repo.active_branch
                if active_branch != 'master':
                    stable_firmware = False  # use development firmware
            except git.InvalidGitRepositoryError:  # no github repo --> use stable firmware
                pass
        except ImportError:  # git not available
            logger.warning(
                'Git not properly installed, assume software release %s', version)
            pass
        if stable_firmware:
            tag_list = get_tag_list(firmware_url)
            matches = [i for i in range(len(tag_list)) if version in tag_list[i]]
            if not matches:
                raise RuntimeError('Cannot find tag version %s at %s', version, firmware_url)
            tag_url = firmware_url + '/' + tag_list[matches[0]]
            logger.info('Download stable firmware version %s', version)
            archiv_name = download_firmware(name, tag_url)
        else:
            logger.info('Download development firmware')
            archiv_name = download_firmware(name, firmware_dev_url + '.md')
        if not archiv_name:
            return
        bit_file = unpack_bit_file(archiv_name)
    flash_firmware(bit_file)


def get_tag_list(url):
    ''' Extracts all tag names from firmware_url

        This is needed since the naming scheme is inconsistent
    '''

    response = get_web_page(url)
    return re.findall(r'href="/silab/bdaq53/tags/(.*?)"', response)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main('BDAQ53.bit')
