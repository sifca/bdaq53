import numpy as np

from online_monitor.converter.transceiver import Transceiver
from online_monitor.utils import utils
from zmq.utils import jsonapi

from bdaq53.analysis import analysis_utils as au


class Bdaq53(Transceiver):

    def setup_transceiver(self):
        ''' Called at the beginning

            We want to be able to change the histogrammmer settings
            thus bidirectional communication needed
        '''
        self.set_bidirectional_communication()

    def setup_interpretation(self):
        ''' Objects defined here are available in interpretation process '''
        utils.setup_logging(self.loglevel)

        self.chunk_size = self.config.get('chunk_size', 1000000)

        # Init result hists
        self.reset_hists()

        # Number of readouts to integrate
        self.int_readouts = 0

        # Variables for meta data time calculations
        self.ts_last_readout = 0.  # Time stamp last readout
        self.hits_last_readout = 0.  # Number of hits
        self.events_last_readout = 0.  # Number of events in last chunk
        self.fps = 0.  # Readouts per second
        self.hps = 0.  # Hits per second
        self.eps = 0.  # Events per second
        self.last_event = -1  # Last chunk last event number
        self.trigger_id = -1  # Last chunk trigger id
        self.ext_trg_num = -1  # external trigger number
        self.last_rawdata = None  # Leftover from last chunk

    def deserialize_data(self, data):
        ''' Inverse of Bdaq53 serialization '''
        try:
            self.meta_data = jsonapi.loads(data)
            return {'meta_data': self.meta_data}
        except ValueError:  # Is raw data
            try:
                dtype = self.meta_data.pop('dtype')
                shape = self.meta_data.pop('shape')
                if self.meta_data:
                    try:
                        raw_data = np.frombuffer(buffer(data),
                                                 dtype=dtype).reshape(shape)
                        return raw_data
                    # KeyError happens if meta data read is omitted
                    # ValueError if np.frombuffer fails due to wrong shape
                    except (KeyError, ValueError):
                        return None
            except AttributeError:  # Happens if first data is not meta data
                return None

    def _interpret_meta_data(self, data):
        ''' Meta data interpratation is deducing timings '''

        meta_data = data[0][1]['meta_data']
        ts_now = float(meta_data['timestamp_stop'])

        # Calculate readout per second with smoothing
        recent_fps = 1.0 / (ts_now - self.ts_last_readout)
        self.fps = self.fps * 0.95 + recent_fps * 0.05

        # Calulate hits per second with smoothing
        recent_hps = self.hits_last_readout * recent_fps
        self.hps = self.hps * 0.95 + recent_hps * 0.05

        # Calulate hits per second with smoothing
        recent_eps = self.events_last_readout * recent_fps
        self.eps = self.eps * 0.95 + recent_eps * 0.05

        self.ts_last_readout = ts_now

        # Add info to meta data
        data[0][1]['meta_data'].update(
            {'fps': self.fps,
             'hps': self.hps,
             'total_hits': self.total_hits,
             'eps': self.eps,
             'total_events': self.total_events})
        return [data[0][1]]

    def interpret_data(self, data):
        ''' Called for every chunk received '''
        if isinstance(data[0][1], dict):  # Meta data
            return self._interpret_meta_data(data)

        if np.any(self.last_rawdata):
            raw_data = np.concatenate((self.last_rawdata, data[0][1]))
        else:
            raw_data = data[0][1]

        # Data is raw data
        (n_hits, last_event_number, chunk_offset, self.trigger_id, self.ext_trg_num) = au.interpret_data(rawdata=raw_data,
                                                                                                         hits=self.hits,
                                                                                                         hist_occ=self.hist_occ,
                                                                                                         hist_tot=self.hist_tot,
                                                                                                         hist_rel_bcid=self.hist_rel_bcid,
                                                                                                         hist_event_status=self.hist_event_status,
                                                                                                         hist_bcid_error=self.hist_bcid_error,
                                                                                                         scan_param_id=0,
                                                                                                         event_number=self.last_event,
                                                                                                         trig_pattern=0b11111111111111111111111111111111,
                                                                                                         align_method=0,
                                                                                                         prev_trig_id=-1,
                                                                                                         prev_trg_number=self.ext_trg_num,
                                                                                                         last_chunk=False
                                                                                                         )

        self.last_rawdata = data[0][1][chunk_offset:]

        self.hits_last_readout = n_hits

        self.events_last_readout = last_event_number - self.last_event
        self.last_event = last_event_number

        self.total_events += self.events_last_readout
        self.total_hits += n_hits
        self.readout += 1

        interpreted_data = {
            'hits': self.hits[:n_hits],
            'occupancy': self.hist_occ[:, :, 0],
            'tot_hist': self.hist_tot.sum(axis=(0, 1, 2)),
            'rel_bcid_hist': self.hist_rel_bcid.sum(axis=(0, 1, 2)),
            'hist_event_status': self.hist_event_status
        }

        if self.int_readouts != 0:  # = 0 for infinite integration
            if self.readout % self.int_readouts == 0:
                self.reset_hists()

        return [interpreted_data]

    def serialize_data(self, data):
        ''' Serialize data from interpretation '''
        if 'hits' in data:
            hits_data = data['hits']
            data['hits'] = None
            return utils.simple_enc(hits_data, data)
        else:
            return utils.simple_enc(None, data)

    def handle_command(self, command):
        ''' Received commands from GUI receiver '''
        if command[0] == 'RESET':
            self.reset_hists()
            self.last_event = -1
            self.trigger_id = -1
        else:
            self.int_readouts = int(command[0])

    def reset_hists(self):
        ''' Reset the histograms '''
        self.total_hits = 0
        self.total_events = 0
        # Readout number
        self.readout = 0

        (self.hits,
         self.hist_occ,
         self.hist_tot,
         self.hist_rel_bcid,
         self.hist_event_status,
         self.hist_bcid_error) = au.init_outs(n_hits=self.chunk_size,
                                              n_scan_params=1)
