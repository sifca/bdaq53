#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import yaml
import logging
import os
import time
import struct
import coloredlogs
import numpy as np
import tables as tb

from tables.exceptions import NoSuchNodeError

from basil.dut import Dut
from basil.utils.BitLogic import BitLogic

from analysis import analysis_utils
from register_utils import RD53ARegisterParser

import pkg_resources
VERSION = pkg_resources.get_distribution("bdaq53").version

loglevel = logging.INFO

''' Set up main logger '''
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
logging.getLogger('basil.HL.RegisterHardwareLayer').setLevel(logging.WARNING)

logging.SUCCESS = 25  # WARNING(30) > SUCCESS(25) > INFO(20)
logging.addLevelName(logging.SUCCESS, 'SUCCESS')

coloredlogs.DEFAULT_FIELD_STYLES = {'asctime': {},
                                    'hostname': {},
                                    'levelname': {'bold': True},
                                    'name': {},
                                    'programname': {}}
coloredlogs.DEFAULT_LEVEL_STYLES = {'critical': {'color': 'red', 'bold': True},
                                    'debug': {'color': 'magenta'},
                                    'error': {'color': 'red', 'bold': True},
                                    'info': {},
                                    'success': {'color': 'green'},
                                    'warning': {'color': 'yellow'}}

coloredlogs.install(fmt="%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s", milliseconds=True)

logger = logging.getLogger('RD53A')
logger.setLevel(loglevel)
logger.success = lambda msg, *args, **kwargs: logger.log(logging.SUCCESS, msg, *args, **kwargs)


class RD53A(Dut):
    cmd_data_map = {
        0: 0b01101010,
        1: 0b01101100,
        2: 0b01110001,
        3: 0b01110010,
        4: 0b01110100,
        5: 0b10001011,
        6: 0b10001101,
        7: 0b10001110,
        8: 0b10010011,
        9: 0b10010101,
        10: 0b10010110,
        11: 0b10011001,
        12: 0b10011010,
        13: 0b10011100,
        14: 0b10100011,
        15: 0b10100101,
        16: 0b10100110,
        17: 0b10101001,
        18: 0b10101010,
        19: 0b10101100,
        20: 0b10110001,
        21: 0b10110010,
        22: 0b10110100,
        23: 0b11000011,
        24: 0b11000101,
        25: 0b11000110,
        26: 0b11001001,
        27: 0b11001010,
        28: 0b11001100,
        29: 0b11010001,
        30: 0b11010010,
        31: 0b11010100
    }

    trigger_map = {
        0: 0b00101011,
        1: 0b00101011,
        2: 0b00101101,
        3: 0b00101110,
        4: 0b00110011,
        5: 0b00110101,
        6: 0b00110110,
        7: 0b00111001,
        8: 0b00111010,
        9: 0b00111100,
        10: 0b01001011,
        11: 0b01001101,
        12: 0b01001110,
        13: 0b01010011,
        14: 0b01010101,
        15: 0b01010110
    }

    CMD_GLOBAL_PULSE = 0b01011100
    CMD_CAL = 0b01100011
    CMD_REGISTER = 0b01100110
    CMD_RDREG = 0b01100101
    CMD_NULL = 0b01101001
    CMD_ECR = 0b01011010
    CMD_BCR = 0b01011001
    CMD_SYNCH = 0b10000001
    CMD_SYNCL = 0b01111110
    CMD_SYNC = [0b10000001, 0b01111110]  # 0x(817E)

    ''' USER_K word identification '''
    kword_map = {
        0: 'MM',  # = 0xD2
        1: 'MA',  # = 0x99
        2: 'AM',  # = 0x55
        3: 'AA',  # = 0xB4
        4: 'EE'  # = 0xCC
    }

    ''' Map hardware IDs for board identification '''
    hw_map = {
        0: 'SIMULATION',
        1: 'BDAQ53',
        2: 'USBPix3',
        3: 'KC705',
        4: 'GENESYS 2'
    }

    hw_con_map = {
        0: 'SMA',
        1: 'FMC_LPC',
        2: 'FMC_HPC',
        3: 'Displayport'

    }

    ''' Options concerning the readout hardware '''
    board_options_map = {
        '640Mbps': 0x01
    }

    macro_regs = [{'columns': range(0, 32),
                   'macro_columns': range(0, 16),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_1'},

                  {'columns': range(32, 64),
                   'macro_columns': range(16, 32),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_2'},

                  {'columns': range(64, 96),
                   'macro_columns': range(32, 48),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_3'},

                  {'columns': range(96, 128),
                   'macro_columns': range(48, 64),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_4'},

                  {'columns': range(128, 160),
                   'macro_columns': range(64, 80),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_1'},

                  {'columns': range(160, 192),
                   'macro_columns': range(80, 96),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_2'},

                  {'columns': range(192, 224),
                   'macro_columns': range(96, 112),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_3'},

                  {'columns': range(224, 256),
                   'macro_columns': range(112, 128),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_4'},

                  {'columns': range(256, 264),
                   'macro_columns': range(128, 132),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_5'},

                  {'columns': range(264, 296),
                   'macro_columns': range(132, 148),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_1'},

                  {'columns': range(296, 328),
                   'macro_columns': range(148, 164),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_2'},

                  {'columns': range(328, 360),
                   'macro_columns': range(164, 180),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_3'},

                  {'columns': range(360, 392),
                   'macro_columns': range(180, 196),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_4'},

                  {'columns': range(392, 400),
                   'macro_columns': range(196, 200),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_5'}]

    core_regs = [{'columns': range(0, 128),
                  'core_columns': range(0, 16),
                  'core_name': 'EN_CORE_COL_SYNC'},

                 {'columns': range(128, 256),
                  'core_columns': range(16, 32),
                  'core_name': 'EN_CORE_COL_LIN_1'},

                 {'columns': range(256, 264),
                  'core_columns': range(32, 33),
                  'core_name': 'EN_CORE_COL_LIN_2'},

                 {'columns': range(264, 392),
                  'core_columns': range(33, 49),
                  'core_name': 'EN_CORE_COL_DIFF_1'},

                 {'columns': range(392, 400),
                  'core_columns': range(49, 50),
                  'core_name': 'EN_CORE_COL_DIFF_2'}]

    default_dac_values = {'IBIASP1_SYNC': 100,
                          'IBIASP2_SYNC': 150,
                          'IBIAS_SF_SYNC': 100,
                          'IBIAS_KRUM_SYNC': 140,
                          'IBIAS_DISC_SYNC': 200,
                          'ICTRL_SYNCT_SYNC': 100,
                          'VBL_SYNC': 450,
                          'VTH_SYNC': 300,
                          'VREF_KRUM_SYNC': 490,
                          'CONF_FE_SYNC': 0x0002,
                          'PA_IN_BIAS_LIN': 300,
                          'FC_BIAS_LIN': 20,
                          'KRUM_CURR_LIN': 50,
                          'LDAC_LIN': 80,
                          'COMP_LIN': 110,
                          'REF_KRUM_LIN': 300,
                          'Vthreshold_LIN': 408,
                          'PRMP_DIFF': 533,
                          'FOL_DIFF': 542,
                          'PRECOMP_DIFF': 551,
                          'COMP_DIFF': 528,
                          'VFF_DIFF': 164,
                          'VTH1_DIFF': 1023,
                          'VTH2_DIFF': 0,
                          'LCC_DIFF': 20,
                          'CONF_FE_DIFF': 10}

    voltage_mux = {0: 'ADCbandgap',
                   1: 'CAL_MED_left',
                   2: 'CAL_HI_left',
                   3: 'TEMPSENS_1',
                   4: 'RADSENS_1',
                   5: 'TEMPSENS_2',
                   6: 'RADSENS_2',
                   7: 'TEMPSENS_4',
                   8: 'RADSENS_4',
                   9: 'VREF_VDAC',
                   10: 'VOUT_BG',
                   11: 'IMUX_out',
                   12: 'VCAL_MED',
                   13: 'VCAL_HIGH',
                   14: 'RADSENS_3',
                   15: 'TEMPSENS_3',
                   16: 'REF_KRUM_LIN',
                   17: 'Vthreshold_LIN',
                   18: 'VTH_SYNC',
                   19: 'VBL_SYNC',
                   20: 'VREF_KRUM_SYNC',
                   21: 'VTH_HI_DIFF',
                   22: 'VTH_LO_DIFF',
                   23: 'VIN_Ana_SLDO',
                   24: 'VOUT_Ana_SLDO',
                   25: 'VREF_Ana_SLDO',
                   26: 'VOFF_Ana_SLDO',
                   27: 'ground',
                   28: 'ground1',
                   29: 'VIN_Dig_SLDO',
                   30: 'VOUT_Dig_SLDO',
                   31: 'VREF_Dig_SLDO',
                   32: 'VOFF_Dig_SLDO',
                   33: 'ground2'}

    current_mux = {0: 'Iref',
                   1: 'IBIASP1_SYNC',
                   2: 'IBIASP2_SYNC',
                   3: 'IBIAS_DISC_SYNC',
                   4: 'IBIAS_SF_SYNC',
                   5: 'ICTRL_SYNCT_SYNC',
                   6: 'IBIAS_KRUM_SYNC',
                   7: 'COMP_LIN',
                   8: 'FC_BIAS_LIN',
                   9: 'KRUM_CURR_LIN',
                   10: 'LDAC_LIN',
                   11: 'PA_IN_BIAS_LIN',
                   12: 'COMP_DIFF',
                   13: 'PRECOMP_DIFF',
                   14: 'FOL_DIFF',
                   15: 'PRMP_DIFF',
                   16: 'LCC_DIFF',
                   17: 'VFF_DIFF',
                   18: 'VTH1_DIFF',
                   19: 'VTH2_DIFF',
                   20: 'CDR_CP_IBIAS',
                   21: 'VCO_BUFF_BIAS',
                   22: 'VCO_IBIAS',
                   23: 'CML_TAP_BIAS0',
                   24: 'CML_TAP_BIAS1',
                   25: 'CML_TAP_BIAS2'}

    def __init__(self, conf=None, **kwargs):
        self.rp = RD53ARegisterParser(**kwargs)
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if not conf:
            conf = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'bdaq53.yaml')

        logger.debug("Loading configuration file from %s" % conf)

        self.reset_masks()
        super(RD53A, self).__init__(conf)

    def get_daq_version(self):
        ret = self['intf'].read(0x0000, 2)
        fw_version = str('%s.%s' % (ret[1], ret[0]))

        ret = self['intf'].read(0x0002, 2)
        board_version = self.hw_map[ret[0] + (ret[1] << 8)]

        ret = self['intf'].read(0x0004, 1)
        board_options = ret[0]

        ret = self['intf'].read(0x0005, 2)
        connector_version = self.hw_con_map[ret[0] + (ret[1] << 8)]

        return fw_version, board_version, board_options, connector_version

    def init(self, **kwargs):
        super(RD53A, self).init()

        self.fw_version, self.board_version, self.board_options, self.connector_version = self.get_daq_version()
        logger.success('Found board %s with %s running firmware version %s' % (self.board_version, self.connector_version, self.fw_version))
        if self.connector_version == 'FMC_LPC':
            T= self._measure_temperature_ntc_CERNFMC()
            logger.info("Temperature measure with NTC is: %f C", T)
        if self.fw_version != VERSION[:3]:  # Compare only the first two digits
            raise Exception("Firmware version (%s) is different than software version (%s)! Please update." % (self.fw_version, VERSION))

        if self.board_version == 'BDAQ53' or self.board_version == 'USBPix3':
            if self['rx'].get_Si570_is_configured() is False:
                import si570
                si570_conf = {'name': 'si570', 'type': 'bdaq53.si570', 'interface': 'intf', 'base_addr': 0xba, 'init': {'frequency': 160.0}}
                bdaq53a_clk_gen = si570.si570(self['i2c'], si570_conf)
                self['cmd'].set_output_en(False)
                self['rx'].reset()
                time.sleep(0.1)
                bdaq53a_clk_gen.init()
                time.sleep(0.1)
                self['cmd'].set_output_en(True)
                self['rx'].set_Si570_is_configured(True)
            else:
                logger.info('Si570 oscillator is already configured')
        elif self.board_version == 'KC705':
            self._setup_kc705_si5324(**kwargs)
        elif self.board_version == 'SIMULATION':
            pass

    def init_communication(self):
        logger.info('Initializing communication with chip...')

        # Configure cmd encoder
        self['cmd'].reset()
        time.sleep(0.1)
        self.write_command(self.write_sync(write=False) * 32)
        # Wait for PLL lock
        self.wait_for_pll_lock()

        aurora_data = self.set_aurora(write=True)

        # Workaround for problems locking
        for _ in range(30):
            self.write_command(self.write_sync(write=False) * 32)
            self.write_ecr(write=True)

            self.write_command(aurora_data)
            time.sleep(0.01)
            try:
                self.wait_for_aurora_sync()
            except RuntimeError:
                pass
            else:
                break

            self.write_command([0x00] * 1000, repetitions=1)
            time.sleep(0.01)
        else:
            self.wait_for_aurora_sync()

        logger.success('Communication established')

    def write_global_pulse(self, width, chip_id=8, write=True):
        # 0101_1100    ChipId<3:0>,0    Width<3:0>,0
        indata = [self.CMD_GLOBAL_PULSE] * 2  # [0b01011100]
        chip_id_bits = chip_id << 1
        indata += [self.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.cmd_data_map[width_bits]]

        if write:
            self.write_command(indata)

        return indata

    def write_cal(self, cal_edge_mode=0, cal_edge_width=10, cal_edge_dly=2, cal_aux_value=0, cal_aux_dly=0, chip_id=8, write=True):
        '''
            Command to send a digital or analog injection to the chip.
            Digital or analog injection is selected globally via the INJECTION_SELECT register.

            For digital injection, only CAL_edge signal is relevant:
                - CAL_edge_mode switches between step (0) and pulse (0) mode
                - CAL_edge_dly is counted in bunch crossings. It sets the delay before the rising edge of the signal
                - CAL_edge_width is the duration of the pulse (only in pulse mode) and is counted in cycles of the 160MHz clock
            For analog injection, the CAL_aux signal is used as well:
                - CAL_aux_value is the value of the CAL_aux signal
                - CAL_aux_dly is counted in cycles of the 160MHz clock and sets the delay before the edge of the signal

            {ChipId[3:0],CalEdgeMode, CalEdgeDelay[2:0],CalEdgeWidth[5:4]}{CalEdgeWidth[3:0],CalAuxMode, CalAuxDly[4:0]}
        '''
        indata = [self.CMD_CAL] * 2
        chip_id_bits = chip_id << 1
        indata += [self.cmd_data_map[(chip_id_bits + cal_edge_mode)]]
        cal_edge_dly_bits = BitLogic.from_value(0, size=3)
        cal_edge_dly_bits[:] = cal_edge_dly
        cal_edge_width_bits = BitLogic.from_value(0, size=6)
        cal_edge_width_bits[:] = cal_edge_width
        cal_aux_dly_bits = BitLogic.from_value(0, size=5)
        cal_aux_dly_bits[:] = cal_aux_dly

        indata += [self.cmd_data_map[(cal_edge_dly_bits[2:0].tovalue() << 2) + cal_edge_width_bits[5:4].tovalue()]]
        indata += [self.cmd_data_map[(cal_edge_width_bits[3:0].tovalue() << 1) + cal_aux_value]]
        indata += [self.cmd_data_map[cal_aux_dly_bits[4:0].tovalue()]]

        if write:
            self.write_command(indata)

        return indata

    def read_register(self, register, chip_id=8, write=True):
        if type(register) is str:
            register = self.rp.get_address(register)

        indata = [self.CMD_RDREG] * 2
        chip_id_bits = chip_id << 1
        indata += [self.cmd_data_map[chip_id_bits]]
        addr_bits = BitLogic.from_value(0, size=9 + 6)
        addr_bits[14:6] = register
        indata += [self.cmd_data_map[addr_bits[14:10].tovalue()]]
        indata += [self.cmd_data_map[addr_bits[9:5].tovalue()]]
        indata += [self.cmd_data_map[addr_bits[4:0].tovalue()]]
        if write:
            self.write_command(indata)
        return indata

    def write_register(self, register, data, chip_id=8, write=True):
        '''
            Sends write command to register with data

            Parameters:
            ----------
                register : str or int
                    The name or address of the register to be written to

            Returns:
            ----------
                indata : binarray
                    Boolean representation of register write command.
        '''
        if type(register) is str:
            address = self.rp.get_address(register)
        else:
            address = register

        if np.issubdtype(type(data), np.signedinteger):
            bits = 9 + 16
            data_to_send = data | (address << 16)
            bits_to_send = "{0:025b}".format(data_to_send)
            wr_reg_mode = 0
        else:
            if(len(data) != 6):
                raise ValueError('Error while writing data %s to register %s:\n Command data size was not 1 or 6. It was: %s' % (data, register, len(data)))

            wr_reg_mode = 1
            bits = 9 + len(data) * 16

            data_to_send = (address << 6 * 16) | (data[0] << 5 * 16) | (data[1] << 4 * 16) | (data[2] << 3 * 16) | (data[3] << 2 * 16) | (data[4] << 1 * 16) | data[5]
            bits_to_send = "{0:0105b}".format(data_to_send)

        indata = [self.CMD_REGISTER] * 2
        chip_id_bits = chip_id << 1
        header = chip_id_bits + wr_reg_mode
        indata += [self.cmd_data_map[header]]

        for b in range(bits / 5):
            v = '0b' + bits_to_send[b * 5:b * 5 + 5]
            indata += [self.cmd_data_map[int(v, 2)]]

        if write:
            self.write_command(indata)

        return indata

    def write_null(self, chip_id=0, write=True):
        indata = [self.CMD_NULL] * 2  # [0b01101001]
        if write:
            self.write_command(indata)
        return indata

    def write_ecr(self, write=True):
        indata = [self.CMD_ECR] * 2  # [0b01011010]
        if write:
            self.write_command(indata)
        return indata

    def write_bcr(self, write=True):
        indata = [self.CMD_BCR] * 2  # [0b01011001]
        if write:
            self.write_command(indata)
        return indata

    def write_sync(self, write=True):
        indata = [self.CMD_SYNCH]
        indata += [self.CMD_SYNCL]
        if write:
            self.write_command(indata)
        return indata

    def write_sync_01(self, write=True):
        indata = [0b01010101]
        indata += [0b01010101]
        if write:
            self.write_command(indata)
        return indata

    def send_trigger(self, trigger, tag=0, write=True):
        # Trigger is always followed by 5 Data bits
        indata = [self.trigger_map[trigger]]
        indata += [self.cmd_data_map[tag]]
        if write:
            self.write_command(indata)
        return indata

    def get_trigger_counter(self):
        return self['tlu']['TRIGGER_COUNTER']

    def set_tlu_module(self, trigger_enable):
        self['tlu']['TRIGGER_ENABLE'] = trigger_enable

    def set_trigger_data_delay(self, trigger_data_delay):
        self['tlu']['TRIGGER_DATA_DELAY'] = trigger_data_delay

    def configure_tlu_module(self, **kwargs):
        # Reset first TLU module
        self['tlu']['RESET'] = 1
        # Set specified registers
        for key, value in kwargs['TRIGGER'].iteritems():
            self['tlu'][key] = value
        # Set maximum number of triggers
        if kwargs['max_triggers']:
            self['tlu']['MAX_TRIGGERS'] = kwargs['max_triggers']
        else:
            # unlimited number of triggers
            self['tlu']['MAX_TRIGGERS'] = 0

    def get_tlu_erros(self):
        return (self['tlu']['TRIGGER_LOW_TIMEOUT_ERROR_COUNTER'], self['tlu']['TLU_TRIGGER_ACCEPT_ERROR_COUNTER'])

    def configure_trigger_cmd_pulse(self, **kwargs):
        # configures pulse which is sent to CMD for incoming triggers; factor 4 is needed for conversion from 160 MHz to 40 MHz (BC)
        self['pulser_trig'].set_en(True)
        self['pulser_trig'].set_width(kwargs['trigger_length'] * 4)
        self['pulser_trig'].set_delay(kwargs['trigger_delay'] * 4)
        self['pulser_trig'].set_repeat(1)

    def configure_tlu_veto_pulse(self, **kwargs):
        # configures pulse for veto of new triggers; factor 4 is needed for conversion from 160 MHz to 40 MHz (BC)
        self['pulser_veto'].set_en(True)
        self['pulser_veto'].set_width(4)
        self['pulser_veto'].set_delay(kwargs['veto_length'] * 4)
        self['pulser_veto'].set_repeat(1)

    def send_trigger_tag(self, trigger, trigger_tag, write=True):
        if trigger == 0:
            logger.error("Illegal trigger number")
            return
        else:
            indata = [self.trigger_map[trigger]]
            indata += [trigger_tag]
            if write:
                self.write_command(indata)
            return indata

    def write_command(self, data, repetitions=1, wait_for_done=True):
        '''
            Write data to the command encoder.

            Parameters:
            ----------
                data : list
                    Up to [get_cmd_size()] bytes
                repetitions : integer
                    Sets repetitions of the current request. 1...2^16-1. Default value = 1.
                wait_for_done : boolean
                    Wait for completion after sending the command. Not advisable in case of repetition mode.
        '''

        if isinstance(data[0], list):
            for indata in data:
                self.write_command(indata, repetitions, wait_for_done)
            return

        assert (0 < repetitions < 65536), "Repetition value must be 0<n<2^16"
        if repetitions > 1:
            logger.debug("Repeating command %i times." % (repetitions))

        self['cmd'].set_data(data)
        self['cmd'].set_size(len(data))
        self['cmd'].set_repetitions(repetitions)
        self['cmd'].start()

        if wait_for_done:
            while (not self['cmd'].is_done()):
                pass

    def set_global_pulse_route(self, bitnames, global_pulse_route_file=None):
        '''
            Used to set a single or multiple bits of GLOBAL_PULSE_ROUTE register by name, using names defined in global_pulse_route.yaml

            Parameters:
            ----------
                bitnames : str or list of str
                    Name(s) of the bit(s) to be set to 1
        '''

        if not global_pulse_route_file:
            global_pulse_route_file = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'global_pulse_route.yaml')

        with open(global_pulse_route_file, 'r') as infile:
            global_pulse_route_map = yaml.load(infile)

        if type(bitnames) == str:
            bitnames = [bitnames]

        listofnames = [bit['name'] for bit in global_pulse_route_map]

        for bitname in bitnames:
            if bitname not in listofnames:
                logger.warning('Could not find %s in possible values for register GLOBAL_PULSE_ROUTE' % bitname)

        val = ''
        for i in range(16):
            if global_pulse_route_map[i]['name'] in bitnames:
                val += '1'
            else:
                val += '0'

        self.write_register('GLOBAL_PULSE_ROUTE', val, write=True)

    def set_aurora(self, tx_lanes=1, CB_Wait=255, CB_Send=1, chip_id=8, only_cb=False, enable_monitor=False, bypass_mode=False, write=False, **kwargs):
        indata = self.write_sync(write=False) * 32

        if bypass_mode is True:
            if self.board_version == 'KC705'and self.connector_version == 'FMC_LPC' and self.board_options & self.board_options_map['640Mbps']:
                self['cmd'].set_bypass_mode(True)
                logger.info("Switching chip and FMC card to BYPASS MODE @640Mb/s")
                indata += self.write_register(
                    register='CDR_CONFIG',
                    data=int(
                        '0b'
                        + '1'  # CDR_SEL_DEL_CLK ***
                        + '00'  # CDR_PD_SEL[1:0]
                        + '0100'  # CDR_PD_DEL[3:0]
                        + '0'  # CDR_EN_GCK2
                        + '011'  # CDR_VCO_GAIN[2:0]
                        + '111',  # CDR_SEL_SER_CLK[2:0] ***
                        2),
                    chip_id=chip_id, write=False)
            else:
                logger.error('Bypass mode is only supported for the KC705+FMC_LPC readout hardware @ 640 Mb/s')
        elif self.board_options & self.board_options_map['640Mbps']:
            self['cmd'].set_bypass_mode(False)
            logger.info("Aurora receiver running at 640Mb/s")
            indata += self.write_register(
                register='CDR_CONFIG',
                data=int(
                    '0b'
                    + '0'  # CDR_SEL_DEL_CLK
                    + '00'  # CDR_PD_SEL[1:0]
                    + '0100'  # CDR_PD_DEL[3:0]
                    + '0'  # CDR_EN_GCK2
                    + '011'  # CDR_VCO_GAIN[2:0]
                    + '001',  # CDR_SEL_SER_CLK[2:0]
                    2),
                chip_id=chip_id, write=False)
        else:
            logger.info("Aurora receiver running at 1.28Gb/s")
            self['cmd'].set_bypass_mode(False)

        if only_cb is False:
            logger.debug("Aurora settings: Lanes: TX=%u, CB_Wait=%u, CB_Send=%u", tx_lanes, CB_Wait, CB_Send)
            if tx_lanes == 4:
                logger.info("4 Aurora lanes active")
                # Sets 4-lane-mode
                indata += self.write_register(register='OUTPUT_CONFIG', data=0b00111100, chip_id=chip_id, write=False)
                # Enable 4 CML outputs
                indata += self.write_register(register='CML_CONFIG', data=0b00001111, chip_id=chip_id, write=False)
            elif tx_lanes == 2:
                logger.info("2 Aurora lanes active")
                # Sets 2-lane-mode
                indata += self.write_register(register='OUTPUT_CONFIG', data=0b00001100, chip_id=chip_id, write=False)
                # Enable 2 CML outputs
                indata += self.write_register(register='CML_CONFIG', data=0b00000011, chip_id=chip_id, write=False)
            elif tx_lanes == 1:
                logger.info("1 Aurora lane active")
                # Sets 2-lane-mode
                indata += self.write_register(register='OUTPUT_CONFIG', data=0b00000100, chip_id=chip_id, write=False)
                # Enable 2 CML outputs
                indata += self.write_register(register='CML_CONFIG', data=0b00000001, chip_id=chip_id, write=False)
            else:
                logger.error("Aurora lane configuration (1,2,4) must be specified")
        else:
            logger.debug("Aurora settings: CB_Wait=%u, CB_Send=%u", CB_Wait, CB_Send)

        # Set CB frame distance and number
        indata += self.write_register(register='AURORA_CB_CONFIG0', data=((CB_Wait << 4) | CB_Send & 0x0f) & 0xff, chip_id=chip_id, write=False)
        indata += self.write_register(register='AURORA_CB_CONFIG1', data=(CB_Wait & 0xfffff) >> 4, chip_id=chip_id, write=False)  # Set CB frame distance and number

        # Reset Aurora by setting global pulse route and sending a pulse
        indata += self.write_register(register='GLOBAL_PULSE_ROUTE', data=0x30, chip_id=chip_id, write=False)
        indata += self.write_global_pulse(width=14, chip_id=chip_id, write=False)

        if enable_monitor:
            indata += self.write_register(register='GLOBAL_PULSE_ROUTE', data=0x0100, chip_id=chip_id, write=False)
            indata += self.write_global_pulse(width=4, chip_id=chip_id, write=False)
            logger.info("Monitor output enabled")

        if write:
            self.write_command(indata)
        return indata

    def wait_for_aurora_sync(self, timeout=1000):
        logger.debug("Waiting for Aurora sync...")
        times = 0

        while times < timeout and self['rx'].get_rx_ready() == 0:
            times += 1

        if self['rx'].get_rx_ready() == 1:
            logger.debug("Aurora link synchronized")
            return True
        else:
            self['cmd'].reset()
            raise RuntimeError('Timeout while waiting for Aurora Sync.')

    def wait_for_pll_lock(self, timeout=1000):
        logger.debug("Waiting for PLL lock...")
        times = 0

        while times < timeout and self['rx'].get_pll_locked() == 0:
            times += 1

        if self['rx'].get_pll_locked() == 1:
            logger.debug("PLL locked")
            return True
        else:
            raise RuntimeError('Timeout while waiting for PLL to lock.')

    def reset_chip(self):
        # Do not reset registers for now, since it makes some inexplicable problems
        # self.reset_registers()

        self.write_register(register='GLOBAL_PULSE_ROUTE', data=0b0100000000000000, write=True)  # Special treatment for synchronous FrontEnd
        self.write_global_pulse(width=14, write=True)  # Special treatment for synchronous FrontEnd

        self.enable_core_col_clock(range(50))  # Enable clock on full chip
        self.enable_macro_col_cal(range(200))  # Enable analog calibration on full chip

        self.reset_masks()
        if self.board_version != 'SIMULATION':  # it is  slow and not needed for simualation
            self.write_masks()

        self.set_dacs()

    def reset_registers(self):
        '''
        Sets all registers to default
        '''
        indata = self.write_sync_01(write=False) * 10
        for reg in self.rp.register_map[1:]:
            address = int(reg['address'], 16)
            default = int(self.rp.get_default(address), 2)
            indata += self.write_register(reg['address'], default, write=False)
        indata += self.write_sync_01(write=False) * 10
        self.write_command(indata)

    def reset_masks(self, enable=True, injection=True, hitbus=True, tdac=True, lin_gain_sel=True):
        '''
            Resets all masks to default (False / 0)
        '''

        if enable:
            self.enable_mask = np.zeros((400, 192), dtype=bool)
        if injection:
            self.injection_mask = np.zeros((400, 192), dtype=bool)
        if hitbus:
            self.hitbus_mask = np.zeros((400, 192), dtype=bool)
        if tdac:
            self.tdac_mask = np.zeros((400, 192), dtype=int)
        if lin_gain_sel:
            self.lin_gain_sel_mask = np.ones((400, 192), dtype=int)

    def good_pixel_mask_diff(self):
        '''
        This function disables pixels with too high parasitic capacitance on the comparator output
        in the differntial front end. The matrix represents one digital core
        with 1 indicating enabled pixels and 0 indicating disabled pixels.
        '''

        for col in range(264, 400, 8):
            for row in range(0, 192, 8):
                self.enable_mask[col:col + 8, row:row + 8] = np.transpose([[0, 0, 1, 1, 0, 0, 0, 0],
                                                                           [0, 0, 0, 1, 0, 0, 0, 0],
                                                                           [0, 0, 0, 1, 0, 0, 0, 0],
                                                                           [0, 0, 0, 1, 1, 0, 0, 0],
                                                                           [0, 0, 0, 1, 1, 0, 0, 0],
                                                                           [0, 0, 0, 1, 1, 0, 0, 0],
                                                                           [0, 0, 0, 1, 0, 0, 0, 0],
                                                                           [0, 0, 0, 0, 1, 0, 0, 0]])

    def inject_digital(self, cal_edge_width=10, cal_edge_dly=2, fine_delay=9, latency=121, repetitions=1):
        '''
            Injects a digital pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
                repetitions : int
                    Number of times, the injection command is repeated, i.e. number of injections
        '''

        data = int('0b01' + str(bin(fine_delay))[2:], 2)
        indata = self.write_register(register='INJECTION_SELECT', data=data, write=True)  # Enable digital injection with zero delay

        indata += self.write_sync_01(write=False) * 10
        indata += self.write_cal(cal_edge_width=cal_edge_width, cal_edge_dly=cal_edge_dly, cal_edge_mode=1, write=False)  # Injection
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        indata += self.send_trigger(trigger=0b1111, write=False) * 8  # Trigger
        if self.board_version != 'SIMULATION':
            indata += self.write_sync_01(write=False) * 800  # Wait for data
        self.write_command(indata, repetitions=repetitions)

    def setup_analog_injection(self, vcal_high, vcal_med, fine_delay=9):
        data = int('0b00' + str(bin(fine_delay))[2:], 2)
        indata = self.write_register(register='INJECTION_SELECT', data=data, write=False)  # Enable analog injection in uniform mode with configured fine delay
        indata += self.write_register(register='VCAL_HIGH', data=vcal_high, write=False)  # Set VCAL_HIGH
        indata += self.write_register(register='VCAL_MED', data=vcal_med, write=False)  # Set VCAL_MED
        indata += self.write_register(register='GLOBAL_PULSE_ROUTE', data=0b0100000000000000, write=False)  # Special treatment for synchronous FrontEnd
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, write=False)  # CalEdge -> 0
        self.write_command(indata)

    def inject_analog_single(self, repetitions=1, latency=122, wait_cycles=400, send_ecr=False, write=True):
        '''
            Injects a single analog pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger
        '''

        indata = self.write_sync_01(write=False)

        if send_ecr:
            indata += self.write_global_pulse(width=8, write=False)
            indata += self.write_ecr(write=False)
            indata += self.write_sync_01(write=False) * 60

        indata += self.write_cal(cal_edge_mode=0, cal_edge_width=1, cal_edge_dly=0, write=False)  # CalEdge -> 1 (inject)
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        indata += self.send_trigger(trigger=0b1111, write=False) * 8  # Trigger
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, write=False)  # CalEdge -> 0

        if self.board_version != 'SIMULATION':
            indata += self.write_sync_01(write=False) * wait_cycles  # Wait for data

        if write:
            self.write_command(indata, repetitions=repetitions)

        return indata

    def toggle_output_select(self, fine_delay=9, wait_cycles=400, latency=121, repetitions=1):
        '''
            Toggles between digital and analog injection. If the comparator is stuck high
            this creates a transient on hit_t and thus a hit.

            Note
            ----
            Chip implementation:
            assign hit_t = EnDigHit ? (CalEdge & cal_en) : hit ;
            assign HitOut = hit_t & hit_en;

            See also Figure 29 in RD53 manual.

            Parameters:
            ----------
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
        '''

        # Enable digital injection = falling edge for stuck high pixels
        indata = self.write_register(register='INJECTION_SELECT', data=int('0b01' + str(bin(fine_delay))[2:], 2), write=False)
        indata += self.write_sync_01(write=False) * 10
        # Enable analog injection, this creates a rising edge if comparator output is stuck high
        indata += self.write_register(register='INJECTION_SELECT', data=int('0b00' + str(bin(fine_delay))[2:], 2), write=True)
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        indata += self.send_trigger(trigger=0b1111, write=False) * 8  # Trigger
        indata += self.write_sync_01(write=False) * wait_cycles
        self.write_command(indata, repetitions=repetitions)

    def enable_core_col_clock(self, core_cols=None, write=True):
        '''
            Enable clocking of given core columns. After POR everything is disabled.

            ----------
            Parameters:
                core_cols : list of int
                    A list of core columns to enable. Default = None disables everything
        '''

        # Disable everything
        indata = self.write_sync_01(write=False)
        indata += self.write_register(register='EN_CORE_COL_SYNC', data=0, write=False)
        indata += self.write_register(register='EN_CORE_COL_LIN_1', data=0, write=False)
        indata += self.write_register(register='EN_CORE_COL_LIN_2', data=0, write=False)
        indata += self.write_register(register='EN_CORE_COL_DIFF_1', data=0, write=False)
        indata += self.write_register(register='EN_CORE_COL_DIFF_2', data=0, write=False)

        if core_cols:
            indata += self.write_sync_01(write=False)
            for r in self.core_regs:
                bits = []
                for i, core_col in enumerate(r['core_columns']):
                    if core_col in core_cols:
                        bits.append(i)

                if not bits:
                    continue
                bits = list(set(bits))  # Remove duplicates

                data = ''
                for i in range(len(r['core_columns'])):
                    data += '1' if i in bits else '0'

                indata += self.write_register(register=r['core_name'], data=int(data[::-1], 2), write=False)

        if write:
            self.write_command(indata)

        return indata

    def enable_macro_col_cal(self, macro_cols=None):
        '''
            Enable analog calibration of given macro (double-) columns. After POR everything is enabled.

            ----------
            Parameters:
                macro_cols : list of int
                    A list of macro columns to enable. Default = None disables everything
        '''

        # Disable everything
        indata = self.write_sync_01(write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_SYNC_1', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_SYNC_2', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_SYNC_3', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_SYNC_4', data=0, write=False)

        indata += self.write_register(register='EN_MACRO_COL_CAL_LIN_1', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_LIN_2', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_LIN_3', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_LIN_4', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_LIN_5', data=0, write=False)

        indata += self.write_register(register='EN_MACRO_COL_CAL_DIFF_1', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_DIFF_2', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_DIFF_3', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_DIFF_4', data=0, write=False)
        indata += self.write_register(register='EN_MACRO_COL_CAL_DIFF_5', data=0, write=False)
        self.write_command(indata)

        if not macro_cols:
            return

        # Enable given columns
        indata = self.write_sync_01(write=False)
        for r in self.macro_regs:
            bits = []
            for i, macro_col in enumerate(r['macro_columns']):
                if macro_col in macro_cols:
                    bits.append(i)

            if not bits:
                continue
            bits = list(set(bits))  # Remove duplicates

            data = ''
            for i in range(len(r['macro_columns'])):
                data += '1' if i in bits else '0'

            indata += self.write_register(register=r['macro_name'], data=int(data[::-1], 2), write=False)

        self.write_command(indata)

    def get_pixel_config(self, column, row):
        enable = str(int(self.enable_mask[column, row]))
        injection_enable = str(int(self.injection_mask[column, row]))
        hitbus_enable = str(int(self.hitbus_mask[column, row]))

        if column >= 128 and column < 264:
            if self.tdac_mask[column, row] < 0:
                self.tdac_mask[column, row] = 0
            tdac = str(bin(self.lin_gain_sel_mask[column, row]))[2:] + str(bin(self.tdac_mask[column, row]))[2:].zfill(4)
        elif column >= 264:
            if self.tdac_mask[column, row] >= 0:
                tdac = str(bin(self.tdac_mask[column, row]))[2:].zfill(5)
            else:
                tdac = '1' + str(bin(abs(self.tdac_mask[column, row])))[2:].zfill(4)
        else:
            tdac = '00000'

        return tdac + hitbus_enable + injection_enable + enable

    def write_double_pixel(self, double_column, row):  # double column?

        indata = self.write_register(register=2, data=row, write=False)  # 'REGION_ROW'
        indata += self.write_register(register=1, data=double_column, write=False)  # 'REGION_COL'

        column = double_column * 2

        pixel_data = '0b' + self.get_pixel_config(column + 1, row) + self.get_pixel_config(column, row)
        indata += self.write_register(register=0, data=int(pixel_data, 2), write=False)  # 'PIX_PORTAL'

        return indata

    def write_masks(self, columns=range(400), write=True):
        '''
            Apply global masks to selected columns.
            In principle, you can always write to all columns, since the information is saved in the mask, but this is very slow.
            To speed things up, you should only write the masks to the necessary columns.

            ----------
            Parameters:
                columns : list of int
                    A list of the columns to write the masks to
        '''

        # TODO: Possible speedup using broadcast mode?

        logger.debug('Writing masks...')
        # Disable broadcast, enable auto-row
        indata = self.write_register(register='PIX_MODE', data=0b0000000000001000, write=False)
        # Disable default config
        indata += self.write_register(register='PIX_DEFAULT_CONFIG', data=0, write=False)
        self.write_command(indata)

        out_data = []

        for column in columns:
            pixel_index = 0 if column % 2 == 0 else 1

            if pixel_index == 0:
                double_column = True if column + 1 in columns else False
            else:
                double_column = False
                if column - 1 in columns:
                    continue

            pair_index = 0 if column % 4 < 2 else 1
            region_in_core_column = 0 if column % 8 < 4 else 1
            core_column = int(column / 8)

            # 6'b core_column + 1'b region_in_core_column + 1'b pair_index
            column_data = '0b' + str(bin(core_column))[2:].zfill(6) + str(region_in_core_column) + str(pair_index)

            indata = self.write_register(register='REGION_ROW', data=0, write=False)
            indata += self.write_register(register='REGION_COL', data=int(column_data, 2), write=False)

            mask = []
            for row in range(192):
                enable = str(int(self.enable_mask[column, row]))
                injection_enable = str(int(self.injection_mask[column, row]))
                hitbus_enable = str(int(self.hitbus_mask[column, row]))
                if column in range(128, 264):
                    if self.tdac_mask[column, row] < 0:
                        self.tdac_mask[column, row] = 0
                    tdac = str(bin(self.lin_gain_sel_mask[column, row]))[2:] + str(bin(self.tdac_mask[column, row]))[2:].zfill(4)
                elif column in range(264, 400):
                    if self.tdac_mask[column, row] >= 0:
                        tdac = str(bin(self.tdac_mask[column, row]))[2:].zfill(5)
                    else:
                        tdac = '1' + str(bin(abs(self.tdac_mask[column, row])))[2:].zfill(4)
                else:
                    if self.tdac_mask[column, row] < 0:
                        self.tdac_mask[column, row] = 0
                    tdac = str(bin(self.tdac_mask[column, row]))[2:].zfill(5)

                pixel_data = '0b'
                if double_column:
                    dc_enable = str(int(self.enable_mask[column + 1, row]))
                    dc_injection_enable = str(int(self.injection_mask[column + 1, row]))
                    dc_hitbus_enable = str(int(self.hitbus_mask[column + 1, row]))
                    if column in range(128, 264):
                        if self.tdac_mask[column + 1, row] < 0:
                            self.tdac_mask[column + 1, row] = 0
                        dc_tdac = str(bin(self.lin_gain_sel_mask[column + 1, row]))[2:] + str(bin(self.tdac_mask[column + 1, row]))[2:].zfill(4)
                    elif column in range(264, 400):
                        if self.tdac_mask[column + 1, row] >= 0:
                            dc_tdac = str(bin(self.tdac_mask[column + 1, row]))[2:].zfill(5)
                        else:
                            dc_tdac = '1' + str(bin(abs(self.tdac_mask[column + 1, row])))[2:].zfill(4)
                    else:
                        if self.tdac_mask[column + 1, row] < 0:
                            self.tdac_mask[column + 1, row] = 0
                        dc_tdac = str(bin(self.tdac_mask[column + 1, row]))[2:].zfill(5)
                    pixel_data += dc_tdac + dc_hitbus_enable + dc_injection_enable + dc_enable + tdac + hitbus_enable + injection_enable + enable
                else:
                    if pixel_index == 0:
                        pixel_data += '00000000' + tdac + hitbus_enable + injection_enable + enable
                    else:
                        pixel_data += tdac + hitbus_enable + injection_enable + enable + '00000000'

                mask.append(int(pixel_data, 2))

                if row % 6 == 5:
                    indata += self.write_register(register=0, data=mask, write=False)
                    mask = []

            if len(out_data) != 0 and (len(out_data[-1]) + len(indata)) < self['cmd'].get_mem_size():
                out_data[-1] = out_data[-1] + indata
            else:
                out_data.append(indata)

            indata = []

        if write:
            for indata in out_data:
                self.write_command(indata)

        return out_data

    def get_chip_status(self, timeout=10000):
        '''
            Returns a map of all important chip parameters.
            Can only be called before or after a scan!
        '''

        voltages = {}
        currents = {}

        logger.info('Recording chip status...')

        try:
            self.enable_monitor_filter()
            self.enable_monitor_data()

            for vmonitor in np.arange(0, 33, 1):
                self.get_ADC(typ='U', address=vmonitor)
                name = self.voltage_mux[vmonitor]
                for _ in range(timeout):
                    if self['FIFO'].get_FIFO_SIZE() > 0:
                        userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self['FIFO'].get_data()))
                        voltages[name] = userk_data['Data'][0]
                        if len(userk_data) > 0:
                            break
                else:
                    logger.error('Timeout while waiting for chip status.')

            for cmonitor in np.arange(0, 26, 1):
                self.get_ADC(typ='I', address=cmonitor)
                name = self.current_mux[cmonitor]
                for _ in range(timeout):
                    if self['FIFO'].get_FIFO_SIZE() > 0:
                        userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self['FIFO'].get_data()))
                        currents[name] = userk_data['Data'][0]
                        if len(userk_data) > 0:
                            break
                    time.sleep(0.1)
                else:
                    logger.error('Timeout while waiting for chip status.')
        except:
            logger.error('There was an error while receiving the chip status.')

        return voltages, currents

    def get_ADC(self, typ, address):
        '''
        Sends multiplexer settings and read command for Adc measurement

        Parameters:
        ----------
            typ : str
                Select either U or I depending on the value to be measured
                ('I' for Table 27 in RD53A manual and 'U' for Table 26)

            address: str or int
                The name or address of the register which is supposed to be measured

        Returns:
        ----------
            nothing
        '''

        def get_dict_key(dictionary, key):
            for address, names in dictionary.iteritems():
                if names == key:
                    return address
            raise ValueError('Given register could not be found.')

        if typ == 'U':
            if type(address) is str:
                address = get_dict_key(self.voltage_mux, address)
            bitstring = int('1' + format(32, '06b') + format(address, '07b'), 2)
        elif typ == 'I':
            if type(address) is str:
                address = get_dict_key(self.current_mux, address)
            bitstring = int('1' + format(address, '06b') + format(11, '07b'), 2)
        else:
            raise ValueError('Please select U for voltage measurement or I for current measurement')

        self.write_register(register='MONITOR_SELECT', data=bitstring, write=True)
        self._reset_ADC()
        self.read_register(register='MonitoringDataADC', write=True)

    def _enable_ADC(self):
        indata = self.write_register(register='GLOBAL_PULSE_ROUTE', data=0x1000, write=False)
        indata += self.write_global_pulse(width=4, write=False)
        self.write_command(indata)

    def _reset_ADC(self):
        indata = self.write_register(register='GLOBAL_PULSE_ROUTE', data=0x0080, write=False)
        indata += self.write_global_pulse(width=4, write=False)
        self.write_command(indata)
        time.sleep(0.01)
        self._enable_ADC()

    def set_dacs(self, **kwargs):
        self.dacs = {}

        indata = self.write_sync_01(write=False)
        for key in self.default_dac_values:
            self.dacs[key] = kwargs.get(key, self.default_dac_values[key])
            indata += self.write_register(register=key, data=self.dacs[key], write=False)
        self.write_command(indata)

    def set_tdac(self, **kwargs):
        TDAC = kwargs.get('TDAC')
        if TDAC is None:  # Set LIN to 7 and DIFF to 0 by default
            TDAC = np.zeros((400, 192), dtype=np.int8)
            TDAC[128:264, :] = 7  # LIN

        mask = np.zeros((400, 192), dtype=np.int8)
        mask[:, :] = TDAC

        try:
            maskfile = kwargs.get('maskfile')
            with tb.open_file(maskfile, 'r') as infile:
                logger.info('Loading TDAC mask from file: %s' % (maskfile))
                mask = infile.root.TDAC_mask[:]
        except (TypeError, NoSuchNodeError):
            pass

        self.tdac_mask[:, :] = mask

    def _setup_kc705_si5324(self, **kwargs):
        frequency = kwargs.get('aurora_ref', 160)

        # set I2C mux to Si5324 (bit 7)
        self['i2c'].write(0xe8, [0x80])

        def si5324_read(addr):
            self['i2c'].write(0xd0, [addr])
            return self['i2c'].read(0xd0, 1)[0]

        def si5324_write(addr, data):
            self['i2c'].write(0xd0, [addr, data & 0xff])

        # Based on: https://github.com/m-labs/si5324_test/blob/master/firmware/runtime/si5324.c
        self['i2c'].write(0xd0, [134])
        ident = struct.unpack(">H", bytearray(self['i2c'].read(0xd0, 2)))[0]
        if ident != 0x0182:
            raise ValueError("It is not Si5324 chip.")

        # To make PLL life easier should be based on lock status?
        LOS1_INT = (si5324_read(129) & 0x02) == 0
        if LOS1_INT:
            logger.debug('Si5324 is already programmed. Skipping clock configuration.')
            return

        # try:
        #    si5324_write(136, 0x80)  # bit 7 = RST_REG = 1: start of reset
        # except IOError:
        #    pass

        # Select XA/XB input
        si5324_write(0, si5324_read(0) | 0x40)  # Free running mode=1, CKOUT_ALWAYS_ON = 0
        si5324_write(11, 0x41)  # Disable CLKIN1
        si5324_write(6, 0x0F)  # Disable CKOUT2 (SFOUT2_REG=001), set CKOUT1 to LVDS (SFOUT1_REG=111)
        si5324_write(21, si5324_read(21) & 0xfe)  # CKSEL_PIN = 0
        si5324_write(3, 0x55)  # CKIN2 selected, SQ_ICAL=1

        # Calculated register values for Si5324 have to be modified in order to work!
        if frequency == 200:
            # For 200 MHz
            N1_HS = 7 - 4
            NC1_LS = 4 - 1
            N2_HS = 10 - 4
            N2_LS = 112000 - 1
            N32 = 22857 - 1
            BWSEL = 2

        elif frequency == 180:
            # For 180 MHz
            N1_HS = 7 - 4
            NC1_LS = 4 - 1
            N2_HS = 10 - 4
            N2_LS = 100800 - 1
            N32 = 22857 - 1
            BWSEL = 2

        elif frequency == 170:
            # For 170 MHz
            N1_HS = 5 - 4
            NC1_LS = 6 - 1
            N2_HS = 10 - 4
            N2_LS = 102000 - 1
            N32 = 22857 - 1
            BWSEL = 2

        elif frequency == 160:
            # For 160MHz
            N1_HS = 4  # 8      -4
            NC1_LS = 3  # 4      -1
            N2_HS = 6  # 10     -4
            N2_LS = 102399  # 102400 -1
            N32 = 22856  # 22857  -1
            BWSEL = 2

        elif frequency == 150:
            # For 150 MHz
            N1_HS = 5  # 9     -4
            NC1_LS = 3  # 4     -1
            N2_HS = 6  # 10    -4
            N2_LS = 33249  # 33250 -1
            N32 = 7036  # 7037  -1
            BWSEL = 2

        elif frequency == 140:
            # For 140 MHz
            N1_HS = 9 - 4
            NC1_LS = 4 - 1
            N2_HS = 10 - 4
            N2_LS = 100800 - 1
            N32 = 22857 - 1
            BWSEL = 2

        elif frequency == 120:
            # For 120 MHz
            N1_HS = 11 - 4
            NC1_LS = 4 - 1
            N2_HS = 11 - 4
            N2_LS = 32000 - 1
            N32 = 7619 - 1
            BWSEL = 2

        elif frequency == 100:
            # For 100 MHz
            N1_HS = 9 - 4
            NC1_LS = 6 - 1
            N2_HS = 10 - 4
            N2_LS = 72000 - 1
            N32 = 15238 - 1
            BWSEL = 2

        else:
            logger.error('Si5534: No valid frequency specified: %u' % frequency)

        si5324_write(2, (si5324_read(2) & 0x0f) | (BWSEL << 4))
        si5324_write(25, N1_HS << 5)
        si5324_write(31, NC1_LS >> 16)
        si5324_write(32, NC1_LS >> 8)
        si5324_write(33, NC1_LS)
        si5324_write(40, (N2_HS << 5) | (N2_LS >> 16))
        si5324_write(41, N2_LS >> 8)
        si5324_write(42, N2_LS)
        si5324_write(46, N32 >> 16)
        si5324_write(47, N32 >> 8)
        si5324_write(48, N32)
        si5324_write(137, si5324_read(137) | 0x01)  # FASTLOCK=1
        si5324_write(136, 0x40)  # ICAL=1

        time.sleep(0.1)

        LOS1_INT = (si5324_read(129) & 0x02) == 0
        LOSX_INT = (si5324_read(129) & 0x01) == 0
        LOL_INT = (si5324_read(130) & 0x01) == 0

        logger.debug('Si5324: Has input: %d' % (LOS1_INT))
        logger.debug('Si5324: Has xtal %d:' % (LOSX_INT))
        logger.debug('Si5324: Locked: %d' % (LOL_INT))

        logger.info('Si5324: Clock set to %u MHz.' % frequency)

        if LOL_INT is False:
            logger.warning('Si5324: Not locked.')

    def get_DP_SENSE(self, DP_ID):
        ''' Read back the vddd_sense lines to identify powered chips '''
        if self.board_version == 'BDAQ53':
            sense = self['DP_CONTROL'].get_data()
            logging.debug('Slow_control sense: %s' % bin(sense[0]))
            if 0 <= DP_ID < 4:
                return ((sense[0] & (1 << DP_ID)) is False)
            else:
                logger.error('Invalid DP_ID (0..3)')
        else:
            logger.warning('RD53A slow control is only available for BDAQ53 hardware')
            return False

    def set_DP_RESET(self, DP_ID, value):
        ''' Controls the POR lines. if set to 1, the POR is pulled to local chip ground via opto-coupler '''
        if self.board_version == 'BDAQ53':
            if 0 <= DP_ID < 4:
                self['DP_CONTROL'].set_data([value << (4 + DP_ID)])
            else:
                logger.error('Invalid DP_ID (0..3)')
        else:
            logger.warning('RD53A slow control is only available for BDAQ53 hardware')
            return False

    def print_powered_dp_connectors(self):
        ''' Report the VDDD_SENSE signal status for the given Displayport connectors'''
        if self.board_version == 'BDAQ53':
            for i in range(4):
                if self.get_DP_SENSE(i):
                    logger.info('VDDD_SENSE detected at Displayport ID %i' % i)

    def dp_power_on_reset(self, DP_ID):
        ''' Short reset pulse for given DP_ID '''
        self.set_DP_RESET(DP_ID, True)
        time.sleep(0.1)
        self.set_DP_RESET(DP_ID, False)

    def enable_monitor_filter(self):
        self['rx'].set_USER_K_FILTER_MASK_1(0x01)  # only allow frames containing register data
        self['rx'].set_USER_K_FILTER_MASK_2(0x02)  # only allow frames containing register data
        self['rx'].set_USER_K_FILTER_MASK_3(0x04)  # only allow frames containing register data
        self['rx'].set_USER_K_FILTER_EN(True)  # enable the filter
        logger.debug('USER_K filter enabled')

    def enable_monitor_data(self):
        indata = self.write_register(register='GLOBAL_PULSE_ROUTE', data=0x0100, write=False)
        indata += self.write_global_pulse(width=4, write=False)
        self.write_command(indata)
        logger.debug('Monitor data enabled')

    def write_digilent_dac(self, value):
        ''' Writes integer upto 16 bits to the externally via PMOD connected Digilent DAC '''
        if self.board_version == 'BDAQ53':
            byts = []
            value = int(value)
            for i in range(0, 2):
                byts.append(value >> (i * 8) & 0xff)
            byts.reverse()
            self['spi_dac'].set_data(byts, addr=0)
            self['spi_dac'].start()
        else:
            logger.warning('Digilent DAC is only available for BDAQ53 hardware')
            return False

    def _measure_temperature_ntc_CERNFMC(self, fmc_hpc=False ):
     
        #-----constants-------------
        Vdd=2.5 #Vdd of the ADC
        R1=39000 #Resistance 1 of the voltage divider, in series with NTC in FMC-card
        R25C=10e3 #NTC constant
        T25=298.15
        B=3435 # Beta NTC constant
        LSB=0.001 # LSB value of the ADC for notmal configuration. Can be changed by changing the configuration register.
        #---------------------------
       
        #Code needed to read the values of the 1 bit ADC in CERN-FMC card:
        if (fmc_hpc):
                                #raw_input("press to write HIGH pin count connector FMC")
            self['i2c'].write(0xe8,[0b00000010])#address of switch in the FPGA, it selects several I2C devices, we select output connected to FMC HPC
                                #print "USING HPC FMC"
        else:
                                #raw_input("press to write low pin count connector FMC")
            self['i2c'].write(0xe8,[0b00000110])#address of switch in the FPGA, it selects several I2C devices, we select output connected to FMC HPC
                                #print "USING LPC FMC"
 
        self['i2c'].write(0x90,[0b00000001])#address of ADC and write the addresss pointer register to point the configuration register(default 0x8583)
        self['i2c'].write(0x90,[0b00000001,0x85,0x83])#Here we reset the ADC to start a single conversion. with this config (which is the default) in the conversion register we will read the voltage drop between the terminals of the NTC resistor.
       
        
        self['i2c'].write(0x90,[0b00000000])
        a = self['i2c'].read(0x90,2) #read two bytes of the conversion register of adc
       
        #print a, hex(a[0]), hex(a[1])
        #a[0] is the more significant byte
        #a[1] is the less significant byte, but its 4 LSBs are not part of the conversion, just zeros (it's a 12bit ADC)
        adc=(((a[0]<<8)|a[1])>>4)
        Vadc=LSB*adc
        Rntc = (R1*Vadc)/(Vdd-Vadc)
        Tntc = (1.0/((1.0/T25)+((1.0/B)*(np.log(Rntc/R25C)))))-273.15
        return Tntc

    def az_setup(self, delay, repeat, width=6, synch=1, **kwargs):
        '''
            Auto-zeroing configuration for the synchronous front-end

            Parameters:
            ----------
                dalay: AZ command distance in [us]
                width: global pulse width (default: 6)
                repeat: How many times. 0 = infinite
                synch >= 0  for width<6  (the = value is the minimum amount of sync needed to correctly mask the HitOr. With more it still works but with higher dead-time
                synch >= 1  for width=6
                      >= 5  for width=7
                      >= 13 for width=8
                      >= 29 for width=9
        '''
        self['pulse_gen_az'].reset()
        if self['pulse_gen_az'].is_done():
            logger.info('Setting auto-zeroing period to %s us' % delay)
            self['pulse_gen_az'].set_delay(delay * 160)
            self['pulse_gen_az'].set_width(1)
            self['pulse_gen_az'].set_repeat(repeat)
        else:
            while not self['pulse_gen_az'].is_done():
                logger.warning('Auto-zeroing pluse generator is busy. Waiting...')
                time.sleep(0.1)

        indata = self.write_register(register='Vthreshold_LIN', data=1023, write=False)

        indata += self.write_register(register='GLOBAL_PULSE_ROUTE', data=0x4000, write=False)
        indata += self.write_register(register='EN_CORE_COL_SYNC', data=0, write=False)
        indata += self.write_global_pulse(width=width, write=False)  # pulse width should be ~400 ns (2^n * 6.25 ns)
        indata += self.write_sync_01(write=False) * synch
        indata += self.write_register(register='EN_CORE_COL_SYNC', data=0xffff, write=False)

        indata += self.write_register(register='Vthreshold_LIN', data=kwargs['Vthreshold_LIN'], write=False)
        indata += self.write_ecr(write=False)

        self.write_command(indata)

        self['cmd'].set_az_veto_cycles(len(indata) * 8)

    def az_start(self):
        ''' Software-controlled start '''
        logger.debug('Starting auto-zeroing')
        self['pulse_gen_az'].start()

    def az_stop(self):
        ''' Software-controlled stop '''
        logger.debug('Stoping auto-zeroing')
        self['pulse_gen_az'].reset()


if __name__ == '__main__':
    rd53a_chip = RD53A()
    rd53a_chip.init()
