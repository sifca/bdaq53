#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import time
import logging
import unittest

import tables as tb
import numpy as np
from slackclient import SlackClient

logger = logging.getLogger(__file__)


class TestScanScripts(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        if os.getenv('CI', False):
            job_id = os.environ.get('CI_JOB_ID', 'unknown')
            text = 'Use setup for hardware based test job #%s in 5 minutes! Visit https://gitlab.cern.ch/silab/bdaq53/-/jobs/%s to cancel.' % (job_id, job_id)
            logger.info(text)
            if os.getenv('SLACK_TOKEN', False):
                cls.slack = SlackClient(os.environ.get('SLACK_TOKEN'))
                if not cls.slack.api_call('chat.postMessage', channel='rd53a_module_setup', text=text, username='BDAQ53 Bot', icon_emoji=':robot_face:')['ok']:
                    raise unittest.SkipTest("Cannot use Slack to inform users about hardware usage. Skip hardware based test!")
            else:
                raise unittest.SkipTest("Cannot get Slack credentials to inform users about hardware usage. Skip hardware based test!")
            time.sleep(60 * 5)
            cls.slack.api_call('chat.postMessage', channel='rd53a_module_setup', text='Starting...', username='BDAQ53 Bot', icon_emoji=':robot_face:')

    @classmethod
    def tearDownClass(cls):
        if os.getenv('CI', False):
            cls.slack.api_call('chat.postMessage', channel='rd53a_module_setup', text='Done!', username='BDAQ53 Bot', icon_emoji=':robot_face:')

    def test_digital_scan(self):
        ''' Test digital scan '''
        from bdaq53.scans import scan_digital
        scan = scan_digital.DigitalScan()
        scan.start(**scan_digital.local_configuration)
        scan.close()

        with tb.open_file(scan.output_filename + '_interpreted.h5') as in_file:
            enable_mask = in_file.root.configuration.enable_mask[:]
            logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2)[enable_mask] != 100)))
            logger.info('Integrated occupancy: %s' % np.sum(in_file.root.HistOcc[:].sum(axis=2)[enable_mask]))
            self.assertTrue(np.all(in_file.root.HistOcc[:].sum(axis=2)[enable_mask] == 100))
            # FIXME: Skip these two for now until shift in BCID and ToT is unterstood
            # self.assertTrue(np.all(in_file.root.HistRelBCID[:] == 15))
            # self.assertTrue(np.all(in_file.root.HistTot[:] == 2))
            self.assertTrue(np.all(in_file.root.HistBCIDError[:] == 0))
            self.assertTrue(np.all(in_file.root.HistEventStatus[:] == 0))

    def test_analog_scan(self):
        ''' Test analog scan '''
        from bdaq53.scans import scan_analog
        scan = scan_analog.AnalogScan()
        scan.start(**scan_analog.local_configuration)
        scan.close()

        with tb.open_file(scan.output_filename + '_interpreted.h5') as in_file:
            # 60 % of the pixels see all injections, since FE issues
            logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2) != 100)))
            self.assertTrue(np.count_nonzero(in_file.root.HistOcc[:].sum(axis=2) == 100) > 0.6 * 400 * 192)
            self.assertTrue(np.any(in_file.root.HistRelBCID[:]))
            self.assertTrue(np.any(in_file.root.HistTot[:]))
            # We expect BCID errors from SYNC
            # self.assertTrue(np.all(in_file.root.HistBCIDError[:] == 0))
            # self.assertTrue(np.all(in_file.root.HistEventStatus[:] == 0))


if __name__ == '__main__':
    unittest.main()
