#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import os
import unittest

import tables as tb
import numpy as np
import matplotlib
matplotlib.use('Agg')  # Allow headless plotting

import bdaq53  # noqa: E731
from bdaq53.analysis import analysis  # noqa: E731
from bdaq53.analysis.plotting import Plotting  # noqa: E731
from bdaq53.tests import utils  # noqa: E731

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


def get_mean_from_histogram(counts, bin_positions, axis=0):
    return np.average(counts, axis=axis, weights=bin_positions) * bin_positions.sum() / np.nansum(counts, axis=axis)


class TestAnalysis(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestAnalysis, cls).setUpClass()
        plt_logger = logging.getLogger('Plotting')
        cls._plt_log_handler = utils.MockLoggingHandler(level='DEBUG')
        plt_logger.addHandler(cls._plt_log_handler)
        cls.plt_log_messages = cls._plt_log_handler.messages

        ana_logger = logging.getLogger('Analysis')
        cls._ana_log_handler = utils.MockLoggingHandler(level='DEBUG')
        ana_logger.addHandler(cls._ana_log_handler)
        cls.ana_log_messages = cls._ana_log_handler.messages

    @classmethod
    def tearDownClass(cls):
        test_files = ['digital_scan', 'analog_scan', 'threshold_scan',
                      'dac_linearity_test', 'global_threshold_tuning',
                      'ext_trigger_scan_tb', 'ext_trigger_scan_force_trg', 'tune_tlu', 'tot_calibration', 'injection_delay_scan']
        for test_file in test_files:
            os.remove(os.path.join(data_folder, test_file + '_interpreted.h5'))
        os.remove(os.path.join(data_folder, 'analog_scan_clustered.h5'))
        os.remove(os.path.join(data_folder, 'analog_scan_clustered_prop.h5'))
        os.remove(os.path.join(data_folder, 'threshold_scan_interpreted.pdf'))
        os.remove(os.path.join(data_folder, 'last_scan.pdf'))
        os.remove(os.path.join(data_folder, 'tune_tlu_interpreted.pdf'))
        os.remove(os.path.join(data_folder, 'tot_calibration_interpreted.pdf'))

    def test_dig_scan_ana(self):
        ''' Test analysis of digital scan data '''
        raw_data_file = os.path.join(data_folder, 'digital_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'digital_scan_interpreted.h5'),
            os.path.join(data_folder, 'digital_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_chunked_ana(self):
        ''' Test analysis of digital scan data '''
        raw_data_file = os.path.join(data_folder, 'digital_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, chunk_size=99991) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'digital_scan_interpreted.h5'),
            os.path.join(data_folder, 'digital_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_ana_scan_ana(self):
        ''' Test analysis of analog scan data '''
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'analog_scan_interpreted.h5'),
            os.path.join(data_folder, 'analog_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_thr_scan_inter(self):
        ''' Test interpretation of threshold scan data '''
        raw_data_file = os.path.join(data_folder, 'threshold_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_interpreted.h5'),
            os.path.join(data_folder, 'threshold_scan_interpreted_result.h5'),
            node_names=['Hits', 'HistOcc', 'HistTot', 'HistRelBCID'],
            exact=True)
        self.assertTrue(data_equal, msg=error_msg)

        with Plotting(analyzed_data_file=a.analyzed_data_file, pdf_file=None,
                      level='preliminary', qualitative=False, internal=False,
                      save_single_pdf=False, save_png=False) as p:
            p.create_standard_plots()

        # Check that no errors are logged (= all plots created)
        self.assertFalse(self.plt_log_messages['error'])

    def test_glob_tun_ana(self):
        ''' Test analysis of global threshold tuning '''
        raw_data_file = os.path.join(data_folder, 'global_threshold_tuning.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'global_threshold_tuning_interpreted.h5'),
            os.path.join(data_folder, 'global_threshold_tuning_interpreted_result.h5'),
            node_names=['Hits', 'HistTot', 'HistEventStatus',
                        'HistRelBCID', 'HistBCIDError', 'HistOcc'])
        self.assertTrue(data_equal, msg=error_msg)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'global_threshold_tuning_interpreted.h5'),
            os.path.join(data_folder, 'global_threshold_tuning_interpreted_result.h5'),
            node_names=['NoiseMap', 'ThresholdMap', 'Chi2Map'],
            exact=False,
            # Allow Threshold/Noise to be 1% off due to fit
            rtol=0.01)
        self.assertTrue(data_equal, msg=error_msg)

    def test_clustering_full(self):
        ''' Test cluster results against fixture '''
        with analysis.Analysis(raw_data_file=os.path.join(data_folder, 'analog_scan.h5'),
                               analyzed_data_file=os.path.join(data_folder, 'analog_scan_clustered.h5'),
                               cluster_hits=True, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'analog_scan_clustered.h5'),
            os.path.join(data_folder, 'analog_scan_clustered_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_clustering_properties(self):
        ''' Check validity of cluster results '''
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        analyzed_data_file = os.path.join(data_folder, 'analog_scan_clustered_prop.h5')
        with analysis.Analysis(raw_data_file=raw_data_file,
                               analyzed_data_file=analyzed_data_file,
                               store_hits=True, cluster_hits=True) as a:
            a.analyze_data()

        # Deduce properties from hit table
        with tb.open_file(analyzed_data_file) as in_file:
            hits = in_file.root.Hits[:]
            # Omit late hits do prevent double counting
            # Only ToT < 14 is clustered
            hits = hits[hits['tot'] < 14]
            n_hits = hits.shape[0]
            tot_sum = hits['tot'].sum()

        with tb.open_file(analyzed_data_file) as in_file:
            self.assertEqual(in_file.root.Cluster[:]['size'].sum(), n_hits)
            self.assertEqual(in_file.root.Cluster[:]['tot'].sum(), tot_sum)
            # Check hists
            hist_cluster_tot = in_file.root.HistClusterTot[:]
            tot_cls_tot = np.sum(hist_cluster_tot * np.arange(hist_cluster_tot.shape[0]))
            tot_cls_shape = np.sum(in_file.root.HistClusterShape[:])
            self.assertEqual(tot_cls_tot, tot_sum)
            self.assertEqual(tot_cls_shape, in_file.root.Cluster[:].shape[0])

    def test_dac_linearity_ana(self):
        raw_data_file = os.path.join(data_folder, 'dac_linearity_test.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_adc_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'dac_linearity_test_interpreted.h5'),
            os.path.join(data_folder, 'dac_linearity_test_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_emtpy_data(self):
        ''' Test for useful error message for empty data
        '''
        raw_data_file = os.path.join(data_folder, 'empty_data.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        self.assertTrue(any('Data is empty' in s for s in self.ana_log_messages['warning']))

    def test_ext_trg_ana(self):
        ''' Test analysis of external trigger scan with TLU trigger words

            Test beam data with TLU not waiting for RD53A device
        '''
        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan_tb.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, chunk_size=3989) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_ext_trg_ana_force_trg(self):
        ''' Test analysis of external trigger scan with TLU trigger words
            and forced trigger alignment.

            Test beam data (from SYNC, HW based veto) with TLU. This test data has not the best quality due to issues with
            SYNC flavor operation (ECR,...), thus this test data is very small. Nevertheless it is better than no test.
            Improved test data will be delivered soon.
        '''
        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan_force_trg.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, align_method=2) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_force_trg_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_force_trg_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, align_method=2, chunk_size=3989) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_force_trg_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_force_trg_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_tune_tlu_ana(self):
        ''' Test analysis of tune TLU
        '''
        from bdaq53.scans.tune_tlu import TuneTlu
        tune_tlu = TuneTlu()
        raw_data_file = os.path.join(data_folder, 'tune_tlu.h5')
        tune_tlu.analyze(raw_data_file[:-3])

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'tune_tlu_interpreted.h5'),
            os.path.join(data_folder, 'tune_tlu_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_calibrate_tot_ana(self):
        ''' Test analysis of calibrate tot scan
        '''
        from bdaq53.scans.calibrate_tot import TotCalibration
        cal_tot = TotCalibration()
        cal_tot.output_filename = os.path.join(data_folder, 'tot_calibration')
        cal_tot.analyze()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'tot_calibration_interpreted.h5'),
            os.path.join(data_folder, 'tot_calibration_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_injection_delay_ana(self):
        ''' Test analysis of injection delay scan
        '''
        from bdaq53.scans.scan_injection_delay import InjDelay
        cal_injdel = InjDelay()
        cal_injdel.output_filename = os.path.join(data_folder, 'injection_delay_scan')
        cal_injdel.analyze()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'injection_delay_scan_interpreted.h5'),
            os.path.join(data_folder, 'injection_delay_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)


if __name__ == '__main__':
    unittest.main()
