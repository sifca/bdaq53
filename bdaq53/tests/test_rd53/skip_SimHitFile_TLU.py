#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import os
import numpy as np
import sys
import yaml
import logging
import argparse

from bdaq53.tests import utils
from basil.utils.sim.utils import cocotb_compile_and_run, cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils as au
from bdaq53.scan_base import ScanBase

from bdaq53.scans.scan_analog import AnalogScan
#from pygments.lexer import words

configuration = {
    "n_injections": 2,

    "mask_step": 2,
    "start_column": 160,
    "stop_column": 161,

    'VCAL_HIGH': 4000,
    'VCAL_MED': 1000,

}

activelanes_rx = 1
activelanes_tx = activelanes_rx
EnableMonitorData = False

rd53a_size = True


chip_configuration = '../bdaq53/scans/0x0C38.yaml'
HEADER_ID = 0x00010000
TLU_ID = 0x80000000
USERK_FRAME_ID = 0x01000000


def proc_data(data_words):
    fe_high_word = True
    data_header = False
    tlu_word = False

    data_dtype = {'names': ['bcid', 'col', 'row', 'tot', 'trig'],
                  'formats': ['uint32', 'uint16', 'uint16', 'uint8', 'uint8']}
    hit_data = np.array([], dtype=data_dtype)

    # trigger_bx = data_in['bcid'][data_in['multicol']>50]

    for word in data_words:
        if word & USERK_FRAME_ID:  # skip USER_K frame
            continue
        if word & HEADER_ID:  # data header
            data_header = True
            fe_high_word = True

        if word & TLU_ID:  # tlu word
            continue
            tlu_word = True
            print (data_header, 'DATA HEADER')

        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if fe_high_word:
            data_word = word & 0xffff
            fe_high_word = False  # Next is low word
            continue  # Low word stll missing
        else:
            data_word = data_word << 16 | word & 0xffff
            fe_high_word = True  # Next is high word
        if data_header:
            data_header = False
            bcid = data_word & 0x7fff
        else:
            print word, 'HIT WORD (FIFO)', data_word, 'CPRRESPOMDIONG DATA WORD'
            multicol = (data_word >> 26) & 0x3f
            region = (data_word >> 16) & 0x3ff

#             if multicol < 50:
#                 col_mod = region % (96/ (2 if rd53a_size else 1))
#                 core = col_mod/16
#                 in_col = 5-core
#                 reg = col_mod%16
#                 row = in_col*64+reg*4
#                 bcid = bcid
#                 col = multicol*8 + 7 - region/(96/ (2 if rd53a_size else 1))
#
#                 for pix_inx, pix_reg in enumerate(['tot0','tot1','tot2','tot3']):
#                     #if i[pix_reg] != 15:
#                     #    tot = i[pix_reg]+1
#                     #    row += pix_inx
#                         #print i, 'bcid=' ,bcid,'multicol=', i['multicol'], 'region=',i['region'], 'col=', col, 'row=', row, 'core=', core
#                     hit_data = np.append(hit_data, np.array([(bcid, col, row, 1, False)], dtype = data_dtype))
# [(730, 180, 228, 1, 0) (730, 180, 228, 1, 0) (730, 180, 228, 1, 0)
#  (730, 180, 228, 1, 0) (769, 178, 220, 1, 0) (769, 178, 220, 1, 0)
#  (769, 178, 220, 1, 0) (769, 178, 220, 1, 0) (773, 176, 196, 1, 0)
#  (773, 176, 196, 1, 0) (773, 176, 196, 1, 0) (773, 176, 196, 1, 0)
#  (780, 182, 212, 1, 0) (780, 182, 212, 1, 0) (780, 182, 212, 1, 0)
#  (780, 182, 212, 1, 0) (831, 182, 252, 1, 0) (831, 182, 252, 1, 0)
#  (831, 182, 252, 1, 0) (831, 182, 252, 1, 0) (935, 182, 252, 1, 0)
#  (935, 182, 252, 1, 0) (935, 182, 252, 1, 0) (935, 182, 252, 1, 0)] hit_data_rec 24

# [(730, 182,  92, 0, 0) (769, 183, 139, 0, 0) (773, 180, 184, 0, 0)
#  (780, 183,  42, 0, 0) (831, 180,  47, 0, 0) (935, 180,  47, 0, 0)] hit_data_rec 6


            for i in range(4):
                col, row = au.translate_mapping(multicol, region, i)
            #print col, row
                tot = (data_word >> i * 4) & 0xf
            # print tot

                if col < 400 and row < 192:
                    if tot != 255 and tot != 15:
                        print bcid, col, row, tot, 'bcid, col, row, tot', multicol, region
                        hit_data = np.append(hit_data, np.array([(bcid, col, row, tot, False)], dtype=data_dtype))

    return hit_data


def get_hits_from_file(filename, col=20):
    # define data type
    data_dtype = {'names': ['bcid', 'col', 'row', 'tot', 'trig'],
                  'formats': ['uint32', 'uint16', 'uint16', 'uint8', 'uint8']}
    my_data = np.genfromtxt(filename, delimiter=',', dtype=data_dtype)

    # select only data where a trigger was
    my_data = my_data[my_data['trig'] == 1]
    if col >= 0:
        hit_data = my_data[my_data['col'] == col]
    else:
        hit_data = my_data

    return hit_data


def proc_hit_file(filename, latency=500):
    data_dtype = {'names': ['bcid', 'col', 'row', 'tot', 'trig'],
                  'formats': ['uint32', 'uint16', 'uint16', 'uint8', 'uint8']}
    my_data = np.genfromtxt(filename, delimiter=',', dtype=data_dtype)
    trigger_bx = np.unique(my_data['bcid'][my_data['trig'] == 1])

    trigger_bx += 22

    hit_data = my_data[my_data['col'] < 400]

    # need to add latency to bcid from hit file since recorded bcid (from data header) has latency wrt to trigger!
    hit_data['bcid'] += (latency + 2)
    hit_data = np.sort(hit_data)

    # select only hit data where bcid is equal to trigger_bx
    #hit_data = hit_data[np.in1d(hit_data['bcid'], trigger_bx)]

    # hit_data['trig'] = 0
    # TODO: hit merging?

#     if rd53a_size:
#         hit_data['col'] = hit_data['col'] * 2
#         hit_data['row'] = hit_data['row'] + 192
#         hit_data = hit_data[hit_data['col'] < 50 * 8]

    return hit_data, trigger_bx


# def proc_data(data_in):
#     trigger_bx = data_in['bcid'][data_in['multicol']>50]
#
#     data_dtype = {'names':['bcid','col','row','tot','trig'], 'formats':['uint32','uint16','uint16', 'uint8','uint8']}
#     hit_data = np.array([], dtype = data_dtype)
#
#     for i in data_in:
#         if i['multicol'] < 50:
#             col_mod = i['region'] % (96/ (2 if rd53a_size else 1))
#             core = col_mod/16
#             in_col = 5-core
#             reg = col_mod%16
#             row = in_col*64+reg*4
#             bcid = i['bcid']
#             col = i['multicol']*8 + 7 - i['region']/(96/ (2 if rd53a_size else 1))
#
#             for pix_inx, pix_reg in enumerate(['tot0','tot1','tot2','tot3']):
#                 if i[pix_reg] != 15:
#                     tot = i[pix_reg]+1
#                     row += pix_inx
#                     print i, 'bcid=' ,bcid,'multicol=', i['multicol'], 'region=',i['region'], 'col=', col, 'row=', row, 'core=', core
#                     hit_data = np.append(hit_data, np.array([(bcid, col, row, tot, False)], dtype = data_dtype))
#
#
#     hit_data = np.sort(hit_data)
#
#     return hit_data, trigger_bx

#@unittest.skip('Needs to be rewritten using analog_scan...')
class TestHitFile(unittest.TestCase):
    '''This test checks if the hit data procuded with a file (e.g. test.csv) is the same as the interpreted data...
    the trg row indicates that this data comes from trigger signal (wire TRIGGER in test bench is set HIHG, thus only this data is expected.)
    '''

    def setUp(self):

        proj_dir = os.path.dirname(os.path.dirname(
            os.path.abspath(__file__)))  # ../

        top_dir = os.path.dirname(os.path.dirname(proj_dir))
        xilinx_dir = os.environ.get('XILINX_VIVADO')
        rd53a_src_dir = os.environ.get('RD53A_SRC')

        logging.info('Using csv file: %s  Test Multi Column: %d\n' %
                     (cmd_args_f, cmd_args_c))

        self.hit_file = proj_dir + '/test_rd53/' + cmd_args_f
        self.test_multicol = cmd_args_c

        extra_defines = ['INCLUDE_DUT=1', 'INIT_PIXEL_CONF=1',
                         'RTL_SIM', 'AURORA_' + str(activelanes_rx) + 'LANE']
        # define selected multi column
        if self.test_multicol >= 0:
            extra_defines += ['TEST_DC=' + str(self.test_multicol)]

        cocotb_compile_and_run(

            sim_files=[proj_dir + '/hdl/bdaq53_tb_tlu.v',
                       proj_dir + '/../../firmware/src/bdaq53_core.v'],
            top_level='tb',
            extra_defines=extra_defines,
            include_dirs=(proj_dir, proj_dir + "/tests",
                          proj_dir + "/../../firmware/src",
                          proj_dir + "/../../firmware/src/rx_aurora",
                          rd53a_src_dir, rd53a_src_dir + "/src/verilog/",
                          xilinx_dir + '/data/verilog/src'),
            extra='export SIMULATION_MODULES=' + yaml.dump({'drivers.HitDataFile': {'filename': self.hit_file}})
            + '\nVSIM_ARGS += -L ../secureip -L ../unisims  work.glbl \nVHDL_SOURCES+='
            + rd53a_src_dir + '/src/verilog/array/cba/regionDigitalWriter.vhd \n'
        )

        with open(proj_dir + '/../bdaq53.yaml', 'r') as f:
            cnfg = yaml.load(f)

        cnfg['transfer_layer'][0]['type'] = 'SiSim'

        cnfg['hw_drivers'].append({'name': 'fifo', 'type': 'sram_fifo',
                                   'interface': 'intf', 'base_addr': 0x8000, 'base_data_addr': 0x80000000})
        cnfg['hw_drivers'].append({'name': 'test_pulser', 'type': 'pulse_gen',
                                   'interface': 'intf', 'base_addr': 0x0900})
#        cnfg['hw_drivers'].append({'name': 'test_pulser2', 'type': 'pulse_gen',
#                                   'interface': 'intf', 'base_addr': 0x0500})

        self.chip = RD53A(cnfg)
        self.chip.init()

    def test_file(self):
        logging.info('Starting file input test')

        logging.info('Configuring TLU module')
        # configure tlu module
        self.chip['tlu']['RESET'] = 1
        self.chip['tlu']['TRIGGER_MODE'] = 3
        self.chip['tlu']['TRIGGER_LOW_TIMEOUT'] = 0
        self.chip['tlu']['TRIGGER_SELECT'] = 0  # trigger on HitOr, if use TLU: 0 (disabled)
        self.chip['tlu']['TRIGGER_INVERT'] = 0
        self.chip['tlu']['TRIGGER_VETO_SELECT'] = 0
        self.chip['tlu']['TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES'] = 5
        self.chip['tlu']['HANDSHAKE_BUSY_VETO_WAIT_CYCLES'] = 250
        self.chip['tlu']['DATA_FORMAT'] = 0
        self.chip['tlu']['EN_TLU_VETO'] = 0
        self.chip['tlu']['TRIGGER_DATA_DELAY'] = 7
        self.chip['tlu']['TRIGGER_COUNTER'] = 0
        # enable tlu module
        self.chip['tlu']['TRIGGER_ENABLE'] = True

        # configure trigger command pulse
        self.chip['pulser_trig'].EN = 1
        self.chip['pulser_trig'].DELAY = 1  # trigger delay
        self.chip['pulser_trig'].WIDTH = 32 * 4  # should correspond to amount of received/read out data headers (frames)
        self.chip['pulser_trig'].REPEAT = 1

        # configure veto command pulse
        self.chip['pulser_veto'].EN = 1
        self.chip['pulser_veto'].DELAY = 1  # zero delay not possible?
        self.chip['pulser_veto'].WIDTH = 240 * 4  # time to veto tlu in BCs, 240 for 32 frames, 180 for 16 frames, 160 for 8 frames
        self.chip['pulser_veto'].REPEAT = 1

        # Genenerate random trigger with pulser
        self.chip['test_pulser'].EN = 1
        self.chip['test_pulser'].DELAY = 650  # trigger delay
        self.chip['test_pulser'].WIDTH = 10  # should correspond to amount of received/read out data headers (frames)
        self.chip['test_pulser'].REPEAT = 300
        self.chip['test_pulser'].START

        # Wait for PLL lock
        self.assertTrue(self.chip.wait_for_pll_lock())

        # Setup Aurora
        self.chip.set_aurora(write=True)
        self.chip.write_ecr(write=True)
        self.assertTrue(self.chip.wait_for_aurora_sync())

        # what does this do? Has to be set, otherwise no TLU words...
        self.chip['control']['CLK_BX_GATE'] = 1
        self.chip['control'].write()

        # insert empty triggers
        logging.info("Enable Columns")
        indata = self.chip.write_sync(write=False)
        # //  EnableColumn
        indata += self.chip.write_register(register='EN_CORE_COL_SYNC', data=0xffff, write=False)
        # //  EnableColumn
        indata += self.chip.write_register(register='EN_CORE_COL_LIN_1', data=0xffff, write=False)
        # //  EnableColumn
        indata += self.chip.write_register(register='EN_CORE_COL_LIN_1', data=0xffff, write=False)
        # //  EnableColumn
        indata += self.chip.write_register(register='EN_CORE_COL_DIFF_1', data=0xffff, write=False)
        # //  EnableColumn
        indata += self.chip.write_register(register='EN_CORE_COL_DIFF_2', data=0xffff, write=False)
        indata += self.chip.write_sync(write=False)
        indata += self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0b0100000000000000, write=False)
        indata += self.chip.write_sync(write=False)
        indata += self.chip.write_global_pulse(width=14, write=False)
        indata += self.chip.write_sync(write=False)
        # TODO: make this programmbale from configuration, this has to be adjusted with source
        indata += self.chip.write_register(
            register='LATENCY_CONFIG', data=100, write=False)

        self.chip.write_command(indata)
        self.chip['cmd'].set_ext_trigger(1)
        self.chip.write_bcr(write=True)

        # progress simulation
        for _ in range(1000):
            self.chip['rx'].get_rx_ready()

        # START AZ -------------------------------------------
        kwargs = {'Vthreshold_LIN': 400}
        self.chip.az_setup(delay=80, repeat=0, **kwargs)
        self.chip.az_start()

        # progress simulation
        for _ in range(10000):
            self.chip['rx'].get_rx_ready()

        # STOP AZ -------------------------------------------
        self.chip.az_stop()

        logging.info("Start Hits")

        rawdata = self.chip['fifo'].get_data()
        prev_size = 0
        cnt = 0
        for i in range(100):
            rawdata = np.hstack((rawdata, self.chip['fifo'].get_data()))
            size = len(rawdata)
            if i % 10 == 0:
                logging.info("#%d Fifo size: %d", i, size)
        print rawdata, 'RAW DATA IN FIFO'
        logging.info("Received: %d words\n", len(rawdata))
        np.save('/tmp/hit_file.npy', rawdata)
        userk_data = au.interpret_userk_data(rawdata)


#         tlu_sel = np.logical_and(rawdata, 0x80000000)
# print np.bitwise_and(rawdata[tlu_sel], 0x7FFFFFFF).shape[0], 'TLU WORDS
# IN RAW DATA'

        # data from loaded hit file
        hit_data_file, trigger_bx_file = proc_hit_file(self.hit_file)
        selected_hits = get_hits_from_file(self.hit_file, self.test_multicol)
        print hit_data_file, 'hit_data_file, bcid, col,row, tot, trig, WITH CORRECTED ROW COL NUMBER'
        print selected_hits, 'HITS FROM FILE THAT ARE TRIGGERED AND FULFILL THE COL SELECTION'
        print trigger_bx_file, 'trigger_bx_file', len(trigger_bx_file), 'Length of trigger bx'

        # analyze raw data
#         hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve, hist_event_status = au.init_outs(len(rawdata), 0)
#         au.interpret_data(rawdata, hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve, hist_event_status, scan_param_id=0, align_method=0)
# print hits['bcid'], hits['col'], hits['row'], hits['tot'], 'bcid, col,
# row, tot'

        # analyzed data
        hit_data_rec = proc_data(rawdata)
        print hit_data_rec, 'hit_data_rec', len(hit_data_rec)

        if self.test_multicol >= 0:
            hit_data_file = hit_data_file[hit_data_file['col']
                                          >= self.test_multicol * 8]
            hit_data_file = hit_data_file[hit_data_file['col'] < (
                self.test_multicol + 1) * 8]

        #logging.info('Triggers received: %s' % (str(trigger_bx_rec.size),))
        logging.info('Triggers expected: %s\n' % (str(trigger_bx_file.size),))

        userk_regs = au.process_userk(userk_data)
        logging.info('USER_K frames received: %s\n' % (str(userk_data.size),))
        logging.info('USER_K registers received: %s\n' %
                     (str(userk_regs.size),))

        print "--------------FILE--------------"
        print hit_data_file
        print "-----FILE TRIGGERS-----"
        print trigger_bx_file
        print "--------------REC---------------"
        #print hit_data_rec
        print "-----REC TRIGGERS-----"
        #print trigger_bx_rec
        #print "--------------USER_K------------"
        #print userk_regs
        print "--------------------------------"

        #logging.info('Hit pixel received: %s' % (str(hit_data_rec.size),))
        logging.info('Hit pixel expected: %s' % (str(hit_data_file.size),))
        logging.info('USER K frames received: %s' % (str(userk_data.size),))

        # diff_trigger  = [x for x in trigger_bx_rec if x not in trigger_bx_file]
        #logging.info('Diff Trigger Data: %s' % (str(diff_trigger)))
        #diff_hit_data = [x for x in hit_data_rec if x not in hit_data_file]
        #logging.info('Diff Hit Data: %s' % (str(diff_hit_data)))

        # self.assertEqual(np.array_equal(trigger_bx_file, trigger_bx_rec), True, 'Triggers don\'t match.')
        # self.assertEqual(np.array_equal(hit_data_file, hit_data_rec), True, 'Data doesn\'t match.')

        # progress simulation
        for _ in range(1000):
           self.chip['rx'].get_rx_ready()

    def tearDown(self):
        utils.close_sim(self.chip)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', default='22', type=int, # 31, 30, 29, 28, 27
                        help='Test Multi Column number, -1 = all')
    parser.add_argument('-f', default='test_full.csv', help='csv hit file')
    cmd_args = parser.parse_args()

#     cmd_args_c = -1
#     cmd_args_f = 'test.csv'  # contains: bcid, col????, row????? , tot, trigger sent

    cmd_args_c = cmd_args.c  # thuis refers to MULTICOL, not COL!!!!!
    cmd_args_f = cmd_args.f

    del sys.argv[1:]

    unittest.main()
