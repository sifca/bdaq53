#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import argparse
import os
import logging
import yaml

from importlib import import_module
from inspect import getmembers
import firmware_downloader


logger = logging.getLogger('BDAQ53')
logging.basicConfig()
logger.setLevel(logging.INFO)


def main():
    scans = {'test_registers': 'RegisterTest',
             'test_dac_linearity': 'DACLinearTest',
             'scan_digital': 'DigitalScan',
             'scan_analog': 'AnalogScan',
             'scan_threshold': 'ThresholdScan',
             'scan_noise_occupancy': 'NoiseOccScan',
             'tune_global_threshold': 'ThresholdTuning',
             'tune_local_threshold': 'TDACTuning',
             'meta_mask_noisy_pixels': 'MetaNOccScan',
             'meta_tune_threshold': 'MetaThrTuning',
             'meta_tune_threshold_simple': 'MetaTDACTuning'
             }

    scan_names = [key for key in scans.iterkeys()]

    parser = argparse.ArgumentParser(
        description='Bonn DAQ system for RD53A prototype\nexample: bdaq53 scan_digital -p start_column=10 stop_column=20', formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('scan',
                        type=str,
                        nargs='?',
                        choices=scan_names,
                        help='Scan name. Allowed values are:\n' + ', '.join(scan_names),
                        metavar='scan_name')

    parser.add_argument('-f', '--parameter_file',
                        type=str,
                        nargs='?',
                        help='Path to scan parameter file. If not given the default configuration is used.',
                        metavar='parameter_file')

    parser.add_argument('-p', '--parameters',
                        type=str,
                        nargs='+',
                        help='or list of parameters. E.g. paramA=9 paramB=1',
                        metavar='',
                        default=[])

    parser.add_argument('--firmware',
                        nargs=1,
                        help='Firmware file name (e.g. auto, BDAQ53, KC705, ...)\n See https://gitlab.cern.ch/silab/bdaq53/tags',)

    parser.add_argument('--vivado_path',
                        default=[None],
                        nargs=1,
                        help='Path of vivado installation',)

    args = parser.parse_args()

    if args.firmware is not None:
        firmware_downloader.main(args.firmware[0], path=args.vivado_path[0])

    if args.scan:
        mod = import_module('bdaq53.scans.' + args.scan)

        if args.parameter_file:
            parameter_file = args.parameter_file
        else:
            parameter_file = os.path.dirname(os.path.abspath(mod.__file__)) + '/default_chip.yaml'

        logger.info('Using parameter file: ' + parameter_file + '\n')

        with open(parameter_file, 'r') as f:
            config = yaml.load(f)

        config.update(mod.local_configuration)

        for param in args.parameters:
            key, value = param.split('=')
            config[key] = eval(value)

        for name, cls in getmembers(mod):
            if name == scans[args.scan]:
                break

        scan = cls()
        scan.start(**config)
        scan.analyze()


if __name__ == '__main__':
    main()
