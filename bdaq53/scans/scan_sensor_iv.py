#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan records an IV curve of the sensor of a module
    while making sure the chip is powered and configured.
'''

import time
from tqdm import tqdm

import tables as tb

from bdaq53.periphery import DeviceNotFoundError
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import plotting


local_configuration = {
    # Scan parameters
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': 'auto',

    # Bias voltage parameters
    'VBIAS_start': 0,
    'VBIAS_stop': -5,
    'VBIAS_step': -1

}


class RawDataTable(tb.IsDescription):
    voltage = tb.Int32Col(pos=1)
    current = tb.Float64Col(pos=2)


class SensorIVScan(ScanBase):
    scan_id = "sensor_iv_scan"

    def scan(self, VBIAS_start=0, VBIAS_stop=-5, VBIAS_step=-1, **kwargs):
        '''
        Sensor IV scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to enable
        stop_column : int [0:400]
            Last column to enable. This column is excluded from the scan.
        start_row : int [0:192]
            First row to enable
        stop_row : int [0:192]
            last row to enable. This row is excluded from the scan.

        VBIAS_start : int
            First bias voltage to scan
        VBIAS_stop : int
            Last bias voltage to scan. This value is included in the scan.
        VBIAS_step : int
            Stepsize to increase the bias voltage by in every step.
        '''

        if 'SensorBias' not in [ps.name for ps in self.periphery.devices]:
            self.logger.critical('No sensor bias device defined! Aborting...')
            raise DeviceNotFoundError('No sensor bias device defined!')

        self.h5_file.remove_node(self.h5_file.root, 'raw_data')
        self.raw_data_table = self.h5_file.create_table(self.h5_file.root, name='raw_data', title='Raw data', description=RawDataTable)

        self.periphery.power_off_sensor_bias()
        try:
            self.periphery.power_on_sensor_bias()

            self.logger.info('Starting scan...')
            add = 1 if VBIAS_stop > 0 else -1
            pbar = tqdm(total=len(range(VBIAS_start, VBIAS_stop + add, VBIAS_step)), unit='Voltage step')
            for v_bias in range(VBIAS_start, VBIAS_stop + add, VBIAS_step):
                self.periphery.set_sensor_bias(v_bias, verbose=False)
                time.sleep(5)
                current = self.periphery.get_sensor_bias()[1]
                row = self.raw_data_table.row
                row['voltage'] = v_bias
                row['current'] = current
                row.append()
                self.raw_data_table.flush()
                pbar.update(1)

            pbar.close()
            self.logger.success('Scan finished')

            self.periphery.power_off_sensor_bias()
        except Exception as e:
            self.logger.error('An error occurred: %s' % e)
            self.periphery.power_off_sensor_bias()

    def analyze(self, create_pdf=True):
        if create_pdf:
            with plotting.Plotting(analyzed_data_file=self.output_filename + '.h5') as p:
                p.logger.info('Creating selected plots...')
                p.create_standard_plots()


if __name__ == "__main__":
    scan = SensorIVScan()
    scan.start(**local_configuration)
    scan.close()
