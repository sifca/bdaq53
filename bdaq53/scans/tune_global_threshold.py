#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script scans over the global threshold with centered TDAC
    to find the mean optimal global threshold value.
'''

import numpy as np
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


local_configuration = {
    # Scan parameters
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': 'auto',

    'VTH_name': 'Vthreshold_LIN',
    'VTH_start': 400,
    'VTH_stop': 300,
    'VTH_step': 2,

    # Target threshold
    'VCAL_MED': 500,
    'VCAL_HIGH': 1000,
}


class ThresholdTuning(ScanBase):
    scan_id = 'global_threshold_tuning'

    def configure(self, TDAC=7, VCAL_MED=500, VCAL_HIGH=4000, **kwargs):
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, n_injections=100,
             VTH_name='Vthreshold_LIN', VTH_start=0, VTH_stop=1024, VTH_step=50, **kwargs):
        '''
        Global threshold tuning main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Row-stepsize of the injection mask. Every mask_stepth row is injected at once.
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH : int
            VCAL_HIGH DAC value.

        VTH_name : str
            Name of the global threshold DAC of the selected flavor
        VTH_start : int
            First value of global threshold DAC to scan.
        VTH_stop : int
            Global threschold DAC value to stop the scan. This value is excluded from the scan.
        VTH_step : int
            Global threshold DAC scan interval.
        '''

        vth_range = range(VTH_start, VTH_stop, -1 * VTH_step)
        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)

        self.logger.info('Starting scan...')
        pbar = tqdm(total=len(mask_data) * len(vth_range), unit=' Mask steps')
        for scan_param_id, vth in enumerate(vth_range):
            self.chip.write_register(VTH_name, vth)

            with self.readout(scan_param_id=scan_param_id):
                for mask in mask_data:
                    self.chip.write_command(mask['command'])
                    if mask['flavor'] == 0:
                        self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                    else:
                        self.chip.inject_analog_single(repetitions=n_injections)
                    pbar.update(1)

        pbar.close()
        self.logger.success('Scan finished')

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()
            try:
                vth_opt = int(np.median(a.threshold_map[np.nonzero(a.threshold_map)]))
            except ValueError:
                vth_opt = 0

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

        return vth_opt


if __name__ == "__main__":
    tuning = ThresholdTuning()
    tuning.start(**local_configuration)
    print tuning.analyze()
    tuning.close()
