#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
from time import sleep
import os
import datetime
import yaml

'''
    This meta script performs digital scans at different VDDD voltages and CMD frequencies.
'''

import zlib  # Workaround

import tables as tb

from bdaq53.meta_scan_base import MetaScanBase
from bdaq53.scans.scan_digital import DigitalScan
from bdaq53.scans.test_registers import RegisterTest
from bdaq53.analysis.plotting import Plotting


local_configuration = {
    # Supply voltage paramters
    'VDDD_mV_start': 800,
    'VDDD_mV_stop': 1200,
    'VDDD_mV_step': 100,

    'VDDA': 1.2,
    'VDDD': 1.2,

    # Frequency parameters
    'F_CMD_start': 100,
    'F_CMD_stop': 200,
    'F_CMD_step': 20,

    # Scan parameters
    'start_column': 0,  # start column for mask
    'stop_column': 400,  # stop column for mask
    'start_row': 0,  # start row for mask
    'stop_row': 192,  # stop row for mask
    'maskfile': 'auto'
}


class RawDataTable(tb.IsDescription):
    supply_voltage = tb.Float32Col(pos=1)
    frequency = tb.Int32Col(pos=2)
    discard_count = tb.Int32Col(pos=3)
    soft_error_count = tb.Int32Col(pos=4)
    hard_error_count = tb.Int32Col(pos=5)
    step_failed = tb.Int32Col(pos=6)


class MetaScanShmoo(MetaScanBase):
    scan_id = 'meta_scan_shmoo'

    def start(self, **kwargs):

        filename = self.output_filename + '.h5'
        self.h5_file = tb.open_file(filename, mode='w', title=self.scan_id)
        self.raw_data_table = self.h5_file.create_table(self.h5_file.root, name='aurora_errors', title='Aurora errors(f,v)', description=RawDataTable)
        self.dump_configuration(**kwargs)
        self.scan(**kwargs)
        self.h5_file.close()

    def scan(self, VDDD_mV_start=1100, VDDD_mV_stop=1300, VDDD_mV_step=50, F_CMD_start=140, F_CMD_stop=180, F_CMD_step=200, **kwargs):
        add = 1 if VDDD_mV_stop > 0 else -1

        for supply_voltage_mV in range(VDDD_mV_start, VDDD_mV_stop + add, VDDD_mV_step):
            supply_voltage = supply_voltage_mV / 1000.0

            for frequency in range(F_CMD_start, F_CMD_stop + F_CMD_step, F_CMD_step):

                # run a digital scan
                scn = DigitalScan(record_chip_status=False)

                # set voltages and frequency
                kwargs['aurora_ref'] = frequency
                kwargs['VDDA'] = kwargs['VDDA']
                kwargs['VDDD'] = supply_voltage

                scn.periphery.power_off_SCC()
                sleep(1)
                scn.periphery.power_on_SCC()
                sleep(1)

                self.logger.info('Starting digital scan at VDDA=%1.2fV and f=%uMHz' % (supply_voltage, frequency))

                scn.logger.addHandler(self.fh)

                try:
                    scn.start(**kwargs)
                    scn.analyze(create_pdf=True)
                    step_failed = False
                except Exception:
                    self.logger.info('skipping')
                    step_failed = True

                scn.close()
                self.scans.append(scn)

                try:
                    with tb.open_file(scn.output_filename + '.h5') as in_file:  # get error counters from scan
                        discard_count = in_file.root.configuration.link_status.aurora_link[0][1]
                        soft_error_count = in_file.root.configuration.link_status.aurora_link[1][1]
                        hard_error_count = in_file.root.configuration.link_status.aurora_link[2][1]
                    in_file.close()
                except Exception:
                    discard_count = -1
                    soft_error_count = -1
                    hard_error_count = -1

                row = self.raw_data_table.row
                row['supply_voltage'] = supply_voltage
                row['frequency'] = frequency
                row['discard_count'] = discard_count
                row['soft_error_count'] = soft_error_count
                row['hard_error_count'] = hard_error_count
                row['step_failed'] = step_failed
                row.append()
                self.raw_data_table.flush()
#                    self.raw_data_table.copy_node(in_file.root.configuration.link_status.aurora_link, self.raw_data_table.root, recursive=True)

                # run a register test
                scn = RegisterTest()
                self.logger.info('Starting register test')
                try:
                    scn.start(**kwargs)
                except Exception:
                    self.logger.info('skipping')

                scn.close()
                self.scans.append(scn)

#        scn.periphery.power_off_SCC()

    def analyze(self):
        with tb.open_file(self.output_filename + '.h5') as in_file:
            with tb.open_file(self.output_filename + '_interpreted.h5', 'a') as outfile:
                outfile.copy_node(in_file.root.configuration.link_status.aurora_link, outfile.root, recursive=True)

        with Plotting(analyzed_data_file=self.output_filename + '.h5') as p:
            p.create_standard_plots()

        in_file.close()
        outfile.close()


if __name__ == '__main__':
    shmoo_scan = MetaScanShmoo()
    shmoo_scan.start(**local_configuration)
