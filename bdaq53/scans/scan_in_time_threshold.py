#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script does a threshold scan and only uses hits that occur at
    the same BCID (in-time threshold scan). To optimize the time point of
    charge injection and to compensate for the pixel position dependent
    injection delay a fine_delay map should be provided.
    The fine_delay map can be created with the `scan_fine_delay` scan.

'''

import numba
from tqdm import tqdm
import numpy as np
import tables as tb
from shutil import copyfile
import os

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
import bdaq53.analysis.analysis_utils as au

local_configuration = {
    # Scan parameters
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': 'auto',
    'mask_diff': False,
    'fine_delay': 13,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10
}


class InTimeThresholdScan(ScanBase):
    scan_id = "in_time_threshold_scan"

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, n_injections=100,
             VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, fine_delay=None, **kwargs):
        '''
        In Time Threshold scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Chip size mask of the injection. Distance between pixels that are injected.
        n_injections : int
            Number of injections.
        fine_delay: int or str
            Sets the fine delay for the scan as an int and scans over range of fine delays if an
            injection_delay_scan_interpreted.h5 file is passed as an argument

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''
        self.logger.info('Starting scan...')

        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)
        delay_map = np.zeros((400, 192), dtype=np.uint8)
        if type(fine_delay) is int:
            self.logger.warning(
                'No fine delay mask supplied. Do you know what you are doing? Please read: https://gitlab.cern.ch/silab/bdaq53/wikis/User-guide/Scans#in-time-threshold-scan')
            delay_map[start_column:stop_column, start_row:stop_row] = fine_delay
        else:
            self.logger.info('Loading fine delay mask from file: %s' % (fine_delay))
            with tb.open_file(filename=fine_delay) as finedelay_file:
                delay_map = finedelay_file.root.configuration.delay_mask[:]
        finedelay_range = np.arange(np.min(delay_map[start_column:stop_column]),
                                    np.max(delay_map[start_column:stop_column]) + 1, 1)

        pbar = tqdm(total=len(mask_data) * len(vcal_high_range) * len(finedelay_range))
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            for delay in finedelay_range:
                self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED, fine_delay=delay)
                with self.readout(scan_param_id=scan_param_id * 16 + delay):
                    for mask in mask_data:
                        self.chip.write_command(mask['command'])
                        if mask['flavor'] == 0:
                            self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                        else:
                            self.chip.inject_analog_single(repetitions=n_injections)
                        pbar.update(1)
        pbar.close()
        self.h5_file.create_carray(self.h5_file.root.configuration, name='delay_mask', title='Fine Delay Mask',
                                   obj=delay_map,
                                   filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        self.logger.info('Scan finished')

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=True) as a:
            a.analyze_data()
            chunk_size = a.chunk_size
        with tb.open_file(self.output_filename + '_interpreted.h5', "r+") as in_file:
            in_file.root.HistRelBCID._f_remove()
            in_file.root.HistOcc._f_remove()
            in_file.root.HistTot._f_remove()
            run_config = au.ConfigDict(in_file.root.configuration.run_config[:])
            min_tdac, max_tdac, _ = au.get_tdac_range(run_config['start_column'],
                                                      run_config['stop_column'])

            # Determines which front end is active and sets the correct bcid cutting value
            if min_tdac < 0:
                rel_bcid = 11
            else:
                rel_bcid = 10
            delay_map = in_file.root.configuration.delay_mask[:]
            scan_range = [v - run_config['VCAL_MED'] for v in range(run_config['VCAL_HIGH_start'],
                                                                    run_config['VCAL_HIGH_stop'],
                                                                    run_config['VCAL_HIGH_step'])]
            tot = np.zeros(shape=(400, 192, len(scan_range), 16, 16), dtype=np.uint8)
            bcid = np.zeros(shape=(400, 192, len(scan_range), 32, 16), dtype=np.uint8)
            for i in tqdm(range(0, in_file.root.Hits.shape[0], chunk_size)):
                hits = in_file.root.Hits[i:i + chunk_size]
                bcid, tot = finedelay_select(bcid, tot, hits)
            in_file.create_carray(in_file.root, name='HistTot_all', title='ToT Histogram for all fine delays', obj=tot,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='HistRelBCID_all',
                                  title='Relativ BCID Histogram for all fine delays',
                                  obj=bcid,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.root.Hits._f_remove()
            shrink_bcid = np.zeros(bcid.shape[:-1], dtype=np.uint8)
            fine_bcid = choose_fine_delay_map(delay_map, bcid, shrink_bcid)
            shrink_tot = np.zeros(tot.shape[:-1], dtype=np.uint8)
            fine_tot = choose_fine_delay_map(delay_map, tot, shrink_tot)
            hist_occ = np.array(np.sum(fine_bcid, axis=3), np.uint8)

            hist_occ_in_time = np.array(fine_bcid[:, :, :, rel_bcid], np.uint8)
            fine_bcid_cut = np.zeros(fine_bcid.shape, dtype=np.uint8)
            fine_bcid_cut[:, :, :, rel_bcid] = fine_bcid[:, :, :, rel_bcid]

            in_file.create_carray(in_file.root, name='HistOcc',
                                  title='Occupancy Histogram',
                                  obj=hist_occ,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_file.create_carray(in_file.root, name='InTimeHistOcc',
                                  title='Occupancy Histogram',
                                  obj=hist_occ_in_time,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            threshold_map, noise_map, chi2_map = au.fit_scurves_multithread(hist_occ.reshape((192 * 400, -1)),
                                                                            scan_range,
                                                                            run_config['n_injections'],
                                                                            optimize_fit_range=False)
            in_t_threshold_map, in_t_noise_map, in_t_chi2_map = au.fit_scurves_multithread(
                hist_occ_in_time.reshape((192 * 400, -1)),
                scan_range,
                run_config['n_injections'],
                optimize_fit_range=False)

            in_file.create_carray(in_file.root, name='ThresholdMap', title='Threshold Map',
                                  obj=threshold_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='NoiseMap', title='Noise Map', obj=noise_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='Chi2Map', title='Chi2 / ndf Map',
                                  obj=chi2_map, filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='HistTot', title='ToT Histogram',
                                  obj=fine_tot,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='HistRelBCID',
                                  title='Relativ BCID Histogram',
                                  obj=fine_bcid,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='InTimeHistRelBCID',
                                  title='In Time Relativ BCID Histogram',
                                  obj=fine_bcid_cut,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_file.create_carray(in_file.root, name='InTimeThresholdMap', title='Threshold Map',
                                  obj=in_t_threshold_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='InTimeNoiseMap', title='Noise Map', obj=noise_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='InTimeChi2Map', title='Chi2 / ndf Map',
                                  obj=in_t_chi2_map, filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        os.rename(self.output_filename + '_interpreted.h5', self.output_filename + '_inter.h5')
        copyfile(self.output_filename + '_inter.h5', self.output_filename + '_interpreted.h5')
        os.remove(self.output_filename + '_inter.h5')

        try:
            overdrive = np.mean(in_t_threshold_map[np.nonzero(in_t_threshold_map)]) - np.mean(threshold_map[np.nonzero(threshold_map)])
        except ValueError:
            overdrive = 0
            self.logger.info('Overdrive estimation failed')
        self.logger.info('The estimated overdrive is: %i DVCAL', overdrive)

        self.plot_standard_plots()
        return overdrive

    def plot_standard_plots(self):
        with tb.open_file(self.output_filename + '_interpreted.h5', 'r') as in_file_h5:
            delay_map = in_file_h5.root.configuration.delay_mask[:]
            in_t_hist_occ = in_file_h5.root.InTimeHistOcc[:]
            in_t_threshold_map = in_file_h5.root.InTimeThresholdMap[:]
            in_t_noise_map = in_file_h5.root.InTimeNoiseMap[:]
            fine_bcid = in_file_h5.root.InTimeHistRelBCID[:]

        with plotting.Plotting(analyzed_data_file=self.output_filename + '_interpreted.h5', save_png=False) as p:
            p.create_standard_plots()
            p._plot_occupancy(hist=np.ma.masked_array(delay_map, p.enable_mask).T,
                              electron_axis=False,
                              z_label='Injection Delay [LSB]',
                              title='Injection Delay Map',
                              use_electron_offset=False,
                              show_sum=False,
                              z_min=0,
                              z_max=16,
                              suffix='injection_delay_map')

            p._plot_distribution(delay_map[p.enable_mask == 0 & ~p.enable_mask].T,
                                 plot_range=np.arange(16),
                                 electron_axis=False,
                                 x_axis_title='Finedelay [LSB]',
                                 title='Fine Delay Distribution',
                                 suffix='fine_delay_distribution')
            scan_parameter_name = '$\Delta$ VCAL'
            electron_axis = True
            scan_parameter_range = [v - p.run_config['VCAL_MED'] for v in
                                    range(p.run_config['VCAL_HIGH_start'],
                                          p.run_config['VCAL_HIGH_stop'] + 1,
                                          p.run_config['VCAL_HIGH_step'])]
            min_tdac, max_tdac, range_tdac = au.get_tdac_range(
                p.run_config['start_column'], p.run_config['stop_column'])
            p._plot_scurves(scurves=in_t_hist_occ.reshape((192 * 400, -1)).T,
                            scan_parameters=scan_parameter_range,
                            electron_axis=electron_axis,
                            scan_parameter_name=scan_parameter_name)
            p._plot_distribution(in_t_threshold_map[in_t_threshold_map != 0 & ~p.enable_mask].T,
                                 plot_range=scan_parameter_range,
                                 electron_axis=electron_axis,
                                 x_axis_title=scan_parameter_name,
                                 title='In Time Threshold distribution for enabled pixels',
                                 print_failed_fits=True,
                                 suffix='in_t_threshold_distribution')
            p._plot_relative_bcid(hist=fine_bcid.sum(axis=(0, 1, 2)).T)
            p._plot_stacked_threshold(data=in_t_threshold_map[in_t_threshold_map != 0 & ~p.enable_mask].T,
                                      tdac_mask=p.tdac_mask[in_t_threshold_map != 0 & ~p.enable_mask].T,
                                      plot_range=scan_parameter_range,
                                      electron_axis=electron_axis,
                                      x_axis_title=scan_parameter_name,
                                      title='In Time Threshold distribution for enabled pixels',
                                      suffix='in_t_tdac_threshold_distribution',
                                      min_tdac=min_tdac,
                                      max_tdac=max_tdac,
                                      range_tdac=range_tdac)
            p._plot_occupancy(hist=np.ma.masked_array(in_t_threshold_map, p.enable_mask).T,
                              electron_axis=True,
                              z_label='In Time Threshold',
                              title='In Time Threshold',
                              use_electron_offset=True,
                              show_sum=False,
                              z_min=None,
                              z_max=None,
                              suffix='threshold_map')
            plot_range = None
            if in_t_noise_map[in_t_noise_map != 0 & ~p.enable_mask].T.shape[0] == 0:
                plot_range = [0, 10]
            p._plot_distribution(in_t_noise_map[in_t_noise_map != 0 & ~p.enable_mask].T,
                                 title='Noise distribution for enabled pixels',
                                 plot_range=plot_range,
                                 electron_axis=electron_axis,
                                 use_electron_offset=False,
                                 x_axis_title=scan_parameter_name,
                                 y_axis_title='# of hits',
                                 print_failed_fits=True,
                                 suffix='noise_distribution')

            p._plot_occupancy(hist=np.ma.masked_array(in_t_noise_map, p.enable_mask).T,
                              electron_axis=False,
                              use_electron_offset=False,
                              z_label='In Time Noise',
                              z_max='median',
                              title='In Time Noise',
                              show_sum=False,
                              suffix='noise_map')


@numba.njit
def finedelay_select(bcid, tot, hits_raw):
    for entries in hits_raw:
        bcid[entries['col'], entries['row'], entries['scan_param_id'] / 16, entries['rel_bcid'], entries[
            'scan_param_id'] % 16] += 1
        tot[entries['col'], entries['row'], entries['scan_param_id'] / 16, entries['tot'], entries[
            'scan_param_id'] % 16] += 1
    return bcid, tot


@numba.njit
def choose_fine_delay_map(finedelay_map, shrink_array, shrinked_array):
    for col in range(400):
        for row in range(192):
            shrinked_array[col, row, :, :] = shrink_array[col, row, :, :, finedelay_map[col, row]]
    return shrinked_array


if __name__ == "__main__":
    scan = InTimeThresholdScan()
    scan.start(**local_configuration)
    scan.close()
