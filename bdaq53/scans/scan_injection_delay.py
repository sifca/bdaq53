#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script scans over different fine delays at constant injection charge
    to find the effective injection for a given charge of the enabled pixels.
'''
import numba
import numpy as np
import tables as tb
from tqdm import tqdm
from scipy import stats

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

import bdaq53.analysis.analysis_utils as au

local_configuration = {
    # Scan parameters
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': 'auto',
    'mask_diff': False,

    'VCAL_MED': 500,
    'VCAL_HIGH': 3500,
}


class InjDelay(ScanBase):
    scan_id = "injection_delay_scan"

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, n_injections=100,
             VCAL_MED=500, VCAL_HIGH=1000, **kwargs):
        '''
        injection delay scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Chip size mask of the injection. Distance between pixels that are injected.
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH : int
            VCAL_HIGH DAC value.
        '''

        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)

        self.logger.info('Starting scan...')
        pbar = tqdm(total=len(mask_data) * 16)
        for scan_param_id in range(16):
            self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED, fine_delay=scan_param_id)
            with self.readout(scan_param_id=scan_param_id):
                for mask in mask_data:
                    self.chip.write_command(mask['command'])
                    if mask['flavor'] == 0:
                        self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                    else:
                        self.chip.inject_analog_single(repetitions=n_injections)
                    pbar.update(1)

        pbar.close()
        self.logger.info('Scan finished')

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=False) as a:
            a.analyze_data()

        with tb.open_file(self.output_filename + '_interpreted.h5', "r+") as io_file:
            run_config = au.ConfigDict(io_file.root.configuration.run_config[:])
            enable_mask = io_file.root.configuration.enable_mask[:]
            min_tdac, max_tdac, _ = au.get_tdac_range(run_config['start_column'],
                                                      run_config['stop_column'])
            if min_tdac < 0:
                rel_bcid = 11
            else:
                rel_bcid = 10
            bcid10_hist = io_file.root.HistRelBCID[:, :, :, rel_bcid]
            bcid_scurve = bcid10_hist.reshape((192 * 400, -1))

            scan_param_range = np.arange(16)
            threshold_map, noise_map, chi2_map = au.fit_scurves_multithread(bcid_scurve, scan_param_range,
                                                                            run_config['n_injections'],
                                                                            invert_x=False,
                                                                            optimize_fit_range=False)
            delay_map = np.zeros(shape=(400, 192), dtype=np.uint32)
            delay_mask = make_fine_delay_mask(bcid_scurve, delay_map, max_occ=run_config['n_injections'])
            delay_mask = avg_core_cols(np.ma.masked_array(delay_mask, ~enable_mask), run_config['start_column'],
                                       run_config['stop_column'])
            io_file.create_carray(io_file.root.configuration, name='delay_mask', title='Fine Delay Mask',
                                  obj=delay_mask,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            io_file.create_carray(io_file.root, name='HistSCurve', title='Scurve Data',
                                  obj=bcid_scurve,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            io_file.create_carray(io_file.root, name='ThresholdMap', title='Threshold Map', obj=threshold_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            io_file.create_carray(io_file.root, name='NoiseMap', title='Noise Map', obj=noise_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            io_file.create_carray(io_file.root, name='Chi2Map', title='Chi2 / ndf Map', obj=chi2_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file, save_png=False) as p:
                p.create_standard_plots()
                scan_parameter_name = 'Finedelay [LSB]'
                electron_axis = False
                scan_parameter_range = range(0, 16)
                p._plot_scurves(scurves=bcid10_hist.reshape((192 * 400, -1)).T,
                                scan_parameters=scan_parameter_range,
                                electron_axis=electron_axis,
                                scan_parameter_name=scan_parameter_name)
                p._plot_occupancy(hist=np.ma.masked_array(delay_mask, p.enable_mask).T,
                                  electron_axis=False,
                                  z_label='Injection Delay [LSB]',
                                  title='Injection Delay Map',
                                  use_electron_offset=False,
                                  show_sum=False,
                                  z_min=0,
                                  z_max=16,
                                  suffix='injection_delay_mask')


@numba.njit
def make_fine_delay_mask(scurves, delay_map, max_occ=100):
    '''
            Generates an injection delay map based on the collected by injection_delay_scan

            Parameters
            ----------
                scurves : array [76800,16]
                    Scurves which show the hit occupancy in bcid 10, as a function of the fine delay
                delay_map : array [400,192]
                    empty array, to write the delay map data into
                max_occ : int [0:2**16]
                    the amount of injections into each pixel

            Returns
            -------
                delay_map : array [400,192]
                    array filled with ideal delay values for each pixel
            '''

    for index in range(len(scurves)):
        entries = scurves[index]
        for index2 in range(len(entries)):
            if entries[index2] >= max_occ:
                delay_map[int(index / 192), index % 192] = index2
                break
    return delay_map


def avg_core_cols(delay_map, start, stop):
    delay_map_out = np.clip(delay_map, 0, 15)
    for corecols in range(start, stop, 8):
        if corecols >= 264:
            min_pix, max_pix = stats.norm.interval(0.99, loc=np.mean(delay_map[corecols:corecols + 8, :]))
            delay_map_out[corecols:corecols + 8, :] = int(max_pix) + 1
        else:
            delay_map_out[corecols:corecols + 8, :] = int(np.mean(delay_map[corecols:corecols + 8, :])) + 2
    delay_map_out = np.clip(delay_map_out, 0, 15)
    return delay_map_out


if __name__ == "__main__":
    scan = InjDelay()
    scan.start(**local_configuration)
    scan.close()
