#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Noise Tuning:
    - set the local threshold to maximum
    - lower global threshold and until disable some pixels
    - lower local threshold (slowly) and increase if see noise hits
'''

import numpy as np
from tqdm import tqdm
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils as au

local_configuration = {
    # Scan parameters
    'start_row': 0,
    'stop_row': 192,
    'maskfile': None,

    # Noise occupancy scan parameters
    'n_triggers': 1e6,

    # For LIN
    # 'VTH_name'     : 'Vthreshold_LIN',
    # 'VTH_start'    : 340,
    # 'start_column' : 16 * 8,
    # 'stop_column'  : 33 * 8,

    # For DIFF
    'VTH_name': 'VTH1_DIFF',
    'VTH_start': 200,
    'start_column': 265,
    'stop_column': 400,
    'wait_cycles': 100,
    'VTH2_DIFF': 50,

    'VTH_step': 1,

    'limit': 0.1,

}


class NoiseThresholdTuning(ScanBase):
    scan_id = "noise_threshold_tuning"

    def take_data(self):

        error = False
        with self.readout(scan_param_id=1, fill_buffer=True, clear_buffer=True):
            for _ in self.steps:
                self.chip.write_command(self.trigger_data, repetitions=50000)

        # if self.chip['rx'].LOST_COUNT:
        if any(self.fifo_readout.get_rx_fifo_discard_count()):
            error = True
            self.chip['rx'].RESET_COUNTERS = 1

            self.chip['rx'].RESET = 1  # temporary fix needs firmware update
            while not self.chip['rx'].RX_READY:
                pass

        dqdata = self.fifo_readout.data
        rawdata = np.concatenate([item[0] for item in dqdata])
        hist_occ = au.analyze_chunk(rawdata)['HistOcc']

        return np.count_nonzero(hist_occ), hist_occ, error

    def log_status(self, n_total_hits, hit_occ, iteration=0,):
        this_tdac = self.chip.tdac_mask[self.start_column:self.stop_column, :].flatten()
        mean_tdac = np.mean(this_tdac)
        if self.min_tdac < 0:
            this_tdac = this_tdac + 15
        bc = np.bincount(this_tdac)
        n_disabled_pixels = (~(self.chip.enable_mask[self.start_column:self.stop_column, self.start_row:self.stop_row])).sum()
        self.r_disabled_pixels = float(n_disabled_pixels) / float(self.n_pixels) * 100.
        np.set_printoptions(linewidth=200)
        self.logger.info('Pixel hit: %d | Threshold in iteration %i is %i| Mean TDAC is %1.2f | %i pixels are disabled. This corresponds to %1.2f%% of %i total pixels. | max_occ %d' % (n_total_hits, iteration, self.vth, mean_tdac, n_disabled_pixels, self.r_disabled_pixels, self.n_pixels, np.max(hit_occ)))
        self.logger.info('TDAC distribution: %s' % (str(bc)))

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, n_triggers=1e6, wait_cycles=200, limit=1, **kwargs):

        self.start_column = start_column
        self.stop_column = stop_column
        self.stop_row = stop_row
        self.start_row = start_row

        self.n_pixels = (stop_column - start_column) * (stop_row - start_row)

        self.trigger_data = self.chip.send_trigger(trigger=0b0001, write=False) * 10  # effectively we send x10 triggers
        self.trigger_data += self.chip.write_sync_01(write=False) * wait_cycles
        n_triggers = int(n_triggers / 10)

        if n_triggers > 50000:
            self.steps = range(0, n_triggers, 50000)
        else:
            self.steps = [n_triggers]

        # disable injection
        self.chip.enable_macro_col_cal(macro_cols=None)
        self.chip.injection_mask[:, :] = False

        self.min_tdac, _, scan_len = self.get_tdac_range(start_column, stop_column)
        FE_VER = 'LIN'
        if self.min_tdac < 0:  # DIFF
            FE_VER = 'DIFF'

        for col in range(start_column, stop_column):
            for row in range(start_row, stop_row):
                if col >= 128 and col < 264:
                    self.chip.tdac_mask[col, row] = 0  # max_tdac
                if col >= 264 and col <= 400:
                    self.chip.tdac_mask[col, row] = 15  # min_tdac

        self.chip.write_masks()

        self.logger.info('Starting scan. Finding lowest global threshold...')

        # Find lowest global threshold
        for vth in range(kwargs.get('VTH_start'), 0, -1 * kwargs.get('VTH_step', 1)):
            self.vth = vth
            self.chip.write_register(kwargs.get('VTH_name'), vth)  # write threshold

            iteration = 0
            while True:
                n_total_hits, hit_occ, error = self.take_data()

                something_to_change = False
                for col in range(start_column, stop_column):
                    for row in range(start_row, stop_row):
                        if hit_occ[col, row]:
                            something_to_change = True
                            if col >= 128 and col < 264:
                                self.chip.enable_mask[col, row] = False
                            if col >= 264 and col <= 400:
                                self.chip.enable_mask[col, row] = False

                if something_to_change:
                    self.chip.write_masks(range(start_column, stop_column))  # can only update what has changed to sepeed up

                self.log_status(n_total_hits, hit_occ, iteration)
                iteration += 1

                if n_total_hits < 0.001 * self.n_pixels and np.max(hit_occ) < 5 and not error:  # no errors ? this is bad?
                    break

            if self.r_disabled_pixels >= limit:
                break

        self.vth += 1  # Increase global threshold -> To be adjusted
        self.chip.write_register(kwargs.get('VTH_name'), self.vth)  # write threshold

        self.fix_tdac = np.ones((400, 192), dtype=bool)
        self.fix_tdac[start_column:stop_column, start_row:stop_row] = 0

        lower_tdac_step = 2  # LIN -> lower threshold for half of pixels
        if FE_VER == 'DIFF':
            lower_tdac_step = 1

        self.logger.info('Starting TDAC scan. Finding lowest TDAC threshold...')

        for iteration in range(scan_len):

            # lower TDAC for not fixed in steps
            for step in range(lower_tdac_step):
                lower_tdac = np.zeros((400, 192), dtype=bool)
                lower_tdac[step::lower_tdac_step] = True
                lower_tdac_2d = np.reshape(lower_tdac, (400, 192))

                for col in range(start_column, stop_column):
                    for row in range(start_row, stop_row):
                        if lower_tdac_2d[col, row] and not self.fix_tdac[col, row]:
                            if col >= 128 and col < 264:
                                if self.chip.tdac_mask[col, row] < 15:
                                    self.chip.tdac_mask[col, row] += 1
                            if col >= 264 and col <= 400:
                                if self.chip.tdac_mask[col, row] > -15:
                                    self.chip.tdac_mask[col, row] -= 1

                while True:
                    # update TDAC and take data
                    self.chip.write_masks(range(start_column, stop_column))
                    n_total_hits, hit_occ, error = self.take_data()

                    # Adjust TDAC up if needed
                    for col in range(start_column, stop_column):
                        for row in range(start_row, stop_row):
                            if hit_occ[col, row]:  # and not self.fix_tdac[col, row]:  only once for TDAC do not adjust later the one set?

                                # increase TDAC and fix
                                self.fix_tdac[col, row] = True
                                if col >= 128 and col < 264 and self.chip.tdac_mask[col, row] > (iteration - 1):
                                    if self.chip.tdac_mask[col, row] > 0:
                                        self.chip.tdac_mask[col, row] -= 1
                                    # else:
                                    #   self.chip.enable_mask[col, row] = False

                                if col >= 264 and col <= 400 and self.chip.tdac_mask[col, row] < (16 - iteration):
                                    if self.chip.tdac_mask[col, row] < 15:
                                        self.chip.tdac_mask[col, row] += 1
                                    # else:
                                    #    self.chip.enable_mask[col, row] = False

                    self.log_status(n_total_hits, hit_occ, iteration)
                    # stop if max 1 hits in any pixel -> To be adjusted)
                    if FE_VER == 'DIFF':
                        if np.max(hit_occ[self.chip.tdac_mask < (16 - iteration)]) <= 1:
                            break
                    else:
                        if np.max(hit_occ[self.chip.tdac_mask > (iteration - 1)]) <= 1:
                            break

        self.TDAC_mask = self.chip.tdac_mask
        self.disable_mask = self.chip.enable_mask
        self.save_disable_mask(update=False)
        self.save_tdac_mask()


if __name__ == "__main__":
    scan = NoiseThresholdTuning()
    scan.start(**local_configuration)

