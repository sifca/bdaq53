#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    TLU data delay tuning.

    This script tries to find a delay value for the TLU module that the error rate in the trigger number transfer is 0.
    An error is detected when the trigger number does not increase by one.

    Note:
    The TLU has to be started with internal trigger generation (e.g. pytlu -c 1000000 -t 10000 -oe CH1 --timeout 5)
    and the trigger data format has to be set to 0 (TLU word consists only of trigger number).
'''

import time
import tables as tb
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils as au


local_configuration = {
    # Scan parameters
    'max_triggers': False,  # do not limit number of triggers; set scan timeout with sleep.
    'sleep': 2,  # Time to record the trigger words per delay setting in seconds
    'trigger_data_delay': range(0, 2**8),  # trigger data delays which are scanned

    # Trigger configuration
    'TRIGGER': {
        'TRIGGER_MODE': 3,  # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 0,  # Selecting trigger input: HitOR (1), disabled (0)
        'DATA_FORMAT': 0,  # Select trigger data format: only trigger number (0), only time stamp (1), combined, 15bit time stamp + 16bit trigger number (2)
        'TRIGGER_COUNTER': 0,
    }
}


class TuneTlu(ScanBase):
    scan_id = "tune_tlu"

    def configure(self, **kwargs):
        self.logger.info('Configuring TLU module...')
        self.chip.configure_tlu_module(**kwargs)

    def scan(self, **kwargs):
        for scan_param_id, delay in enumerate(kwargs['trigger_data_delay']):
            self.logger.info('TRIGGER DATA DELAY: %i' % delay)
            self.chip.set_trigger_data_delay(delay)
            time.sleep(0.1)
            self.logger.info('Starting scan...')
            with self.readout(scan_param_id, **kwargs):
                self.chip.set_tlu_module(True)
                time.sleep(kwargs['sleep'])
                self.chip.set_tlu_module(False)
                if self.chip.get_trigger_counter() == 0:
                    raise RuntimeError('No triggers collected. Check if TLU is on and the IO is set correctly.')

            self.logger.success('Scan finished')

    def analyze(self, output_filename=None):
        # make it possible to call it also from outside with other raw data file
        if output_filename is None:
            output_filename = self.output_filename
        else:
            output_filename = output_filename

        with tb.open_file(output_filename + '.h5', 'r') as in_file_h5:
            meta_data = in_file_h5.root.meta_data[:]
            data = in_file_h5.root.raw_data

            if data.shape[0] == 0:
                raise RuntimeError('No trigger words recorded')

            # Get scan parameters
            run_config = au.ConfigDict(in_file_h5.root.configuration.run_config[:])
            scan_parameters = run_config['trigger_data_delay']
            n_scan_pars = len(scan_parameters)

            # Output data
            with tb.open_file(output_filename + '_interpreted.h5', 'w') as out_file_h5:
                with PdfPages(output_filename + '_interpreted.pdf') as output_pdf:
                    description = [('TRIGGER_DATA_DELAY', np.uint8), ('error_rate', np.float)]  # Output data table description
                    data_array = np.zeros((n_scan_pars,), dtype=description)
                    data_table = out_file_h5.create_table(out_file_h5.root, name='error_rate', description=np.zeros((1,), dtype=description).dtype,
                                                          title='Trigger number error rate for different data delay values')

                    for scan_par_id, words in au.words_of_parameter(data, meta_data):
                        data_array['TRIGGER_DATA_DELAY'][scan_par_id] = scan_parameters[scan_par_id]
                        selection = np.bitwise_and(words, 0x80000000) == 0x80000000  # Select the trigger words in the data stream
                        trigger_words = np.bitwise_and(words[selection], 0x7FFFFFFF)  # Get the trigger values
                        if selection.shape[0] != words.shape[0]:
                            self.logger.warning('There are not only trigger words in the data stream')
                        actual_errors = np.count_nonzero(np.diff(trigger_words[trigger_words != 0x7FFFFFFF]) != 1)
                        data_array['error_rate'][scan_par_id] = float(actual_errors) / selection.shape[0]

                        # Plot trigger number
                        plt.clf()
                        plt.plot(range(trigger_words.shape[0]), trigger_words, '-', label='data')
                        plt.title('Trigger words for delay setting index %d' % scan_par_id)
                        plt.xlabel('Trigger word index')
                        plt.ylabel('Trigger word')
                        plt.grid(True)
                        plt.legend(loc=0)
                        output_pdf.savefig()

                    data_table.append(data_array)  # Store valid data
                    if np.all(data_array['error_rate'] != 0):
                        self.logger.warning('There is no delay setting without errors')
                    self.logger.info('ERRORS: %s', str(data_array['error_rate']))

                    # Determine best delay setting (center of working delay settings)
                    good_indices = np.where(np.logical_and(data_array['error_rate'][:-1] == 0, np.diff(data_array['error_rate']) == 0))[0]
                    best_index = good_indices[good_indices.shape[0] / 2]
                    best_delay_setting = data_array['TRIGGER_DATA_DELAY'][best_index]
                    self.logger.info('The best delay setting for this setup is %d', best_delay_setting)

                    # Plot error rate plot
                    plt.clf()
                    plt.plot(data_array['TRIGGER_DATA_DELAY'], data_array['error_rate'], '.-', label='data')
                    plt.plot([best_delay_setting, best_delay_setting], [0, 1], '--', label='best delay setting')
                    plt.title('Trigger word error rate for different data delays')
                    plt.xlabel('TRIGGER_DATA_DELAY')
                    plt.ylabel('Error rate')
                    plt.grid(True)
                    plt.legend(loc=0)
                    output_pdf.savefig()


if __name__ == "__main__":
    tlu = TuneTlu()
    tlu.start(**local_configuration)
    tlu.analyze()
    tlu.close()
