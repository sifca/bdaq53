#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script scans over the local threshold (TDAC) with fixed
    global threshold to find the optimal TDAC value for each pixel.
'''

import os

import numpy as np
from tqdm import tqdm
import tables as tb

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import plotting


local_configuration = {
    # Scan parameters
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': None,

    # Target threshold
    'VCAL_MED': 500,
    'VCAL_HIGH': 693
}


class TDACTuning(ScanBase):
    scan_id = 'local_threshold_tuning'

    def configure(self, VCAL_MED=500, VCAL_HIGH=4000, **kwargs):
        super(TDACTuning, self).configure(**kwargs)
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, n_injections=100, **kwargs):
        '''
        Local threshold tuning main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Row-stepsize of the injection mask. Every mask_stepth row is injected at once.
        n_injections : int
            Number of injections.
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH : int
            VCAL_HIGH DAC value.
        '''

        mask_data = {}
        min_tdac, max_tdac, scan_len = self.get_tdac_range(start_column, stop_column)

        self.logger.info('Preparing injection masks...')
        for tdac_index, tdac in enumerate(range(min_tdac, max_tdac)):
            kwargs['TDAC'] = tdac
            self.chip.set_tdac(**kwargs)
            mask_data[tdac_index] = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)

        self.logger.info('Starting scan...')
        pbar = tqdm(total=len(mask_data[0]) * scan_len)
        for scan_param_id in range(scan_len):
            with self.readout(scan_param_id=scan_param_id):
                for mask in mask_data[scan_param_id]:
                    self.chip.write_command(mask['command'])
                    if mask['flavor'] == 0:
                        self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                    else:
                        self.chip.inject_analog_single(repetitions=n_injections)
                    pbar.update(1)

        pbar.close()
        self.logger.info('Scan finished')

    def analyze(self, create_pdf=True, create_mask_file=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()
            # Get scan settings
            min_tdac, max_tdac, _ = self.get_tdac_range(a.run_config['start_column'], a.run_config['stop_column'])
            scan_param_range = np.arange(min_tdac, max_tdac)
            n_injections = a.run_config['n_injections']

            with tb.open_file(a.analyzed_data_file) as out_file:
                hist_scurve = out_file.root.HistSCurve[:]
                # Deduce TDAC settings from threshold scan result
                self.TDAC_mask = np.rint(a.threshold_map)  # Round to integer
                # Get fit-less approximation for failed fits; a bad guess is better than no guess
                threshold_no_fit = au.get_threshold(scan_param_range, hist_scurve, n_injections)
                threshold_no_fit = np.reshape(threshold_no_fit, (400, 192))
                self.TDAC_mask[a.chi2_map == 0] = threshold_no_fit[a.chi2_map == 0]
                # Restrict TDACs to available range
                self.TDAC_mask = np.clip(self.TDAC_mask, min_tdac, max_tdac).astype(np.int8)

                if create_mask_file:
                    self.save_tdac_mask()

                if create_pdf:
                    with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                        p.create_standard_plots()

                # FIXME:  Create plot here for now
                if create_pdf:
                    from matplotlib import pylab as plt
                    plt.clf()
                    plt.xlabel('TDAC')
                    plt.ylabel('Count')
                    plt.grid()
                    plt.hist(self.TDAC_mask[a.run_config['start_column']:a.run_config['stop_column']].ravel(), range=(min_tdac, max_tdac),
                             bins=max_tdac - min_tdac, width=1)
                    plt.savefig(os.path.join(self.working_dir, self.timestamp + '_mask.pdf'))

                return self.TDAC_mask


if __name__ == "__main__":
    tuning = TDACTuning()
    tuning.start(**local_configuration)
    tuning.analyze()
