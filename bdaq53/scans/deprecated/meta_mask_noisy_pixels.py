#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os

from bdaq53.meta_scan_base import MetaScanBase
from bdaq53.scans.scan_noise_occupancy import NoiseOccScan


local_configuration = {
    # Scan parameters
    'start_column'  : 128,
    'stop_column'   : 264,
    'start_row'     : 0,
    'stop_row'      : 192,
    'maskfile'      : None,
    
    'limit'         : 1,
    'VTH_name'      : 'Vthreshold_LIN',
    'VTH_start'     : 350,
    'VTH_step'      : 1
    }


class MetaNOccScan(MetaScanBase):
    scan_id = 'meta_noisy_pixel_scan'

    def clean_up(self):
        pass


    def scan(self, limit=1, **kwargs):
        vth_name = kwargs.get('VTH_name', 'Vthreshold_LIN')
        
        for vth in range(kwargs.get('VTH_start',350),0,-1*kwargs.get('VTH_step',1)):
            self.logger.info('Performing noise occupancy scan at %s = %i' % (vth_name, vth))
            kwargs[kwargs.get('VTH_name', 'Vthreshold_LIN')] = vth
            self.vth = vth
            
            self.scan = NoiseOccScan(record_chip_status=False)
            self.scan.logger.addHandler(self.fh)
            self.scan.start(**kwargs)
            _, nocc, _, _ = self.scan.analyze(create_pdf=True)
            self.scan.close()
            
            if nocc > limit:
                break
            
            os.remove(self.scan.output_filename + '.log')
            os.remove(self.scan.output_filename + '.h5')
            os.remove(self.scan.output_filename + '_interpreted.h5')
            os.remove(self.scan.output_filename + '_interpreted.pdf')
            os.remove(self.scan.maskfile)
            
        self.logger.info('Tuning finished! Exceeded limit of %s%% noisy pixels at %s = %i with %1.2f%% noisy pixels.' % (limit, vth_name, vth, nocc))


    def analyze(self):
        return self.scan.maskfile
            

        
if __name__ == '__main__':
    nps = MetaNOccScan()
    nps.start(**local_configuration)
    print nps.analyze()
    