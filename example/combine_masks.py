#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Very simple script to combine two mask files (TDAC and/or disable mask).
    Does not yet check for errors or different cases.
    Will not work if both files provide a mask for the same flavor.
'''

import tables as tb
import numpy as np
from tables.exceptions import NoSuchNodeError


first_mask_file = 'first_mask.h5'
second_mask_file = 'second_mask.h5'

output_file = 'combined_mask.h5'

# Main loop

first_has_disable = False
second_has_disable = False

with tb.open_file(first_mask_file) as infile:
    first_tdac_mask = infile.root.TDAC_mask[:]
    try:
        first_disable_mask = infile.root.disable_mask[:]
        first_has_disable = True
    except NoSuchNodeError:
        pass

with tb.open_file(second_mask_file) as infile:
    second_tdac_mask = infile.root.TDAC_mask[:]
    try:
        second_disable_mask = infile.root.disable_mask[:]
        second_has_disable = True
    except NoSuchNodeError:
        pass

combined_tdac_mask = np.zeros((400, 192), dtype=int)
combined_disable_mask = np.ones((400, 192), dtype=bool)

for row in range(192):
    for col in range(128, 400):
        if first_tdac_mask[col, row] == 0:
            combined_tdac_mask[col, row] = second_tdac_mask[col, row]
            if second_has_disable:
                combined_disable_mask[col, row] = second_disable_mask[col, row]
        else:
            combined_tdac_mask[col, row] = first_tdac_mask[col, row]
            if first_has_disable:
                combined_disable_mask[col, row] = first_disable_mask[col, row]

with tb.open_file(output_file, 'w') as outfile:
    outfile.create_carray(outfile.root,
                          name='TDAC_mask',
                          title='TDAC mask',
                          obj=combined_tdac_mask,
                          filters=tb.Filters(complib='blosc',
                                             complevel=5,
                                             fletcher32=False))
    outfile.create_carray(outfile.root,
                          name='disable_mask',
                          title='Disable mask',
                          obj=combined_disable_mask,
                          filters=tb.Filters(complib='blosc',
                                             complevel=5,
                                             fletcher32=False))
