echo '`pragma protect begin' > sim/core.v
cat aurora_64b66b_1lane/*.v aurora_64b66b_1lane/aurora_64b66b_1lane/src/*.v aurora_64b66b_1lane/aurora_64b66b_1lane/example_design/gt/*.v aurora_64b66b_1lane/aurora_64b66b_1lane/example_design/support/*.v >> sim/core.v
echo "" >> sim/core.v
echo '`pragma protect end' >> sim/core.v

vlog +protect sim/core.v

mv ./work/core.vp ./sim/core.vp
#rm ./sim/core.v
rm -r work

#`pragma protect end
#`pragma protect begin
