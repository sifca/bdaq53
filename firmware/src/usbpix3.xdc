
# ------------------------------------------------------------
#  Copyright (c) SILAB , Physics Institute of Bonn University
# ------------------------------------------------------------

#
#   Constraints for the USBPIX3 PCB with the Mercury KX1(160T-2) FPGA board
#

create_clock -period 10.000 -name clkin -add [get_ports clkin]
create_clock -period 8.000 -name rgmii_rxc -add [get_ports rgmii_rxc]
create_clock -period 5.000 -name CLK200_P -add [get_ports CLK200_P]
create_clock -period 6.250 -name MGT_REFCLK0_P -add [get_ports MGT_REFCLK0_P]
create_clock -period 6.250 [get_pins -hier -filter {name=~*aurora_64b66b_1lane_wrapper_i*aurora_64b66b_1lane_multi_gt_i*aurora_64b66b_1lane_gtx_inst/gtxe2_i/TXOUTCLK}]
create_clock -period 6.250 [get_pins -hier -filter {name=~*aurora_64b66b_1lane_wrapper_i*aurora_64b66b_1lane_multi_gt_i*aurora_64b66b_1lane_gtx_inst/gtxe2_i/RXOUTCLK}]

create_generated_clock -name i_bdaq53_core/i_clock_divisor_i2c/I2C_CLK -source [get_pins PLLE2_BASE_inst/CLKOUT0] -divide_by 1500 [get_pins i_bdaq53_core/i_clock_divisor_i2c/CLOCK_reg/Q]
create_generated_clock -name rgmii_txc -source [get_pins rgmii/ODDR_inst/C] -divide_by 1 [get_ports rgmii_txc]

set_false_path -from [get_clocks CLK125PLLTX] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK125PLLTX]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks rgmii_rxc]
set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks CLK200_P] -to [get_clocks MGT_REFCLK0_P]
set_false_path -from [get_clocks MGT_REFCLK0_P] -to [get_clocks CLK200_P]
set_false_path -from [get_clocks i_bdaq53_core/i_clock_divisor_i2c/I2C_CLK] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks i_bdaq53_core/i_clock_divisor_i2c/I2C_CLK]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks CLK200_P]

set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks MGT_REFCLK0_P]
set_false_path -from [get_clocks MGT_REFCLK0_P] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_1lane_block_i/clock_module_i/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_1lane_block_i/clock_module_i/mmcm_adv_inst/CLKOUT0]]

set_false_path -from [get_clocks CLK200_P] -to [get_clocks i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_1lane_block_i/aurora_64b66b_1lane_i/inst/aurora_64b66b_1lane_wrapper_i/aurora_64b66b_1lane_multi_gt_i/aurora_64b66b_1lane_gtx_inst/gtxe2_i/RXOUTCLK]
set_false_path -from [get_clocks i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_1lane_block_i/aurora_64b66b_1lane_i/inst/aurora_64b66b_1lane_wrapper_i/aurora_64b66b_1lane_multi_gt_i/aurora_64b66b_1lane_gtx_inst/gtxe2_i/RXOUTCLK] -to [get_clocks CLK200_P]

set_false_path -from [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_1lane_block_i/aurora_64b66b_1lane_i/inst/aurora_64b66b_1lane_wrapper_i/common_reset_cbcc_i/u_rst_sync_reset_to_fifo_rd_clk/stg31_reg__0/C] -to [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_1lane_block_i/aurora_64b66b_1lane_i/inst/aurora_64b66b_1lane_wrapper_i/cbcc_gtx0_i/data_fifo/RST]

set_clock_groups -asynchronous -group [get_clocks CLK200_P] -group [get_clocks user_clk_i]
set_clock_groups -asynchronous -group [get_clocks rgmii_rxc] -group [get_clocks CLK125PLLTX]
set_clock_groups -asynchronous -group [get_clocks user_clk_i] -group [get_clocks CLK200_P]

set_property ASYNC_REG true [get_cells sitcp/SiTCP/GMII/GMII_TXCNT/irMacPauseExe_0]
set_property ASYNC_REG true [get_cells sitcp/SiTCP/GMII/GMII_TXCNT/irMacPauseExe_1]


#Oscillator 100MHz
set_property PACKAGE_PIN AA3 [get_ports clkin]
set_property IOSTANDARD LVCMOS15 [get_ports clkin]

#Oscillator 200MHz
set_property PACKAGE_PIN AD18 [get_ports CLK200_N]
set_property PACKAGE_PIN AC18 [get_ports CLK200_P]
set_property IOSTANDARD LVDS [get_ports CLK200_*]

#CLK Mux
set_property PACKAGE_PIN K25 [get_ports CLK_SEL]
set_property IOSTANDARD LVCMOS25 [get_ports CLK_SEL]
set_property PULLUP true [get_ports CLK_SEL]


# Reset push button
set_property PACKAGE_PIN C18 [get_ports RESET_BUTTON]
set_property IOSTANDARD LVCMOS25 [get_ports RESET_BUTTON]
set_property PULLUP true [get_ports RESET_BUTTON]

#USER push button
set_property PACKAGE_PIN AF22 [get_ports USER_BUTTON]
set_property IOSTANDARD LVCMOS25 [get_ports USER_BUTTON]
set_property PULLUP true [get_ports USER_BUTTON]


#SITCP
set_property SLEW FAST [get_ports mdio_phy_mdc]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdc]
set_property PACKAGE_PIN N16 [get_ports mdio_phy_mdc]

set_property SLEW FAST [get_ports mdio_phy_mdio]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdio]
set_property PACKAGE_PIN U16 [get_ports mdio_phy_mdio]

set_property SLEW FAST [get_ports phy_rst_n]
set_property IOSTANDARD LVCMOS25 [get_ports phy_rst_n]
set_property PACKAGE_PIN M20 [get_ports phy_rst_n]

set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rxc]
set_property PACKAGE_PIN R21 [get_ports rgmii_rxc]

set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_ctl]
set_property PACKAGE_PIN P21 [get_ports rgmii_rx_ctl]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[0]}]
set_property PACKAGE_PIN P16 [get_ports {rgmii_rxd[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[1]}]
set_property PACKAGE_PIN N17 [get_ports {rgmii_rxd[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[2]}]
set_property PACKAGE_PIN R16 [get_ports {rgmii_rxd[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[3]}]
set_property PACKAGE_PIN R17 [get_ports {rgmii_rxd[3]}]

set_property SLEW FAST [get_ports rgmii_txc]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_txc]
set_property PACKAGE_PIN R18 [get_ports rgmii_txc]

set_property SLEW FAST [get_ports rgmii_tx_ctl]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_ctl]
set_property PACKAGE_PIN P18 [get_ports rgmii_tx_ctl]

set_property SLEW FAST [get_ports {rgmii_txd[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[0]}]
set_property PACKAGE_PIN N18 [get_ports {rgmii_txd[0]}]
set_property SLEW FAST [get_ports {rgmii_txd[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[1]}]
set_property PACKAGE_PIN M19 [get_ports {rgmii_txd[1]}]
set_property SLEW FAST [get_ports {rgmii_txd[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[2]}]
set_property PACKAGE_PIN U17 [get_ports {rgmii_txd[2]}]
set_property SLEW FAST [get_ports {rgmii_txd[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[3]}]
set_property PACKAGE_PIN T17 [get_ports {rgmii_txd[3]}]


# Aurora related signals
set_property PACKAGE_PIN D6 [get_ports MGT_REFCLK0_P]
set_property PACKAGE_PIN D5 [get_ports MGT_REFCLK0_N]
set_property PACKAGE_PIN F6 [get_ports MGT_REFCLK1_P]
set_property PACKAGE_PIN F5 [get_ports MGT_REFCLK1_N]

# Quad 116 RX
set_property PACKAGE_PIN G4 [get_ports {MGT_RX_P[0]}]
set_property PACKAGE_PIN G3 [get_ports {MGT_RX_N[0]}]
set_property PACKAGE_PIN E4 [get_ports {MGT_RX_P[1]}]
set_property PACKAGE_PIN E3 [get_ports {MGT_RX_N[1]}]
set_property PACKAGE_PIN C4 [get_ports {MGT_RX_P[2]}]
set_property PACKAGE_PIN C3 [get_ports {MGT_RX_N[2]}]
set_property PACKAGE_PIN B6 [get_ports {MGT_RX_P[3]}]
set_property PACKAGE_PIN B5 [get_ports {MGT_RX_N[3]}]
# Quad 116 TX
set_property PACKAGE_PIN F2 [get_ports {MGT_TX_P[0]}]
set_property PACKAGE_PIN F1 [get_ports {MGT_TX_N[0]}]


# Debug LEDs
set_property PACKAGE_PIN M17 [get_ports {LED[0]}]
set_property PACKAGE_PIN L18 [get_ports {LED[1]}]
set_property PACKAGE_PIN L17 [get_ports {LED[2]}]
set_property PACKAGE_PIN K18 [get_ports {LED[3]}]
set_property PACKAGE_PIN P26 [get_ports {LED[4]}]
set_property PACKAGE_PIN M25 [get_ports {LED[5]}]
set_property PACKAGE_PIN L25 [get_ports {LED[6]}]
set_property PACKAGE_PIN P23 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports LED*]
set_property SLEW SLOW [get_ports LED*]


# PMOD
#  ____________
# |1 2 3 4  G +|  First PMOD channel (4 signal lines, ground and vcc)
# |7_8_9_10_G_+|  Second PMOD channel ("")
#
# PMOD connector PMOD10-->PMOD0; PMOD9-->PMOD1; PMOD8-->PMOD2; PMOD7-->PMOD3;
set_property PACKAGE_PIN AC23 [get_ports {PMOD[0]}]
set_property PACKAGE_PIN AC24 [get_ports {PMOD[1]}]
set_property PACKAGE_PIN W25 [get_ports {PMOD[2]}]
set_property PACKAGE_PIN W26 [get_ports {PMOD[3]}]
# PMOD connector PMOD4-->PMOD4; PMOD3-->PMOD5; PMOD2-->PMOD6; PMOD1-->PMOD7;
set_property PACKAGE_PIN AA25 [get_ports {PMOD[4]}]
set_property PACKAGE_PIN AB25 [get_ports {PMOD[5]}]
set_property PACKAGE_PIN V24 [get_ports {PMOD[6]}]
set_property PACKAGE_PIN V26 [get_ports {PMOD[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports PMOD*]
# pull down the PMOD pins which are used as inputs
set_property PULLDOWN true [get_ports {PMOD[0]}]
set_property PULLDOWN true [get_ports {PMOD[1]}]
set_property PULLDOWN true [get_ports {PMOD[2]}]
set_property PULLDOWN true [get_ports {PMOD[3]}]


# Lemo
set_property PACKAGE_PIN AB21 [get_ports LEMO_TX0]
set_property PACKAGE_PIN V23 [get_ports LEMO_TX1]
set_property IOSTANDARD LVCMOS25 [get_ports LEMO_TX*]
set_property SLEW FAST [get_ports LEMO_TX*]


# I2C pins
set_property PACKAGE_PIN N24 [get_ports I2C_SCL]
set_property PACKAGE_PIN P24 [get_ports I2C_SDA]
set_property IOSTANDARD LVCMOS25 [get_ports I2C_*]
set_property SLEW SLOW [get_ports I2C_*]


# EEPROM (SPI for SiTCP)
# IO25=B53, IO23=B56, IO19=B62, IO17=B65
#set_property PACKAGE_PIN G14 [get_ports EEPROM_CS]
#set_property PACKAGE_PIN H11 [get_ports EEPROM_SK]
#set_property PACKAGE_PIN D8 [get_ports EEPROM_DI]
#set_property PACKAGE_PIN A8 [get_ports EEPROM_DO]
#set_property IOSTANDARD LVCMOS25 [get_ports EEPROM_*]


# Aurora IP core
#set_property LOC GTXE2_CHANNEL_X0Y7 [get_cells  aurora_64b66b_1lane_kx1_block_i/aurora_64b66b_1lane_kx1_i/inst/aurora_64b66b_1lane_kx1_wrapper_i/aurora_64b66b_1lane_kx1_multi_gt_i/aurora_64b66b_1lane_kx1_gtx_inst/gtxe2_i]
 set_false_path -to [get_pins -hier *aurora_64b66b_1lane_cdc_to*/D]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *data_sync_reg1}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *ack_sync_reg1}]


# TLU
set_property PACKAGE_PIN V21 [get_ports RJ45_TRIGGER]
set_property PACKAGE_PIN Y25 [get_ports RJ45_RESET]
set_property IOSTANDARD LVCMOS25 [get_ports RJ45_RESET]
set_property IOSTANDARD LVCMOS25 [get_ports RJ45_TRIGGER]


# 8-port RJ45 block (bottom row, port "D", usually used for Mimosa JTAG)
# HITOR3:Pin1-2, HITOR2:Pin3-6, HITOR1:Pin4-5, HITOR0:Pin7-8
set_property PACKAGE_PIN A18 [get_ports {RJ45_HITOR_P[3]}]
set_property PACKAGE_PIN A19 [get_ports {RJ45_HITOR_N[3]}]
set_property PACKAGE_PIN F17 [get_ports {RJ45_HITOR_P[2]}]
set_property PACKAGE_PIN E17 [get_ports {RJ45_HITOR_N[2]}]
set_property PACKAGE_PIN E15 [get_ports {RJ45_HITOR_P[1]}]
set_property PACKAGE_PIN E16 [get_ports {RJ45_HITOR_N[1]}]
set_property PACKAGE_PIN G15 [get_ports {RJ45_HITOR_P[0]}]
set_property PACKAGE_PIN F15 [get_ports {RJ45_HITOR_N[0]}]
set_property IOSTANDARD LVDS_25 [get_ports RJ45_HITOR_*]


# Boot memory
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]