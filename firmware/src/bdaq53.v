/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University
 * ------------------------------------------------------------
 */

`timescale 1ns / 1ps
`default_nettype wire

/**
 *   BDAQ53 (KX2) FPGA:  XC7K160T-2-FFG676-I    (select xc7k160tffg676-2)    Quad-SPI flash: S25FL512S
 *   USBPIX3 (KX1) FPGA: XC7K160T-1-FBG676-C    (select xc7k160tfbg676-1)    Quad-SPI flash: MT25QL256 or S25FL512S depending on the KX1 revision
 *   KC705 FPGA:         XC7K325T-2-FFG900-C    (select xc7k325tffg900-2)    Quad-SPI flash: MT25QL128
**/

/** THE HARDWARE PLATFORM is already set by the run.tcl script and written to the Vivado project options
 *  Options:
 *  1. BDAQ53 base board with Enclustra Mercury+ KX2 FPGA module
 *  2. USBPIX3(MIO3,MMC3) base board with Enclustra Mercury KX1 FPGA module.
 *  3. Xilinx KC705 evaluation board
 **/


/** SELECT THE AURORA LANE CONFIGURATION **/
`define AURORA_1LANE
//`define AURORA_2LANE (NOT IMPLEMENTED)
//`define AURORA_4LANE (NOT IMPLEMENTED)


// Use GMII instead of RGMII for KC705
`ifdef KC705
    `define GMII
`else
    `include "utils/rgmii_io.v"
`endif

`include "utils/fifo_32_to_8.v"
`include "utils/tcp_to_bus.v"
`include "utils/clock_divider.v"

//`ifndef SYNTHESIS	`include "bdaq53_core.v"	`endif

module bdaq53(
    input wire RESET_BUTTON,
    input wire SMA_MGT_REFCLK_N, SMA_MGT_REFCLK_P,
    input wire MGT_REFCLK0_P, MGT_REFCLK0_N,
//        // DP_ML ("DP2") connected to SelectIOs
//    input wire DP_GPIO_LANE0_P, DP_GPIO_LANE0_N,  // HITOR0
//    input wire DP_GPIO_LANE1_P, DP_GPIO_LANE1_N,  // HITOR1
//    input wire DP_GPIO_LANE2_P, DP_GPIO_LANE2_N,  // HITOR2
//    input wire DP_GPIO_LANE3_P, DP_GPIO_LANE3_N,  // HITOR3
    `ifdef KC705
        input wire CLKSi570_P, CLKSi570_N,          // aurora drp clock
        output wire SI5324_RST,
        output wire SI5342_FMC_RST,
        input wire Si5324_N, Si5324_P,              // aurora reference clock
        input wire GPIO_SW_C,                       // resets aurora pll
        input wire [3:0] GPIO_DIP_SW,               // DIP switch
        output wire USER_SMA_P, USER_SMA_N,         // used for CMD
        output wire CMD_FMC_LPC_P, CMD_FMC_LPC_N,              //used for cmd FMC
        output wire XADC_GPIO_23_P, XADC_GPIO_23_N,
        output wire DP2_EN_LPC, DP1_EN_LPC,         // controls the data direction of lane 2 and 3 (0: FPGA --> RD53A, 1: FPGA <-- RD53A)

        input wire DP_GPIO_LANE0_LPC_P, DP_GPIO_LANE0_LPC_N,  // HITOR0
        input wire DP_GPIO_LANE1_LPC_P, DP_GPIO_LANE1_LPC_N,  // HITOR1
        input wire DP_GPIO_LANE2_LPC_P, DP_GPIO_LANE2_LPC_N,  // HITOR2
        input wire DP_GPIO_LANE3_LPC_P, DP_GPIO_LANE3_LPC_N,  // HITOR3

        output wire DP1_EXT_CMD_CLK_P, DP1_EXT_CMD_CLK_N,       // for CDR bypass mode
        output wire DP1_EXT_SER_CLK_P, DP1_EXT_SER_CLK_N,       // for CDR bypass mode
    `else
        input wire USER_BUTTON,                     // resets aurora pll
        input wire MGT_REFCLK1_P, MGT_REFCLK1_N,    // aurora reference clock
        input wire clkin,                           // main fpga clock source
        output wire CLK_SEL,                        // switch between Si570 and external reference clock
    `endif
    `ifdef BDAQ53
        // DP_ML ("DP2") connected to SelectIOs
        input wire DP_GPIO_LANE0_P, DP_GPIO_LANE0_N,  // HITOR0
        input wire DP_GPIO_LANE1_P, DP_GPIO_LANE1_N,  // HITOR1
        input wire DP_GPIO_LANE2_P, DP_GPIO_LANE2_N,  // HITOR2
        input wire DP_GPIO_LANE3_P, DP_GPIO_LANE3_N,  // HITOR3
//        output wire DP_GPIO_AUX_P, DP_GPIO_AUX_N,

        // DP_ML connected to MGTs
        output wire DP_ML_AUX_P, DP_ML_AUX_N,       // Multi-lane DP
        output wire [2:0] DP_SL_AUX_P, DP_SL_AUX_N, // Single-lane DP

        // Displayport "slow control" signals
        output wire [3:0] GPIO_RESET,
        input wire [3:0]  GPIO_SENSE,

        // 2-row PMOD header for general purpose IOs
        inout wire [7:0]  PMOD,
    `elsif USBPIX3
        // RJ45 block. Port "D" on top side, usually used for Mimosa plane 3
        input wire [3:0] RJ45_HITOR_P,
        input wire [3:0] RJ45_HITOR_N,

        // 2-row PMOD header for general purpose IOs
        inout wire [7:0]  PMOD,
    `endif

    input wire CLK200_P, CLK200_N,

    // Ethernet
    `ifdef GMII
        inout  wire       mdio_phy_mdio,
        output wire       mdio_phy_mdc,
        output wire       phy_rst_n,
        input  wire       gmii_crs,
        input  wire       gmii_col,
        output wire [7:0] gmii_txd,
        output wire       gmii_tx_clk,
        output wire       gmii_tx_en,
        output wire       gmii_tx_er,
        input  wire [7:0] gmii_rxd,
        input  wire       gmii_rx_clk,
        input  wire       gmii_rx_er,
        input  wire       gmii_rx_dv,
    `else
        output wire [3:0] rgmii_txd,
        output wire       rgmii_tx_ctl,
        output wire       rgmii_txc,
        input  wire [3:0] rgmii_rxd,
        input  wire       rgmii_rx_ctl,
        input  wire       rgmii_rxc,
        output wire       mdio_phy_mdc,
        inout  wire       mdio_phy_mdio,
        output wire       phy_rst_n,
    `endif
/*
    // DDR3 Memory Interface
    output wire [14:0]ddr3_addr,
    output wire [2:0] ddr3_ba,
    output wire       ddr3_cas_n,
    output wire       ddr3_ras_n,
    output wire       ddr3_ck_n, ddr3_ck_p,
    output wire [0:0] ddr3_cke,
    output wire       ddr3_reset_n,
    inout  wire [7:0] ddr3_dq,
    inout  wire       ddr3_dqs_n, ddr3_dqs_p,
    output wire [0:0] ddr3_dm,
    output wire       ddr3_we_n,
    output wire [0:0] ddr3_cs_n,
    output wire [0:0] ddr3_odt,
*/
    // Aurora lanes
        // Aurora lanes
    `ifdef _SMA
        input wire [3:0]  MGT_RX_P, MGT_RX_N,
    `elsif _FMC_LPC
        input wire [3:0] MGT_RX_FMC_LPC_P, MGT_RX_FMC_LPC_N,
    `else
        input wire [3:0]  MGT_RX_P, MGT_RX_N,
    `endif

    `ifdef USBPIX3
        output wire [0:0] MGT_TX_P, MGT_TX_N,
    `endif
    `ifdef KC705
      `ifdef _SMA
            output wire [0:0]  MGT_TX_P, MGT_TX_N,
        `elsif _FMC_LPC
            output wire [0:0] MGT_TX_FMC_LPC_P, MGT_TX_FMC_LPC_N,
        `endif
    `endif

    // Debug signals
    output wire [7:0] LED,

    `ifdef BDAQ53
        // SiTCP EEPROM
        output wire EEPROM_CS, EEPROM_SK, EEPROM_DI,
        input wire EEPROM_DO,
    `endif

    `ifdef KC705   output wire SM_FAN_PWM,   `endif

    input wire RJ45_TRIGGER,
    output wire LEMO_TX1, //Busy
            // LEMO
    output wire LEMO_TX0,

    `ifndef KC705
        // TLU
        input wire RJ45_RESET,

    `endif
    
    `ifdef KC705
        output wire LEMO_TX0_n, //differential signal for AZ VETO to TLU
    `endif
    

    // I2C bus
    inout wire I2C_SDA,
    output wire I2C_SCL
);

wire RESET_N;
wire RST;
wire AURORA_RESET;
wire BUS_CLK_PLL, CLK200PLL, CLK125PLLTX, CLK125PLLTX90, CLK125PLLRX, CLK_INIT_PLL;
wire PLL_FEEDBACK, LOCKED;
wire clkin1;
wire REFCLK1_OUT;   // Internal copy of the MGT ref clock
wire BYPASS_MODE;

// Clock OUTPUT driver
wire MGT_REF_CLK_P, MGT_REF_CLK_N;
OBUFDS #(
    .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
    .SLEW("FAST")           // Specify the output slew rate
) i_OBUFDS_MGT_REF (
    .O(MGT_REF_CLK_P),      // Diff_p output (connect directly to top-level port)
    .OB(MGT_REF_CLK_N),     // Diff_n output (connect directly to top-level port)
    .I(REFCLK1_OUT)         // Buffer input
);

wire TX_OUT_CLK;     // ------------------------------------------------------------------> DEBUGGING
/*
wire TX_OUT_CLK_P, TX_OUT_CLK_N;
OBUFDS #(
    .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
    .SLEW("FAST")           // Specify the output slew rate
) i_OBUFDS_TX_CLK (
    .O(TX_OUT_CLK_P),       // Diff_p output (connect directly to top-level port)
    .OB(TX_OUT_CLK_N),      // Diff_n output (connect directly to top-level port)
    .I(TX_OUT_CLK)          // Buffer input
);
*/

`ifdef KC705
    assign RESET_N = ~RESET_BUTTON;
    assign AURORA_RESET = GPIO_SW_C;
    wire CLK_SEL = 0;   // dummy signal for the ref clk mux only present on the mmc3 hardware
    localparam CONF_CLKIN1_PERIOD = 5.000;
    localparam CONF_CLKFBOUT_MULT = 5;
    localparam CLKOUT0_DIVIDE_VAL = 6;

    wire clkin_buf;
    IBUFDS #(
      .DIFF_TERM("FALSE"),       // Differential Termination
      .IBUF_LOW_PWR("FALSE"),    // Low power="TRUE", Highest performance="FALSE"
      .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
    ) IBUFDS_inst (
      .O(clkin_buf),  // Buffer output
      .I(CLK200_P),  // Diff_p buffer input (connect directly to top-level port)
      .IB(CLK200_N) // Diff_n buffer input (connect directly to top-level port)
    );

    assign clkin1 = clkin_buf;
    assign SI5324_RST = !RST;
    assign SI5342_FMC_RST = !RST;

`else
    assign RESET_N = RESET_BUTTON;
    assign AURORA_RESET = ~USER_BUTTON;
    localparam CONF_CLKIN1_PERIOD = 10.000;
    localparam CONF_CLKFBOUT_MULT = 10;
    assign clkin1 = clkin;
  `ifdef BDAQ53
        localparam CLKOUT0_DIVIDE_VAL = 6;
  `elsif USBPIX3
      localparam CLKOUT0_DIVIDE_VAL = 7;
  `else
      localparam CLKOUT0_DIVIDE_VAL = 6;
  `endif
`endif

PLLE2_BASE #(
    .BANDWIDTH("OPTIMIZED"),    // OPTIMIZED, HIGH, LOW
    .CLKFBOUT_MULT(CONF_CLKFBOUT_MULT),         // Multiply value for all CLKOUT, (2-64)
    .CLKFBOUT_PHASE(0.0),       // Phase offset in degrees of CLKFB, (-360.000-360.000).
    .CLKIN1_PERIOD(CONF_CLKIN1_PERIOD),     // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).

    .CLKOUT0_DIVIDE(CLKOUT0_DIVIDE_VAL),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT0_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT0_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT1_DIVIDE(5),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT1_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT1_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT2_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT2_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT2_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT3_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT3_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT3_PHASE(90.0),       // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT4_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT4_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT4_PHASE(-5.625),     // Phase offset for CLKOUT0 (-360.000-360.000).     // resolution is 45°/[CLKOUTn_DIVIDE]
    //-65 -> 0?; - 45 -> 39;  -25 -> 100; -5 -> 0;

    .CLKOUT5_DIVIDE(10),        // Divide amount for CLKOUT0 (1-128)
    .CLKOUT5_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT5_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

    .DIVCLK_DIVIDE(1),          // Master division value, (1-56)
    .REF_JITTER1(0.0),          // Reference input jitter in UI, (0.000-0.999).
    .STARTUP_WAIT("FALSE")      // Delay DONE until PLL Locks, ("TRUE"/"FALSE")
 )
 PLLE2_BASE_inst (              // VCO = 1 GHz
     .CLKOUT0(BUS_CLK_PLL),     // 142.857 MHz
     .CLKOUT1(CLK200PLL),       // 200 MHz for MIG
     .CLKOUT2(CLK125PLLTX),     // 125 MHz
     .CLKOUT3(CLK125PLLTX90),
     .CLKOUT4(CLK125PLLRX),
     .CLKOUT5(CLK_INIT_PLL),    // 100 MHz init clock for the Aurora core

     .CLKFBOUT(PLL_FEEDBACK),

     .LOCKED(LOCKED),           // 1-bit output: LOCK

     // Input clock
     .CLKIN1(clkin1),

     // Control Ports
     .PWRDWN(0),
     .RST(!RESET_N),

     // Feedback
     .CLKFBIN(PLL_FEEDBACK)
 );

assign RST = !RESET_N | !LOCKED;
`ifdef KC705
  assign DP1_EN_LPC = ~BYPASS_MODE;  //GPIO_DIP_SW[0];   //"enable": Lanes 0..3 are inputs (L0..3 --> FPGA). "disable": Lanes 0,1 are inputs, Lanes 2,3 are outputs (L0,1 --> FPGA, L2,3 <-- FPGA)
  assign DP2_EN_LPC = 1;   //"enable"=1: Lanes 0..3 are inputs (L0..3 --> FPGA). "disable": Lanes 0,1 are inputs, Lanes 2,3 are outputs (L0,1 --> FPGA, L2,3 <-- FPGA)
`endif
wire BUS_CLK, CLK125TX, CLK125TX90, CLK125RX, CLKCMD;
BUFG BUFG_inst_BUS_CKL (.O(BUS_CLK), .I(BUS_CLK_PLL) );

`ifdef GMII
    BUFG BUFG_inst_CLK125RX   ( .O(CLK125RX),   .I(gmii_rx_clk)   );
    assign CLK125TX = CLK125RX;
    assign gmii_tx_clk = CLK125TX;
`else
    BUFG BUFG_inst_CLK125RX   ( .O(CLK125RX),   .I(rgmii_rxc)     );
    BUFG BUFG_inst_CLK125TX   ( .O(CLK125TX),   .I(CLK125PLLTX)   );
    BUFG BUFG_inst_CLK125TX90 ( .O(CLK125TX90), .I(CLK125PLLTX90) );
`endif

BUFG BUFG_inst_CLKCMDENC  ( .O(CLKCMD), .I(REFCLK1_OUT) );

wire RX_CLK_IN_P, RX_CLK_IN_N;
wire INIT_CLK_IN_P, INIT_CLK_IN_N;  // 50...200 MHz


`ifdef KC705
    assign INIT_CLK_IN_P = CLKSi570_P;
    assign INIT_CLK_IN_N = CLKSi570_N;
    `ifdef EXT_AURORA_REF_CLK	// external LVDS reference clock input, connected to the "MGT_CLK" SMA inputs
        assign RX_CLK_IN_P = SMA_MGT_REFCLK_P;
        assign RX_CLK_IN_N = SMA_MGT_REFCLK_N;
    `else						// programmable on-board oscillator
        assign RX_CLK_IN_P = Si5324_P;
        assign RX_CLK_IN_N = Si5324_N;
    `endif
`else
    assign INIT_CLK_IN_P = CLK200_P;        // from 200 MHz oscillator
    assign INIT_CLK_IN_N = CLK200_N;
    assign RX_CLK_IN_P   = MGT_REFCLK0_P;   // from Si570
    assign RX_CLK_IN_N   = MGT_REFCLK0_N;
`endif


// -------  MDIO interface  ------- //
wire   mdio_gem_mdc;
wire   mdio_gem_i;
wire   mdio_gem_o;
wire   mdio_gem_t;
wire   link_status;
wire  [1:0] clock_speed;
wire   duplex_status;

`ifndef GMII
    // -------  RGMII interface  ------- //
    wire   gmii_tx_clk;
    wire   gmii_tx_en;
    wire  [7:0] gmii_txd;
    wire   gmii_tx_er;
    wire   gmii_crs;
    wire   gmii_col;
    wire   gmii_rx_clk;
    wire   gmii_rx_dv;
    wire  [7:0] gmii_rxd;
    wire   gmii_rx_er;

    rgmii_io rgmii
    (
        .rgmii_txd(rgmii_txd),
        .rgmii_tx_ctl(rgmii_tx_ctl),
        .rgmii_txc(rgmii_txc),

        .rgmii_rxd(rgmii_rxd),
        .rgmii_rx_ctl(rgmii_rx_ctl),

        .gmii_txd_int(gmii_txd),        // Internal gmii_txd signal.
        .gmii_tx_en_int(gmii_tx_en),
        .gmii_tx_er_int(gmii_tx_er),
        .gmii_col_int(gmii_col),
        .gmii_crs_int(gmii_crs),
        .gmii_rxd_reg(gmii_rxd),        // RGMII double data rate data valid.
        .gmii_rx_dv_reg(gmii_rx_dv),    // gmii_rx_dv_ibuf registered in IOBs.
        .gmii_rx_er_reg(gmii_rx_er),    // gmii_rx_er_ibuf registered in IOBs.

        .eth_link_status(link_status),
        .eth_clock_speed(clock_speed),
        .eth_duplex_status(duplex_status),

                                        // FOllowing are generated by DCMs
        .tx_rgmii_clk_int(CLK125TX),    // Internal RGMII transmitter clock.
        .tx_rgmii_clk90_int(CLK125TX90),// Internal RGMII transmitter clock w/ 90 deg phase
        .rx_rgmii_clk_int(CLK125RX),    // Internal RGMII receiver clock

        .reset(!phy_rst_n)
    );
`endif

// Instantiate tri-state buffer for MDIO
IOBUF i_iobuf_mdio(
    .O(mdio_gem_i),
    .IO(mdio_phy_mdio),
    .I(mdio_gem_o),
    .T(mdio_gem_t));


// -------  SiTCP module  ------- //
wire TCP_OPEN_ACK, TCP_CLOSE_REQ;
wire TCP_RX_WR, TCP_TX_WR;
wire TCP_TX_FULL, TCP_ERROR;
wire [7:0] TCP_RX_DATA, TCP_TX_DATA;
wire [15:0] TCP_RX_WC;
wire RBCP_ACK, RBCP_ACT, RBCP_WE, RBCP_RE;
wire [7:0] RBCP_WD, RBCP_RD;
wire [31:0] RBCP_ADDR;
wire SiTCP_RST;
wire EEPROM_CS_int, EEPROM_SK_int, EEPROM_DI_int, EEPROM_DO_int;

`ifdef KC705
    localparam CONST_phy_addr = 5'd7;
`else
    localparam CONST_phy_addr = 5'd3;
`endif

// connect the physical EEPROM pins only for the BDAQ53
`ifdef BDAQ53
    assign EEPROM_CS = EEPROM_CS_int;
    assign EEPROM_SK = EEPROM_SK_int;
    assign EEPROM_DI = EEPROM_DI_int;
    assign EEPROM_DO_int = EEPROM_DO;
`endif

// use PMOD[3:0] to set the IP address
wire [3:0] IP_ADDR_SEL;
wire FORCE_DEFAULTn;
`ifdef BDAQ53
    assign PMOD[7:4] = 4'hf;
    assign IP_ADDR_SEL = {PMOD[0], PMOD[1], PMOD[2], PMOD[3]};
    assign FORCE_DEFAULTn = 1'b0;
`elsif USBPIX3
    assign PMOD[7:4] = 4'hf;
    assign IP_ADDR_SEL = {PMOD[0], PMOD[1], PMOD[2], PMOD[3]};
    assign FORCE_DEFAULTn = 1'b0;
`elsif KC705
    assign IP_ADDR_SEL = {GPIO_DIP_SW[0], GPIO_DIP_SW[1], GPIO_DIP_SW[2], GPIO_DIP_SW[3]};
`else
    assign IP_ADDR_SEL = 4'h0;
    assign FORCE_DEFAULTn = 1'b0;
`endif

WRAP_SiTCP_GMII_XC7K_32K sitcp(
    .CLK(BUS_CLK)               ,    // in    : System Clock >129MHz
    .RST(RST)                   ,    // in    : System reset
    // Configuration parameters
    .FORCE_DEFAULTn(FORCE_DEFAULTn), // in    : Load default parameters
    .EXT_IP_ADDR({8'd192, 8'd168, |{IP_ADDR_SEL} ? 8'd10 + IP_ADDR_SEL : 8'd10, 8'd12})  ,    //IP address[31:0] default: 192.168.10.12. If jumpers are set: 192.168.[11..25].12
    .EXT_TCP_PORT(16'd24)       ,    // in    : TCP port #[15:0]
    .EXT_RBCP_PORT(16'd4660)    ,    // in    : RBCP port #[15:0]
    .PHY_ADDR(CONST_phy_addr)   ,    // in    : PHY-device MIF address[4:0]
    // EEPROM
    .EEPROM_CS(EEPROM_CS_int)   ,    // out    : Chip select
    .EEPROM_SK(EEPROM_SK_int)   ,    // out    : Serial data clock
    .EEPROM_DI(EEPROM_DI_int)   ,    // out    : Serial write data
    .EEPROM_DO(EEPROM_DO_int)   ,    // in     : Serial read data
    // user data, intialial values are stored in the EEPROM, 0xFFFF_FC3C-3F
    .USR_REG_X3C()              ,    // out    : Stored at 0xFFFF_FF3C
    .USR_REG_X3D()              ,    // out    : Stored at 0xFFFF_FF3D
    .USR_REG_X3E()              ,    // out    : Stored at 0xFFFF_FF3E
    .USR_REG_X3F()              ,    // out    : Stored at 0xFFFF_FF3F
    // MII interface
    .GMII_RSTn(phy_rst_n)       ,    // out    : PHY reset
    .GMII_1000M(1'b1)           ,    // in    : GMII mode (0:MII, 1:GMII)
    // TX
    .GMII_TX_CLK(CLK125TX)      ,    // in    : Tx clock
    .GMII_TX_EN(gmii_tx_en)     ,    // out    : Tx enable
    .GMII_TXD(gmii_txd)         ,    // out    : Tx data[7:0]
    .GMII_TX_ER(gmii_tx_er)     ,    // out    : TX error
    // RX
    .GMII_RX_CLK(CLK125RX)      ,    // in    : Rx clock
    .GMII_RX_DV(gmii_rx_dv)     ,    // in    : Rx data valid
    .GMII_RXD(gmii_rxd)         ,    // in    : Rx data[7:0]
    .GMII_RX_ER(gmii_rx_er)     ,    // in    : Rx error
    .GMII_CRS(gmii_crs)         ,    // in    : Carrier sense
    .GMII_COL(gmii_col)         ,    // in    : Collision detected
    // Management IF
    .GMII_MDC(mdio_phy_mdc)     ,    // out    : Clock for MDIO
    .GMII_MDIO_IN(mdio_gem_i)   ,    // in    : Data
    .GMII_MDIO_OUT(mdio_gem_o)  ,    // out    : Data
    .GMII_MDIO_OE(mdio_gem_t)   ,    // out    : MDIO output enable
    // User I/F
    .SiTCP_RST(SiTCP_RST)       ,    // out    : Reset for SiTCP and related circuits
    // TCP connection control
    .TCP_OPEN_REQ(1'b0)         ,    // in    : Reserved input, shoud be 0
    .TCP_OPEN_ACK(TCP_OPEN_ACK) ,    // out    : Acknowledge for open (=Socket busy)
    .TCP_ERROR(TCP_ERROR)       ,    // out    : TCP error, its active period is equal to MSL
    .TCP_CLOSE_REQ(TCP_CLOSE_REQ)   ,    // out    : Connection close request
    .TCP_CLOSE_ACK(TCP_CLOSE_REQ)   ,    // in    : Acknowledge for closing
    // FIFO I/F
    .TCP_RX_WC(TCP_RX_WC)       ,    // in    : Rx FIFO write count[15:0] (Unused bits should be set 1)
    .TCP_RX_WR(TCP_RX_WR)       ,    // out   : Write enable
    .TCP_RX_DATA(TCP_RX_DATA)   ,    // out   : Write data[7:0]
    .TCP_TX_FULL(TCP_TX_FULL)   ,    // out   : Almost full flag
    .TCP_TX_WR(TCP_TX_WR)       ,    // in    : Write enable
    .TCP_TX_DATA(TCP_TX_DATA)   ,    // in    : Write data[7:0]
    // RBCP
    .RBCP_ACT(RBCP_ACT)         ,    // out   : RBCP active
    .RBCP_ADDR(RBCP_ADDR)       ,    // out   : Address[31:0]
    .RBCP_WD(RBCP_WD)           ,    // out   : Data[7:0]
    .RBCP_WE(RBCP_WE)           ,    // out   : Write enable
    .RBCP_RE(RBCP_RE)           ,    // out   : Read enable
    .RBCP_ACK(RBCP_ACK)         ,    // in    : Access acknowledge
    .RBCP_RD(RBCP_RD)                // in    : Read data[7:0]
);


// -------  BUS SIGNALING  ------- //
wire BUS_WR, BUS_RD, BUS_RST;
wire [31:0] BUS_ADD;
wire [31:0] BUS_DATA;
assign BUS_RST = SiTCP_RST;
wire INVALID;

tcp_to_bus i_tcp_to_bus(
    .BUS_RST(BUS_RST),
    .BUS_CLK(BUS_CLK),

    // SiTCP TCP RX
    .TCP_RX_WC(TCP_RX_WC),
    .TCP_RX_WR(TCP_RX_WR),
    .TCP_RX_DATA(TCP_RX_DATA),

    // SiTCP RBCP (UDP)
    .RBCP_ACT(RBCP_ACT),
    .RBCP_ADDR(RBCP_ADDR),
    .RBCP_WD(RBCP_WD),
    .RBCP_WE(RBCP_WE),
    .RBCP_RE(RBCP_RE),
    .RBCP_ACK(RBCP_ACK),
    .RBCP_RD(RBCP_RD),

    // Basil bus
    .BUS_WR(BUS_WR),
    .BUS_RD(BUS_RD),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),

    .INVALID(INVALID)
);

// HitOr
wire [3:0] HITOR_P;
wire [3:0] HITOR_N;
wire [3:0] HITOR_BUF;
wire HITOR;

// Assign HITOR input signals to the differential buffers
// OR the outputs
`ifdef BDAQ53
    localparam n_hitors = 4;
    assign HITOR_P = {DP_GPIO_LANE3_P, DP_GPIO_LANE2_P, DP_GPIO_LANE1_P, DP_GPIO_LANE0_P};
    assign HITOR_N = {DP_GPIO_LANE3_N, DP_GPIO_LANE2_N, DP_GPIO_LANE1_N, DP_GPIO_LANE0_N};
    assign HITOR = |{HITOR_BUF[3], HITOR_BUF[2], HITOR_BUF[1], HITOR_BUF[0]};
`elsif USBPIX3
    localparam n_hitors = 4;
    assign HITOR_P = RJ45_HITOR_P;
    assign HITOR_N = RJ45_HITOR_N;
    assign HITOR = |{HITOR_BUF[3], HITOR_BUF[2], HITOR_BUF[1], HITOR_BUF[0]};
`elsif KC705
    localparam n_hitors = 4;
    assign HITOR_P = {DP_GPIO_LANE3_LPC_P, DP_GPIO_LANE2_LPC_P, DP_GPIO_LANE1_LPC_P, DP_GPIO_LANE0_LPC_P};
    assign HITOR_N = {DP_GPIO_LANE3_LPC_N, DP_GPIO_LANE2_LPC_N, DP_GPIO_LANE1_LPC_N, DP_GPIO_LANE0_LPC_N};
    assign HITOR = |{HITOR_BUF[3], HITOR_BUF[2], HITOR_BUF[1], HITOR_BUF[0]};
`else
    localparam n_hitors = 0;
    assign HITOR = 0;
`endif


genvar gen_hitor;
generate
    if (n_hitors>0) begin
        for (gen_hitor=0; gen_hitor<n_hitors; gen_hitor=gen_hitor+1) begin : generate_hitor_ibufds
            IBUFDS #(
                .DIFF_TERM("TRUE"),
                .IBUF_LOW_PWR("FALSE"),
                .IOSTANDARD("LVDS_25")
            ) IBUFDS_inst_HITOR0 (
                .O(HITOR_BUF[gen_hitor]),
                .I(HITOR_P[gen_hitor]),
                .IB(HITOR_N[gen_hitor])
            );
        end
    end
endgenerate


// ----- bdaq53 CORE ----- //
wire CMD_OUT_CH0, CLKCMD_REG;
wire CMD_OUTPUT_EN;
wire AUR_RX_LANE_UP;
wire AUR_RX_CHANNEL_UP;
wire AUR_PLL_LOCKED;
wire AURORA_CLK_OUT, AURORA_CLK_OUT_REG;
wire [31:0] FIFO_DATA;
wire FIFO_WRITE, FIFO_EMPTY, FIFO_FULL;

// signals for siumulation testbench
wire BUS_BYTE_ACCESS;
wire DUT_RESET, RESET_TB;
wire CMD_DATA, CMD_WRITING;

`ifdef BDAQ53
    wire [3:0] GPIO_RESET_inverted;
    assign GPIO_RESET = ~GPIO_RESET_inverted;   // invert for the low-active opto-coupler
`endif
//TLU KC705 LEMO CONNECTORS
`ifdef KC705
    wire  RJ45_RESET; //LEMO_TX0;
    assign RJ45_RESET = 0;
    assign LEMO_TX0_n = !LEMO_TX0;
    //assign LEMO_TX0 = 0;
    //wire AURORA_CLK_OUT, AURORA_CLK_OUT_REG;

`endif
bdaq53_core i_bdaq53_core(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),    // full 32 bit bus
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_BYTE_ACCESS(BUS_BYTE_ACCESS),

    // Clocks from oscillators and mux select
    .CLK200_P(INIT_CLK_IN_P), .CLK200_N(INIT_CLK_IN_N),
    .RX_CLK_IN_P(RX_CLK_IN_P), .RX_CLK_IN_N(RX_CLK_IN_N),
    `ifdef KC705    .CLK_SEL(),
    `else           .CLK_SEL(CLK_SEL),
    `endif

    // PLL
    .CLK_CMD(CLKCMD),
    .REFCLK1_OUT(REFCLK1_OUT),
    .TX_OUT_CLK(TX_OUT_CLK),
    .AURORA_CLK_OUT(AURORA_CLK_OUT),

    // Aurora lanes
    `ifdef _FMC_LPC .MGT_RX_P(MGT_RX_FMC_LPC_P), .MGT_RX_N(MGT_RX_FMC_LPC_N),
    `else           .MGT_RX_P(MGT_RX_P), .MGT_RX_N(MGT_RX_N),
    `endif

    `ifdef USBPIX3  .TX_P(MGT_TX_P), .TX_N(MGT_TX_N), `endif    // USBPIX3 uses MGTs
    `ifdef KC705
        `ifdef _SMA      .TX_P(MGT_TX_P), .TX_N(MGT_TX_N), // KC705 uses MGTs
        `elsif _FMC_LPC  .TX_P(MGT_TX_FMC_LPC_P), .TX_N(MGT_TX_FMC_LPC_N),
        `endif
     `endif
    `ifdef BDAQ53   .TX_P(),         .TX_N(),         `endif    // BDAQ53 uses SelectIOs

    // CMD encoder
    .CMD_WRITING(CMD_WRITING),
    .CMD_DATA(CMD_DATA),
    .CMD_OUT(CMD_OUT_CH0),
    .CMD_OUTPUT_EN(CMD_OUTPUT_EN),
    .BYPASS_MODE(BYPASS_MODE),

    // HitOr
    .HITOR(HITOR),

    `ifdef BDAQ53
        //PMOD SIGNAL
        .PMOD(),
        // Displayport "slow control" signals
        .GPIO_RESET(GPIO_RESET_inverted),
        .GPIO_SENSE(GPIO_SENSE),
    `endif

    // Debug signals
    `ifdef USBPIX3  .DEBUG_TX0(), .DEBUG_TX1(),
    `else           .DEBUG_TX0(), .DEBUG_TX1(),
    `endif

    .RX_LANE_UP(AUR_RX_LANE_UP),
    .RX_CHANNEL_UP(AUR_RX_CHANNEL_UP),
    .PLL_LOCKED(AUR_PLL_LOCKED),

    // I2C bus
    .I2C_SDA(I2C_SDA), .I2C_SCL(I2C_SCL),

    // FIFO cpontrol signals (TX FIFO of DAQ)
    .FIFO_DATA(FIFO_DATA),
    .FIFO_WRITE(FIFO_WRITE),
    .FIFO_EMPTY(FIFO_EMPTY),
    .FIFO_FULL(FIFO_FULL),

    .BX_CLK_EN(),
    .DUT_RESET(DUT_RESET),
    .RESET_TB(RESET_TB),
    .AURORA_RESET(AURORA_RESET),

    // TLU
    .RJ45_TRIGGER(RJ45_TRIGGER),
    .RJ45_RESET(RJ45_RESET),
    .RJ45_BUSY_LEMO_TX1(LEMO_TX1),
    .RJ45_CLK_LEMO_TX0(LEMO_TX0)
);


// Command encoder outputs
wire CMD_FMC_OUT_REG;
wire [3:0] CMD_P, CMD_N;
wire CMD_CLK_OUT_P, CMD_CLK_OUT_N;
wire AURORA_CLK_OUT_P, AURORA_CLK_OUT_N;

`ifdef KC705
    localparam nr_of_cmd_outputs = 2;

    wire [nr_of_cmd_outputs-1:0] CMD_OUT;
    assign CMD_OUT = {~CMD_OUT_CH0, CMD_OUT_CH0};

    assign CMD_FMC_LPC_P = CMD_P[0];
    assign CMD_FMC_LPC_N = CMD_N[0];

    assign USER_SMA_P = CMD_P[1];
    assign USER_SMA_N = CMD_N[1];

    assign DP1_EXT_CMD_CLK_P = CMD_CLK_OUT_P;
    assign DP1_EXT_CMD_CLK_N = CMD_CLK_OUT_N;
    assign DP1_EXT_SER_CLK_P = AURORA_CLK_OUT_P;
    assign DP1_EXT_SER_CLK_N = AURORA_CLK_OUT_N;
`elsif BDAQ53
    localparam nr_of_cmd_outputs = 4;

    wire [nr_of_cmd_outputs-1:0] CMD_OUT;
    assign CMD_OUT = {~CMD_OUT_CH0, ~CMD_OUT_CH0, ~CMD_OUT_CH0, ~CMD_OUT_CH0};

    assign DP_ML_AUX_P = CMD_P[0];
    assign DP_ML_AUX_N = CMD_N[0];
    assign DP_SL_AUX_P[2:0] = CMD_P[3:1];
    assign DP_SL_AUX_N[2:0] = CMD_N[3:1];
`elsif USBPIX3
    localparam nr_of_cmd_outputs = 0;
    wire [nr_of_cmd_outputs-1:0] CMD_OUT;
`else
    localparam nr_of_cmd_outputs = 0;
    wire [nr_of_cmd_outputs-1:0] CMD_OUT;
`endif

wire [nr_of_cmd_outputs-1:0] CMD_OUT_REG;

genvar i;
generate
    for (i=0; i<nr_of_cmd_outputs; i=i+1) begin : generate_cmd_obufds
        ODDR oddr_cmd_gen(
            .D1(CMD_OUT[i]), .D2(CMD_OUT[i]),
            .C(CLKCMD), .CE(1'b1), .R(1'b0), .S(1'b0),
            .Q(CMD_OUT_REG[i])
        );

        OBUFDS #(
            .IOSTANDARD("LVDS"),    // Specify the output I/O standard
            .SLEW("FAST")           // Specify the output slew rate
        ) i_OBUFDS_CMD_DATA_gen (
            .O(CMD_P[i]),           // Diff_p output (connect directly to top-level port)
            .OB(CMD_N[i]),          // Diff_n output (connect directly to top-level port)
            .I(CMD_OUT_REG[i])      // Buffer input
        );
    end
endgenerate


ODDR oddr_aurora(
    .D1(1'b1), .D2(1'b0),
    .C(AURORA_CLK_OUT), .CE(1'b1), .R(1'b0), .S(1'b0),
    .Q(AURORA_CLK_OUT_REG)
);

OBUFDS #(
    .IOSTANDARD("LVDS"),    // Specify the output I/O standard
    .SLEW("FAST")           // Specify the output slew rate
) i_OBUFDS_AURORA_CLK (
    .O(AURORA_CLK_OUT_P),   // Diff_p output (connect directly to top-level port)
    .OB(AURORA_CLK_OUT_N),  // Diff_n output (connect directly to top-level port)
    .I(AURORA_CLK_OUT_REG)      // Buffer input
);


ODDR oddr_cmd_clk(
    .D1(1'b1), .D2(1'b0),
    .C(CLKCMD), .CE(1'b1), .R(1'b0), .S(1'b0),
    .Q(CLKCMD_REG)
    );

OBUFDS #(
    .IOSTANDARD("LVDS"),    // Specify the output I/O standard
    .SLEW("FAST")           // Specify the output slew rate
) i_OBUFDS_CMD_CLK (
    .O(CMD_CLK_OUT_P),      // Diff_p output (connect directly to top-level port)
    .OB(CMD_CLK_OUT_N),     // Diff_n output (connect directly to top-level port)
    .I(CLKCMD_REG)              // Buffer input
);


// -------  MODULES for fast data readout(FIFO) - cdc_fifo is for timing reasons
wire [31:0] cdc_data_out;
wire full_32to8, cdc_fifo_empty;

cdc_syncfifo #(.DSIZE(32), .ASIZE(3)) cdc_syncfifo_i
(
    .rdata(cdc_data_out),
    .wfull(FIFO_FULL),
    .rempty(cdc_fifo_empty),
    .wdata(FIFO_DATA),
    .winc(FIFO_WRITE), .wclk(BUS_CLK), .wrst(BUS_RST),
    .rinc(!full_32to8), .rclk(BUS_CLK), .rrst(BUS_RST)
);


fifo_32_to_8 #(.DEPTH(256*1024)) i_data_fifo (
    .RST(BUS_RST),
    .CLK(BUS_CLK),

    .WRITE(!cdc_fifo_empty),
    .READ(TCP_TX_WR),
    .DATA_IN(cdc_data_out),
    .FULL(full_32to8),
    .EMPTY(FIFO_EMPTY),
    .DATA_OUT(TCP_TX_DATA)
);


// draining the TCP-FIFO
assign TCP_TX_WR = !TCP_TX_FULL && !FIFO_EMPTY;


// Status LEDs
wire [7:0] LED_int;
assign LED_int = {TCP_OPEN_ACK, TCP_RX_WR, TCP_TX_WR, FIFO_FULL, AUR_RX_LANE_UP, AUR_RX_CHANNEL_UP, AUR_PLL_LOCKED, LOCKED};
`ifdef KC705 assign LED = LED_int;
`else        assign LED = ~LED_int;  // BDAQ uses active-low LED drivers
`endif


// Reduce the fan speed (KC705)
`ifdef KC705
    clock_divider #(
    .DIVISOR(142860)
    ) i_clock_divisor_fan (
        .CLK(BUS_CLK),
        .RESET(1'b0),
        .CE(),
        .CLOCK(SM_FAN_PWM)
    );
`endif

endmodule
